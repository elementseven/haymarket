<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug')->unique()->nullable();
            $table->string('link')->nullable();
            $table->boolean('status')->nullable();
            $table->text('summary')->nullable();
            $table->text('details')->nullable();
            $table->text('boooking_code')->nullable();
            $table->text('boooking_code_script')->nullable();
            $table->datetime('expiry')->nullable();
            $table->integer('order')->nullable();
            $table->string('image')->nullable();
            
            $table->bigInteger('venue_id')->unsigned()->index()->nullable();
            $table->foreign('venue_id')->references('id')->on('venues')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('offers');
    }
};