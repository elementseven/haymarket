<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\InstagramSession;

class GetInstagramSession extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:getsession';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a new instagram session';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $instagram  = \InstagramScraper\Instagram::withCredentials(new \GuzzleHttp\Client(), 'haymarketbelfast', 'Haymarket247!', new \Phpfastcache\Helper\Psr16Adapter('Files'));
        $login = $instagram->login();
        $whatIWant = substr($login['cookie'], strpos($login['cookie'], "sessionid") + 10);    
        $final = strtok($whatIWant, ';');
        $db = InstagramSession::first();
        $db->sessionid = $final;
        $db->save();
    }
}
