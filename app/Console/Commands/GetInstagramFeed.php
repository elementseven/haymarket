<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\InstagramSession;
use App\Models\InstagramImage;

class GetInstagramFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:getfeed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the latest instagram images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $db = InstagramSession::first();
        $sessionid = $db->sessionid;
        $instagram  = \InstagramScraper\Instagram::withCredentials(new \GuzzleHttp\Client(), 'haymarketbelfast', 'Haymarket247!', new \Phpfastcache\Helper\Psr16Adapter('Files'));
        $instagram->loginWithSessionId($sessionid);
        $feed  = $instagram->getPaginateMedias('haymarketbelfast');

        $old = InstagramImage::get();
        foreach($old as $o){
            $o->delete();
        }
        $key = 1;
        foreach($feed['medias'] as $f){
            if($key <= 8){
                $theimg = InstagramImage::create([
                    'url' => $f->getImageHighResolutionUrl()
                ]);
                $theimg->addMediaFromUrl($theimg->url)->toMediaCollection('instagram');
            }
            $key++;
        }   
        
    }
}
