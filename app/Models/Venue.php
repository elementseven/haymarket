<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Venue extends Model
{
    use HasFactory;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name',
        'slug'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($venue) {
            $venue->slug = Str::slug($venue->name, "-");
        });
    }

    public function offers(){
        return $this->hasMany('App\Models\Offer');
    }
}
