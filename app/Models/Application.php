<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Application extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'phone', 'email', 'role', 'cv'
  ];

  protected $casts = [
        'created_at' => 'datetime',
    ]; 

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('applications')->singleFile();
    }
}
