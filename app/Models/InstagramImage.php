<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class InstagramImage extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    protected $fillable = [
      'url'
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->height(500)->crop('crop-center', 266, 266);
        $this->addMediaConversion('webp')->height(500)->crop('crop-center', 266, 266)->format('webp');
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('instagram')->singleFile();
    }

}
