<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Str;

class Offer extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'title',
        'slug',
        'link',
        'roller',
        'summary',
        'details',
        'brunch',
        'booking_code',
        'booking_code_script',
        'expiry',
        'order',
        'image',
        'venue_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($offer) {
            $offer->slug = Str::slug($offer->title, "-");
        });
    }
    
    protected $casts = [
        'expiry' => 'date',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('normal')->width(800);
        $this->addMediaConversion('normal-webp')->width(800)->format('webp');
        $this->addMediaConversion('mob')->width(500);
        $this->addMediaConversion('mob-webp')->width(500)->format('webp');
        $this->addMediaConversion('thumbnail')->crop('crop-center', 400, 400);
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('offers')->singleFile();
    }

    public function venue(){
        return $this->belongsTo('App\Models\Venue');
    }
}
