<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Boolean;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Offer extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var class-string<\App\Models\Offer>
     */
    public static $model = \App\Models\Offer::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            Text::make('Title')->sortable(),
            Text::make('Link')->onlyOnForms()->nullable(),
            Text::make('Roller')->onlyOnForms()->nullable(),
            Boolean::make('Status')->sortable(),
            Trix::make('Summary')->onlyOnForms()->nullable(),
            Text::make('Details')->onlyOnForms()->nullable(),
            Boolean::make('Brunch')->sortable(),
            Image::make('Image')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('image')->toMediaCollection('offers');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('offers', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('offers', 'thumbnail');
            })->deletable(false)->nullable(),
            Date::make('Expiry', 'expiry')->sortable()->nullable(),
            Text::make('Booking Code','booking_code')->onlyOnForms()->nullable(),
            Text::make('Booking Code Script','booking_code_script')->onlyOnForms()->nullable(),
            Number::make('Order')->nullable(),
            BelongsTo::make('Venue')->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}