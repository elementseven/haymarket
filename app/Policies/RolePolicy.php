<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can create a Role.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Role $role
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can update the Role.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Role $role
     * @return mixed
     */
    public function update(User $user, Role $role)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the Role.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Role $role
     * @return mixed
     */
    public function view(User $user, Role $role)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the Role.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Role $role
     * @return mixed
     */
    public function delete(User $user, Role $role)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Role  $role
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role->name == 'Super Admin';
    }

}   
