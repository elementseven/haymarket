<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InstagramImage;
use App\Models\FaqCategory;
use App\Models\Faq;
use App\Models\Offer;
class PageController extends Controller
{

  // Main Pages
  public function newHome(){
    return view('new-home');
  }
  public function theCourtyard(){
    return view('courtyard');
  }
  public function theArmoury(){
    return view('armoury');
  }
  public function theArmouryInfo(){
    $categories = FaqCategory::whereHas('Faqs')->get();
    return view('armoury-info')->with(['categories' => $categories]);
  }

  public function theStockExchange(){
    return view('stock');
  }
  public function virtualDarts(){
    $categories = FaqCategory::whereHas('Faqs')->get();
    return view('virtual-darts')->with(['categories' => $categories]);
  }
  public function stockMarketBar(){
    $categories = FaqCategory::whereHas('Faqs')->get();
    return view('stock-market-bar')->with(['categories' => $categories]);
  }
  public function shuffleBoard(){
    $categories = FaqCategory::whereHas('Faqs')->get();
    return view('shuffle-board')->with(['categories' => $categories]);
  }

  public function welcome(){
    return view('new-home');
  }
  public function contact(){
    $categories = FaqCategory::whereHas('Faqs')->get();
    return view('contact')->with(['categories' => $categories]);
  }
  public function parties(){
    return view('parties');
  }
  public function workForUs(){
    return view('workForUs');
  }
  public function resdiary(){
    return view('resdiary');
  }
  public function tandcs(){
    return view('tandcs');
  }
  public function privacyPolicy(){
    return view('privacyPolicy');
  }
  public function vouchers(){
    return view('vouchers');
  }
  public function xmasvouchers(){
    return view('xmasvouchers');
  }

  // Student Deals
  public function studentDeals(){
    return view('offers.students.index');
  }
  public function studentBrunch(){
    return view('offers.students.student-brunch');
  }
  public function studentQuiz(){
    return view('offers.students.student-quiz');
  }

  // Bottomless Brunch Takeovers
  public function takeovers(){
    return view('offers.takeover.index');
  }
  public function abba(){
    return view('offers.takeover.abba');
  }
  public function shania(){
    return view('offers.takeover.shania');
  }
  public function taylor(){
    return view('offers.takeover.taylor');
  }
  public function lust(){
    return view('offers.takeover.lust');
  }
  public function brookes(){
    return view('offers.takeover.brookes');
  }
  
  // Offers & Events
  public function offers(){
    return view('offers.index');
  }
  public function karaoke(){
    return view('offers.karaoke');
  }
  public function selectBrunch(){
    $brunches = Offer::whereIn('id', [19,3,15,21,31,32])->orderBy('order','asc')->get();
    foreach($brunches as $o){
      $o->normal = $o->getFirstMediaUrl('offers', 'normal');
      $o->normalwebp = $o->getFirstMediaUrl('offers', 'normal-webp');
      $o->mob = $o->getFirstMediaUrl('offers', 'mob');
      $o->mobwebp = $o->getFirstMediaUrl('offers', 'mob-webp');
      $o->mimetype = $o->getFirstMedia('offers')->mime_type;
    }
    return view('events.select-brunch')->with(['brunches' => $brunches]);
  }
  public function bottomlessBrunch(){
    return view('events.bottomlessBrunch');
  }
  public function bottomlessBrunchnew(){
    return view('events.bottomless-brunch');
  }
  public function winOrBooze(){
    return view('events.win-or-booze');
  }
  public function throwbackThursday(){
    return view('events.throwback-thursday');
  }
  public function fridays(){
    return view('events.fridays');
  }
  public function saturdays(){
    return view('events.saturdays');
  }
  public function sixNations(){
    return view('offers.six-nations');
  }
  public function mothersday(){
    return view('offers.mothers-day');
  }
  public function patricks(){
    return view('offers.patricks');
  }
  public function easter(){
    return view('offers.easter');
  }
  public function christmas(){
    return view('christmas');
  }

  public function instagram(Request $request){
    $images = InstagramImage::paginate(8);
    foreach($images as $p){
      $p->normal = $p->getFirstMediaUrl('instagram', 'normal');
      $p->webp = $p->getFirstMediaUrl('instagram', 'webp');
      $p->mimetype = $p->getFirstMedia('instagram')->mime_type;
    }
    return $images;
  }
}
