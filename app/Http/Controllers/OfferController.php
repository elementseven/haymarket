<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use Illuminate\Http\Request;
use Carbon\Carbon;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('offers.index-new');
    }

    /**
     * get offers
     */
    public function get(Request $request)
    {
        $now = Carbon::now();
        if($request->input('venue') != null && $request->input('venue') != '' && $request->input('venue') != 'undefined'){
            $offers = Offer::where('status', 1)
            ->where(function ($query) use ($now) {
                $query->whereDate('expiry', '>', $now)
                      ->orWhereNull('expiry');
            })
            ->where('venue_id', $request->input('venue'))
            ->orderBy('order', 'asc')
            ->get();
        }else{
            $offers = Offer::where('status', 1)
               ->where(function($query) use ($now) {
                   $query->whereDate('expiry', '>', $now)
                         ->orWhereNull('expiry');
               })
               ->orderBy('order', 'asc')
               ->get();
        }
        foreach($offers as $o){
          $o->normal = $o->getFirstMediaUrl('offers', 'normal');
          $o->normalwebp = $o->getFirstMediaUrl('offers', 'normal-webp');
          $o->mob = $o->getFirstMediaUrl('offers', 'mob');
          $o->mobwebp = $o->getFirstMediaUrl('offers', 'mob-webp');
          $o->mimetype = $o->getFirstMedia('offers')->mime_type;
        }
        return $offers;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Offer $offer)
    {
        if($offer->venue_id == 1){
            return view('offers.show')->with(['offer' => $offer]);
        }else if($offer->venue_id == 2){
            return view('offers.armouryshow')->with(['offer' => $offer]);
        }else{
            return view('offers.show')->with(['offer' => $offer]);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Offer $offer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Offer $offer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Offer $offer)
    {
        //
    }
}
