<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        $menu = Menu::where('name', 'Food Menu')->orderBy('created_at','desc')->first();
        $url = $menu->getFirstMediaUrl('menus');
        return redirect()->to($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function food()
    {
        $menu = Menu::where('name', 'Food Menu')->orderBy('created_at','desc')->first();
        $url = $menu->getFirstMediaUrl('menus');
        return redirect()->to($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function cocktail()
    {
        $menu = Menu::where('name', 'Cocktail Menu')->orderBy('created_at','desc')->first();
        $url = $menu->getFirstMediaUrl('menus');
        return redirect()->to($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function brunch()
    {
        $menu = Menu::where('name', 'Brunch Menu')->orderBy('created_at','desc')->first();
        $url = $menu->getFirstMediaUrl('menus');
        return redirect()->to($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function stockFood()
    {
        $menu = Menu::where('name', 'Stock Food Menu')->orderBy('created_at','desc')->first();
        $url = $menu->getFirstMediaUrl('menus');
        return redirect()->to($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function stockDrinks()
    {
        $menu = Menu::where('name', 'Stock Drinks Menu')->orderBy('created_at','desc')->first();
        $url = $menu->getFirstMediaUrl('menus');
        return redirect()->to($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function armouryFood()
    {
        $menu = Menu::where('name', 'Armoury Food Menu')->orderBy('created_at','desc')->first();
        $url = $menu->getFirstMediaUrl('menus');
        return redirect()->to($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu $menu
     * @return \Illuminate\Http\Response
     */
    public function armouryDrinks()
    {
        $menu = Menu::where('name', 'Armoury Drinks Menu')->orderBy('created_at','desc')->first();
        $url = $menu->getFirstMediaUrl('menus');
        return redirect()->to($url);
    }

}
