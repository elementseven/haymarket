<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

use Newsletter;
use Mail;
use Auth;


class SendMail extends Controller
{
  public function enquiry(Request $request){

    // Validate the form data
		$this->validate($request,[
			'name' => 'required|string|max:255',
			'email' => 'required|email',
			'message' => 'required|string',
		]); 

		$subject = "General Enquiry";
		$name = $request->input('name');
		$email = $request->input('email');
		$content = $request->input('message');
		$phone = null;
		if($request->input('phone') != ""){
			$phone = $request->input('phone');
		}
		Mail::send('emails.enquiry',[
			'name' => $name,
			'subject' => $subject,
			'email' => $email,
			'phone' => $phone,
			'content' => $content
		], function ($message) use ($subject, $email, $name, $content, $phone){
			$message->from('donotreply@haymarketbelfast.com', 'Haymarket Belfast');
			$message->subject($subject);
			$message->replyTo($email);
			$message->to('info@haymarketbelfast.com');
			//$message->to('luke@elementseven.co');
		});
		return 'success';
	}

	public function christmasEnquiry(Request $request){
		// Validate the form data
		$this->validate($request,[
			'name' => 'required|string|max:255',
			'phone' => 'required|numeric',
			'email' => 'required|email',
			'date' => 'required|date',
			'time' => 'required|string|max:255',
			'people' => 'required|numeric|min:1|max:500'
		]); 

		$subject = "Christmas Booking Request";
		$name = $request->input('name');
		$phone = $request->input('phone');
		$time = $request->input('time');
		$email = $request->input('email');
		$content = $request->input('message');
		$date = Carbon::parse($request->input('date'))->format('d/m/Y');
		$people = $request->input('people');

		$content = null;
		if($request->input('message') != ""){
			$content = $request->input('message');
		}
		$heard = null;
		if($request->input('heard') != ""){
			$heard = $request->input('heard');
		}
		$company = null;
		if($request->input('company') != ""){
			$company = $request->input('company');
		}

		Mail::send('emails.christmas',[
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'time' => $time,
			'date' => $date,
			'people' => $people,
			'subject' => $subject,
			'company' => $company,
			'heard' => $heard,
			'content' => $content
		], function ($message) use ($subject, $email, $phone, $name, $content, $time, $people, $date, $company, $heard){
			$message->from('donotreply@haymarketbelfast.com', 'Haymarket Belfast');
			$message->subject($subject);
			$message->replyTo($email);
			$message->to('info@haymarketbelfast.com');
			//$message->to('luke@elementseven.co');

		});
		return 'success';
	}


	public function partyEnquiry(Request $request){
		// Validate the form data
		$this->validate($request,[
			'name' => 'required|string|max:255',
			'phone' => 'required|string|max:255',
			'email' => 'required|email',
			'people' => 'required|numeric|min:1|max:500',
			'date' => 'required|date'
		]); 

		$subject = "Party/Event Enquiry";
		$name = $request->input('name');
		$phone = $request->input('phone');
		$email = $request->input('email');
		$content = $request->input('message');
		$date = Carbon::parse($request->input('date'))->format('d/m/Y');
		$people = $request->input('people');
		$company = null;
		if($request->input('company') != ""){
			$company = $request->input('company');
		}
		$content = null;
		if($request->input('message') != ""){
			$content = $request->input('message');
		}
		Mail::send('emails.eventenquiry',[
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'company' => $company,
			'date' => $date,
			'people' => $people,
			'subject' => $subject,
			'content' => $content
		], function ($message) use ($subject, $email, $name, $content, $phone, $people, $date, $company){
			$message->from('donotreply@haymarketbelfast.com', 'Haymarket Belfast');
			$message->subject($subject);
			$message->replyTo($email);
			$message->to('info@haymarketbelfast.com');
			//$message->to('luke@elementseven.co');
		});
		return 'success';
	}

		public function armouryEnquiry(Request $request){
		// Validate the form data
		$this->validate($request,[
			'name' => 'required|string|max:255',
			'phone' => 'required|string|max:255',
			'email' => 'required|email',
			'people' => 'required|numeric|min:1|max:500',
			'time' => 'required',
			'group' => 'required',
			'date' => 'required|date'
		]); 

		$subject = "Armoury Enquiry";
		$name = $request->input('name');
		$phone = $request->input('phone');
		$email = $request->input('email');
		$time = $request->input('time');
		$group = $request->input('group');
		$content = $request->input('message');
		$date = Carbon::parse($request->input('date'))->format('d/m/Y');
		$people = $request->input('people');
		$company = null;
		if($request->input('company') != ""){
			$company = $request->input('company');
		}
		$content = null;
		if($request->input('message') != ""){
			$content = $request->input('message');
		}
		Mail::send('emails.armouryenquiry',[
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'company' => $company,
			'date' => $date,
			'people' => $people,
			'time' => $time,
			'group' => $group,
			'subject' => $subject,
			'content' => $content
		], function ($message) use ($subject, $email, $name, $content, $phone, $people, $date, $company, $time, $group){
			$message->from('donotreply@haymarketbelfast.com', 'Haymarket Belfast');
			$message->subject($subject);
			$message->replyTo($email);
			$message->to('info@haymarketbelfast.com');
			//$message->to('luke@elementseven.co');
		});
		return 'success';
	}

	public function mailingListSignup(Request $request){
	  	$this->validate($request,[
	      'email' => 'required|string|email|max:255'
	    ]);
	    Newsletter::subscribe($request->input('email'));
	    return "success";
	}
}