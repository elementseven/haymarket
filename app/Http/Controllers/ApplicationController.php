<?php

namespace App\Http\Controllers;

use App\Models\Application;
use Illuminate\Http\Request;
use Mail;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:applications',
            'phone' => 'required|string|max:255',
            'role' => 'required|string|max:255|not_in:*',
            'cv' => 'required|mimes:doc,pdf,docx'
        ],
        [
            'cv.required' => 'Please upload a CV along with your application.',
            'cv.mimes' => 'Your CV should be in doc, docx or pdf format.',
            'email.unique' => 'You have already applied for a role using this email address.',
            'role.not_in' => 'Please select a role.'
        ]);
        if ($request->hasFile('cv')) {
            $application = Application::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'role' => $request->input('role')
            ]);
            $application->addMediaFromRequest('cv')->toMediaCollection('applications');

            Mail::send('emails.newApplication',[
                'application' => $application
            ], function ($message) use ($application){
                $message->from('donotreply@haymarketbelfast.com', 'Haymarket Belfast');
                $message->subject('New Application');
                $message->replyTo($application->email);
                $message->to('info@haymarketbelfast.com');
                //$message->to('luke@elementseven.co');
            });

        }
        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function edit(Application $application)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        //
    }
}
