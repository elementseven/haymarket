<?php

namespace App\Http\Controllers;

use App\Models\DrinksMenu;
use Illuminate\Http\Request;

class DrinksMenuController extends Controller
{
    public function show(DrinksMenu $drinksmenu)
    {
        $menu = DrinksMenu::orderBy('created_at','desc')->first();
        $url = $menu->getFirstMediaUrl('drinksmenus');
        return redirect()->to($url);
    }
}
