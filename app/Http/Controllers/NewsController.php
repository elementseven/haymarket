<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;

class NewsController extends Controller
{

	// Main news page
  public function index(){
    $categories = Category::has('posts')->orderBy('name','asc')->get();
    return view('news.index')->with(['categories' => $categories]);
  }

  // Return json news posts
  public function get(Request $request){
  	if($request->input('category') == '*'){
      $posts = Post::where('status','!=','draft')
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
    }else{
      $posts = Post::orderBy('created_at','desc')->whereHas('categories', function($q) use($request){
          $q->where('id', $request->input('category'));
      })
      ->where('status','!=','draft')
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
    }
    foreach($posts as $p){
      $p->normal = $p->getFirstMediaUrl('news', 'normal');
      $p->normalwebp = $p->getFirstMediaUrl('news', 'normal-webp');
      $p->double = $p->getFirstMediaUrl('news', 'double');
      $p->doublewebp = $p->getFirstMediaUrl('news', 'double-webp');
      $p->featured = $p->getFirstMediaUrl('news', 'featured');
      $p->featuredwebp = $p->getFirstMediaUrl('news', 'featured-webp');
      $p->mimetype = $p->getFirstMedia('news')->mime_type;
    }
    return $posts;
  }

  // Single news article
  public function show($date, $slug){
  	$post = Post::where([['slug',$slug],['status','!=','draft']])->whereDate('created_at', $date)->first();
  	$others = Post::where('id','!=', $post->id)->where('status','!=','draft')->orderBy('created_at','desc')->paginate(2);
  	return view('news.show')->with(['post' => $post, 'others' => $others]);
  }

}
