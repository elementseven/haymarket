import path from 'path';
import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue'; 
import purge from '@erbelion/vite-plugin-laravel-purgecss'

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: true,
        }),
        vue({ 
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
        purge({
            templates: ['blade', 'vue'],
            dynamicAttributes: ['data-aos'],
            safelist: ['cc-window', 'carousel', 'carousel__track', 'carousel__viewport', 'carousel__sr-only', 
    'carousel__prev', 'carousel__next', 'carousel--rtl', 'carousel__pagination', 
    'carousel__pagination-button', 'carousel__slide', 'carousel__icon', 'collapse', 'card', 
    'aos-init', 'aos-animate', 'page-stock', 'btn-primary']
        })
    ],
    resolve: { 
        alias: {
            '$fonts': path.resolve(__dirname, "public/fonts"),
            '$img': path.resolve(__dirname, "public/img"),
            '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
            vue: 'vue/dist/vue.esm-bundler.js',
        },
    },
});
