(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["Slider"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeSliderVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! vue-owl-carousel */"./node_modules/vue-owl-carousel/dist/vue-owl-carousel.js");
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
components:{
carousel:vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default()},

data:function data(){
return {
videos:[],
errors:{},
success:false,
loaded:true};

},
methods:{}};


/***/},

/***/"./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesCssLoaderIndexJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesPostcssLoaderSrcIndexJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeSliderVueVueTypeStyleIndex0LangScss(module,exports,__webpack_require__){

exports=module.exports=__webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */"./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.id,"#cargo-slider-1 {\n  position: relative;\n  width: 1200px;\n  margin-right: 0;\n  margin-left: auto;\n}\n#cargo-slider-1 .slideshow-box {\n  position: absolute;\n  top: calc(50% - 288px);\n  width: 670px;\n  height: 556px;\n  left: -300px;\n  right: auto;\n  z-index: 2;\n  -webkit-box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n  -moz-box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n  box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n}\n#cargo-slider-1 .cargo-slideshow-logo {\n  width: 250px;\n  position: absolute;\n  top: 4rem;\n  left: 4rem;\n  z-index: 2;\n}\n#cargo-slider-1 .cargo-slide {\n  position: relative;\n  width: 1200;\n  height: 811px;\n  background-repeat: no-repeat;\n  background-position: center center;\n  background-size: cover;\n  border-radius: 0;\n  text-align: left;\n  vertical-align: top;\n  overflow: hidden;\n  cursor: pointer;\n}\n#cargo-slider-1 .cargo-slide img {\n  z-index: 1;\n  position: relative;\n  width: 1200px;\n  display: block;\n  -webkit-box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n  -moz-box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n  box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n}\n#cargo-slider-1 .owl-carousel .owl-stage-outer {\n  overflow: hidden !important;\n}\n#cargo-slider-1 .owl-theme .owl-dots {\n  position: absolute;\n  top: 50%;\n  right: 0;\n  width: 180px;\n  margin: auto;\n  transform: rotate(90deg);\n}\n#cargo-slider-1 .owl-theme .owl-dots .owl-dot span {\n  width: 30px;\n  height: 8px;\n  background: #fff;\n  border-radius: 0;\n}\n#cargo-slider-1 .owl-theme .owl-dots .owl-dot.active span {\n  background: #F6CF3C;\n}\n@media only screen and (max-width: 767px) {\n#cargo-slider-1 {\n    width: 100vw;\n}\n#cargo-slider-1 .cargo-slide {\n    width: 100vw;\n    height: 80vw;\n}\n#cargo-slider-1 .cargo-slide img {\n    width: 100vw;\n    height: 80vw;\n}\n#cargo-slider-1 .cargo-slideshow-logo {\n    width: 25vw;\n    top: 1rem;\n    left: 1rem;\n}\n#cargo-slider-1 .owl-theme .owl-dots {\n    margin: auto;\n    width: 80vw;\n    left: -35vw;\n    right: auto;\n}\n#cargo-slider-1 .owl-theme .owl-dots .owl-dot span {\n    width: 24px;\n    height: 6px;\n    margin: 3px;\n}\n#cargo-slider-1 .owl-theme .owl-nav {\n    margin: 1rem -33px -30px auto;\n}\n#cargo-slider-1 .slideshow-box {\n    top: auto;\n    width: calc(100vw - 2rem);\n    height: auto;\n    left: auto;\n    margin-left: 1rem;\n    position: relative;\n    margin-top: -20vw;\n}\n}\n",""]);

// exports


/***/},

/***/"./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesStyleLoaderIndexJsNode_modulesCssLoaderIndexJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesPostcssLoaderSrcIndexJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeSliderVueVueTypeStyleIndex0LangScss(module,__unused_webpack_exports,__webpack_require__){


var content=__webpack_require__(/*! !!../../../../node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Slider.vue?vue&type=style&index=0&lang=scss& */"./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=style&index=0&lang=scss&");

if(typeof content==='string')content=[[module.id,content,'']];

var transform;



var options={"hmr":true};

options.transform=transform;
options.insertInto=undefined;

var update=__webpack_require__(/*! !../../../../node_modules/style-loader/lib/addStyles.js */"./node_modules/style-loader/lib/addStyles.js")(content,options);

if(content.locals)module.exports=content.locals;

/***/},

/***/"./resources/js/components/Home/Slider.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/Home/Slider.vue ***!
  \*************************************************/
/***/function resourcesJsComponentsHomeSliderVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _Slider_vue_vue_type_template_id_00866296___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./Slider.vue?vue&type=template&id=00866296& */"./resources/js/components/Home/Slider.vue?vue&type=template&id=00866296&");
/* harmony import */var _Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./Slider.vue?vue&type=script&lang=js& */"./resources/js/components/Home/Slider.vue?vue&type=script&lang=js&");
/* harmony import */var _Slider_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./Slider.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/Home/Slider.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
_Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
_Slider_vue_vue_type_template_id_00866296___WEBPACK_IMPORTED_MODULE_0__.render,
_Slider_vue_vue_type_template_id_00866296___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Home/Slider.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Home/Slider.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/Home/Slider.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/***/function resourcesJsComponentsHomeSliderVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Slider.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default;

/***/},

/***/"./resources/js/components/Home/Slider.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Home/Slider.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************/
/***/function resourcesJsComponentsHomeSliderVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/style-loader/index.js!../../../../node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Slider.vue?vue&type=style&index=0&lang=scss& */"./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=style&index=0&lang=scss&");
/* harmony reexport (unknown) */var __WEBPACK_REEXPORT_OBJECT__={};
/* harmony reexport (unknown) */var _loop=function _loop(__WEBPACK_IMPORT_KEY__){if(__WEBPACK_IMPORT_KEY__!=="default")__WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__]=function(){return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__];};};for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__){_loop(__WEBPACK_IMPORT_KEY__);}
/* harmony reexport (unknown) */__webpack_require__.d(__webpack_exports__,__WEBPACK_REEXPORT_OBJECT__);


/***/},

/***/"./resources/js/components/Home/Slider.vue?vue&type=template&id=00866296&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/Home/Slider.vue?vue&type=template&id=00866296& ***!
  \********************************************************************************/
/***/function resourcesJsComponentsHomeSliderVueVueTypeTemplateId00866296(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_template_id_00866296___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_template_id_00866296___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_template_id_00866296___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Slider.vue?vue&type=template&id=00866296& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=template&id=00866296&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=template&id=00866296&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/Slider.vue?vue&type=template&id=00866296& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeSliderVueVueTypeTemplateId00866296(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"div",
{attrs:{id:"cargo-slider-1"}},
[
_c(
"carousel",
{
attrs:{
items:1,
margin:0,
center:false,
loop:true,
autoWidth:true,
nav:false,
dots:true,
autoplayTimeout:7000,
autoplayHoverPause:true,
autoplay:true,
"data-aos":"fade-right"}},


[
_c("div",{staticClass:"cargo-slide"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/cargo/home-cargo-1.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/cargo/home-cargo-1.jpg",
type:"image/jpg"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy",
attrs:{
src:"/img/cargo/home-cargo-1.jpg",
alt:
"Burgers at Cargo by Vertigo - Resteraunt Belfast Northern Ireland"}})])]),




_vm._v(" "),
_c("div",{staticClass:"cargo-slide"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/cargo/home-cargo-2.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/cargo/home-cargo-2.jpg",
type:"image/jpg"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy",
attrs:{
src:"/img/cargo/home-cargo-2.jpg",
alt:
"Pizza at Cargo by Vertigo - Resteraunt Belfast Northern Ireland"}})])]),




_vm._v(" "),
_c("div",{staticClass:"cargo-slide"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/cargo/home-cargo-4.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/cargo/home-cargo-4.jpg",
type:"image/jpg"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy",
attrs:{
src:"/img/cargo/home-cargo-4.jpg",
alt:
"Burgers and Beer at Cargo by Vertigo - Resteraunt Belfast Northern Ireland"}})])]),




_vm._v(" "),
_c("div",{staticClass:"cargo-slide"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/cargo/home-cargo-3.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/cargo/home-cargo-3.jpg",
type:"image/jpg"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy",
attrs:{
src:"/img/cargo/home-cargo-3.jpg",
alt:
"Drinks at Cargo by Vertigo - Resteraunt Belfast Northern Ireland"}})])])]),






_vm._v(" "),
_vm._m(0)],

1);

};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"div",
{
staticClass:
"slideshow-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-left",
attrs:{"data-aos":"fade-left"}},

[
_c("p",{staticClass:"box-title-top text-grey"},[
_vm._v("Great for")]),

_vm._v(" "),
_c("p",{staticClass:"mimic-h1"},[
_vm._v("Quality Food"),
_c("br"),
_vm._v("& Drinks")]),

_vm._v(" "),
_c("p",{staticClass:"text-large mb-4"},[
_vm._v(
"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of types.")]),


_vm._v(" "),
_c("p",{staticClass:"mb-0"},[
_c(
"button",
{
staticClass:"btn btn-black btn-icon d-inline-block booknowbtn",
attrs:{type:"button"}},

[
_vm._v("Book Now "),
_c("i",{staticClass:"custom-icon chevron-double-up-white"})])]),



_vm._v(" "),
_c(
"p",
{
staticClass:
"button-after button-after-dark mt-1 pl-2 mb-5 mob-mb-0"},

[
_c(
"a",
{
attrs:{
href:"/docs/cargo_by_vertigo_a4_food_menu_v19.pdf",
target:"_blank"}},


[_c("i",{staticClass:"return-arrow"}),_vm._v(" View Menu")])])]);





}];

_render._withStripped=true;



/***/}}]);

}());
