(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["TopDetails"],{

/***/"./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/TopDetails.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/TopDetails.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesCssLoaderIndexJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesPostcssLoaderSrcIndexJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsTopDetailsVueVueTypeStyleIndex0LangScss(module,exports,__webpack_require__){

exports=module.exports=__webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */"./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.id,".top-details {\n  position: absolute;\n  height: 500px;\n  right: 3rem;\n  top: calc(50% - 250px);\n  z-index: 5;\n}\n.top-details a {\n  color: #fff;\n}\n.top-details a i {\n  font-size: 20px;\n}\n.top-details a:hover {\n  color: #F6CF3C;\n}\n.top-details a.top-details-email {\n  display: inline-block;\n  font-family: Gotham, sans-serif;\n  text-transform: uppercase;\n  transform: rotate(270deg);\n  position: absolute;\n  top: 217px;\n  left: -111px;\n  font-size: 1rem;\n  font-weight: 700;\n}\n.top-details .skydiver {\n  position: absolute;\n  top: 473px;\n  left: -25px;\n  width: 70px;\n}\n.top-details .skydiver img {\n  width: 70px;\n}\n.top-details .skydiver:before {\n  height: 100px;\n  width: 2px;\n  background-color: #fff;\n  content: \"\";\n  position: absolute;\n  top: -110px;\n  left: 34px;\n}\n",""]);

// exports


/***/},

/***/"./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/TopDetails.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/TopDetails.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesStyleLoaderIndexJsNode_modulesCssLoaderIndexJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesPostcssLoaderSrcIndexJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsTopDetailsVueVueTypeStyleIndex0LangScss(module,__unused_webpack_exports,__webpack_require__){


var content=__webpack_require__(/*! !!../../../node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./TopDetails.vue?vue&type=style&index=0&lang=scss& */"./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/TopDetails.vue?vue&type=style&index=0&lang=scss&");

if(typeof content==='string')content=[[module.id,content,'']];

var transform;



var options={"hmr":true};

options.transform=transform;
options.insertInto=undefined;

var update=__webpack_require__(/*! !../../../node_modules/style-loader/lib/addStyles.js */"./node_modules/style-loader/lib/addStyles.js")(content,options);

if(content.locals)module.exports=content.locals;

/***/},

/***/"./resources/js/components/TopDetails.vue":
/*!************************************************!*\
  !*** ./resources/js/components/TopDetails.vue ***!
  \************************************************/
/***/function resourcesJsComponentsTopDetailsVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _TopDetails_vue_vue_type_template_id_d2264550___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./TopDetails.vue?vue&type=template&id=d2264550& */"./resources/js/components/TopDetails.vue?vue&type=template&id=d2264550&");
/* harmony import */var _TopDetails_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./TopDetails.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/TopDetails.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script={};



/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
script,
_TopDetails_vue_vue_type_template_id_d2264550___WEBPACK_IMPORTED_MODULE_0__.render,
_TopDetails_vue_vue_type_template_id_d2264550___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/TopDetails.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/TopDetails.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/TopDetails.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************/
/***/function resourcesJsComponentsTopDetailsVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_TopDetails_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../node_modules/style-loader/index.js!../../../node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./TopDetails.vue?vue&type=style&index=0&lang=scss& */"./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/TopDetails.vue?vue&type=style&index=0&lang=scss&");
/* harmony reexport (unknown) */var __WEBPACK_REEXPORT_OBJECT__={};
/* harmony reexport (unknown) */var _loop=function _loop(__WEBPACK_IMPORT_KEY__){if(__WEBPACK_IMPORT_KEY__!=="default")__WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__]=function(){return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_TopDetails_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__];};};for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_TopDetails_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__){_loop(__WEBPACK_IMPORT_KEY__);}
/* harmony reexport (unknown) */__webpack_require__.d(__webpack_exports__,__WEBPACK_REEXPORT_OBJECT__);


/***/},

/***/"./resources/js/components/TopDetails.vue?vue&type=template&id=d2264550&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/TopDetails.vue?vue&type=template&id=d2264550& ***!
  \*******************************************************************************/
/***/function resourcesJsComponentsTopDetailsVueVueTypeTemplateIdD2264550(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TopDetails_vue_vue_type_template_id_d2264550___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TopDetails_vue_vue_type_template_id_d2264550___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TopDetails_vue_vue_type_template_id_d2264550___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./TopDetails.vue?vue&type=template&id=d2264550& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/TopDetails.vue?vue&type=template&id=d2264550&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/TopDetails.vue?vue&type=template&id=d2264550&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/TopDetails.vue?vue&type=template&id=d2264550& ***!
  \**********************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsTopDetailsVueVueTypeTemplateIdD2264550(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _vm._m(0);
};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"div",
{staticClass:"top-details text-white text-center d-none d-lg-block"},
[
_c("a",{attrs:{href:"http://www.facebook.com/VertigoBelfast"}},[
_c("i",{staticClass:"fa fa-facebook mb-3"})]),

_c("br"),
_vm._v(" "),
_c(
"a",
{attrs:{href:"http://www.instagram.com/wearevertigobelfast/"}},
[_c("i",{staticClass:"fa fa-instagram mb-3"})]),

_c("br"),
_vm._v(" "),
_c("a",{attrs:{href:"http://twitter.com/weare_vertigo"}},[
_c("i",{staticClass:"fa fa-twitter"})]),

_c("br"),
_vm._v(" "),
_c(
"a",
{
staticClass:"top-details-email",
attrs:{href:"mailto:info@wearevertigo.com"}},

[_vm._v("info@wearevertigo.com")]),

_vm._v(" "),
_c("div",{staticClass:"skydiver"},[
_c("img",{
attrs:{
src:"/img/icons/skydiver.svg",
alt:"We are Vertigo Skydiver"}})])]);





}];

_render._withStripped=true;



/***/}}]);

}());
