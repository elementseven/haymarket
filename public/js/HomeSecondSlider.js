(function () {
'use strict';

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var check = function (it) {
  return it && it.Math == Math && it;
};

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global_1 =
  // eslint-disable-next-line es/no-global-this -- safe
  check(typeof globalThis == 'object' && globalThis) ||
  check(typeof window == 'object' && window) ||
  // eslint-disable-next-line no-restricted-globals -- safe
  check(typeof self == 'object' && self) ||
  check(typeof commonjsGlobal == 'object' && commonjsGlobal) ||
  // eslint-disable-next-line no-new-func -- fallback
  (function () { return this; })() || Function('return this')();

var fails = function (exec) {
  try {
    return !!exec();
  } catch (error) {
    return true;
  }
};

// Detect IE8's incomplete defineProperty implementation
var descriptors = !fails(function () {
  // eslint-disable-next-line es/no-object-defineproperty -- required for testing
  return Object.defineProperty({}, 1, { get: function () { return 7; } })[1] != 7;
});

var $propertyIsEnumerable = {}.propertyIsEnumerable;
// eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

// Nashorn ~ JDK8 bug
var NASHORN_BUG = getOwnPropertyDescriptor && !$propertyIsEnumerable.call({ 1: 2 }, 1);

// `Object.prototype.propertyIsEnumerable` method implementation
// https://tc39.es/ecma262/#sec-object.prototype.propertyisenumerable
var f = NASHORN_BUG ? function propertyIsEnumerable(V) {
  var descriptor = getOwnPropertyDescriptor(this, V);
  return !!descriptor && descriptor.enumerable;
} : $propertyIsEnumerable;

var objectPropertyIsEnumerable = {
	f: f
};

var createPropertyDescriptor = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};

var toString = {}.toString;

var classofRaw = function (it) {
  return toString.call(it).slice(8, -1);
};

var split = ''.split;

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var indexedObject = fails(function () {
  // throws an error in rhino, see https://github.com/mozilla/rhino/issues/346
  // eslint-disable-next-line no-prototype-builtins -- safe
  return !Object('z').propertyIsEnumerable(0);
}) ? function (it) {
  return classofRaw(it) == 'String' ? split.call(it, '') : Object(it);
} : Object;

// `RequireObjectCoercible` abstract operation
// https://tc39.es/ecma262/#sec-requireobjectcoercible
var requireObjectCoercible = function (it) {
  if (it == undefined) throw TypeError("Can't call method on " + it);
  return it;
};

// toObject with fallback for non-array-like ES3 strings



var toIndexedObject = function (it) {
  return indexedObject(requireObjectCoercible(it));
};

var isObject = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};

// `ToPrimitive` abstract operation
// https://tc39.es/ecma262/#sec-toprimitive
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
var toPrimitive = function (input, PREFERRED_STRING) {
  if (!isObject(input)) return input;
  var fn, val;
  if (PREFERRED_STRING && typeof (fn = input.toString) == 'function' && !isObject(val = fn.call(input))) return val;
  if (typeof (fn = input.valueOf) == 'function' && !isObject(val = fn.call(input))) return val;
  if (!PREFERRED_STRING && typeof (fn = input.toString) == 'function' && !isObject(val = fn.call(input))) return val;
  throw TypeError("Can't convert object to primitive value");
};

// `ToObject` abstract operation
// https://tc39.es/ecma262/#sec-toobject
var toObject = function (argument) {
  return Object(requireObjectCoercible(argument));
};

var hasOwnProperty = {}.hasOwnProperty;

var has = Object.hasOwn || function hasOwn(it, key) {
  return hasOwnProperty.call(toObject(it), key);
};

var document = global_1.document;
// typeof document.createElement is 'object' in old IE
var EXISTS = isObject(document) && isObject(document.createElement);

var documentCreateElement = function (it) {
  return EXISTS ? document.createElement(it) : {};
};

// Thank's IE8 for his funny defineProperty
var ie8DomDefine = !descriptors && !fails(function () {
  // eslint-disable-next-line es/no-object-defineproperty -- requied for testing
  return Object.defineProperty(documentCreateElement('div'), 'a', {
    get: function () { return 7; }
  }).a != 7;
});

// eslint-disable-next-line es/no-object-getownpropertydescriptor -- safe
var $getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

// `Object.getOwnPropertyDescriptor` method
// https://tc39.es/ecma262/#sec-object.getownpropertydescriptor
var f$1 = descriptors ? $getOwnPropertyDescriptor : function getOwnPropertyDescriptor(O, P) {
  O = toIndexedObject(O);
  P = toPrimitive(P, true);
  if (ie8DomDefine) try {
    return $getOwnPropertyDescriptor(O, P);
  } catch (error) { /* empty */ }
  if (has(O, P)) return createPropertyDescriptor(!objectPropertyIsEnumerable.f.call(O, P), O[P]);
};

var objectGetOwnPropertyDescriptor = {
	f: f$1
};

var anObject = function (it) {
  if (!isObject(it)) {
    throw TypeError(String(it) + ' is not an object');
  } return it;
};

// eslint-disable-next-line es/no-object-defineproperty -- safe
var $defineProperty = Object.defineProperty;

// `Object.defineProperty` method
// https://tc39.es/ecma262/#sec-object.defineproperty
var f$2 = descriptors ? $defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (ie8DomDefine) try {
    return $defineProperty(O, P, Attributes);
  } catch (error) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};

var objectDefineProperty = {
	f: f$2
};

var createNonEnumerableProperty = descriptors ? function (object, key, value) {
  return objectDefineProperty.f(object, key, createPropertyDescriptor(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};

var setGlobal = function (key, value) {
  try {
    createNonEnumerableProperty(global_1, key, value);
  } catch (error) {
    global_1[key] = value;
  } return value;
};

var SHARED = '__core-js_shared__';
var store = global_1[SHARED] || setGlobal(SHARED, {});

var sharedStore = store;

var functionToString = Function.toString;

// this helper broken in `core-js@3.4.1-3.4.4`, so we can't use `shared` helper
if (typeof sharedStore.inspectSource != 'function') {
  sharedStore.inspectSource = function (it) {
    return functionToString.call(it);
  };
}

var inspectSource = sharedStore.inspectSource;

var WeakMap = global_1.WeakMap;

var nativeWeakMap = typeof WeakMap === 'function' && /native code/.test(inspectSource(WeakMap));

var shared = createCommonjsModule(function (module) {
(module.exports = function (key, value) {
  return sharedStore[key] || (sharedStore[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: '3.14.0',
  mode:  'global',
  copyright: '© 2021 Denis Pushkarev (zloirock.ru)'
});
});

var id = 0;
var postfix = Math.random();

var uid = function (key) {
  return 'Symbol(' + String(key === undefined ? '' : key) + ')_' + (++id + postfix).toString(36);
};

var keys = shared('keys');

var sharedKey = function (key) {
  return keys[key] || (keys[key] = uid(key));
};

var hiddenKeys = {};

var OBJECT_ALREADY_INITIALIZED = 'Object already initialized';
var WeakMap$1 = global_1.WeakMap;
var set, get, has$1;

var enforce = function (it) {
  return has$1(it) ? get(it) : set(it, {});
};

var getterFor = function (TYPE) {
  return function (it) {
    var state;
    if (!isObject(it) || (state = get(it)).type !== TYPE) {
      throw TypeError('Incompatible receiver, ' + TYPE + ' required');
    } return state;
  };
};

if (nativeWeakMap || sharedStore.state) {
  var store$1 = sharedStore.state || (sharedStore.state = new WeakMap$1());
  var wmget = store$1.get;
  var wmhas = store$1.has;
  var wmset = store$1.set;
  set = function (it, metadata) {
    if (wmhas.call(store$1, it)) throw new TypeError(OBJECT_ALREADY_INITIALIZED);
    metadata.facade = it;
    wmset.call(store$1, it, metadata);
    return metadata;
  };
  get = function (it) {
    return wmget.call(store$1, it) || {};
  };
  has$1 = function (it) {
    return wmhas.call(store$1, it);
  };
} else {
  var STATE = sharedKey('state');
  hiddenKeys[STATE] = true;
  set = function (it, metadata) {
    if (has(it, STATE)) throw new TypeError(OBJECT_ALREADY_INITIALIZED);
    metadata.facade = it;
    createNonEnumerableProperty(it, STATE, metadata);
    return metadata;
  };
  get = function (it) {
    return has(it, STATE) ? it[STATE] : {};
  };
  has$1 = function (it) {
    return has(it, STATE);
  };
}

var internalState = {
  set: set,
  get: get,
  has: has$1,
  enforce: enforce,
  getterFor: getterFor
};

var redefine = createCommonjsModule(function (module) {
var getInternalState = internalState.get;
var enforceInternalState = internalState.enforce;
var TEMPLATE = String(String).split('String');

(module.exports = function (O, key, value, options) {
  var unsafe = options ? !!options.unsafe : false;
  var simple = options ? !!options.enumerable : false;
  var noTargetGet = options ? !!options.noTargetGet : false;
  var state;
  if (typeof value == 'function') {
    if (typeof key == 'string' && !has(value, 'name')) {
      createNonEnumerableProperty(value, 'name', key);
    }
    state = enforceInternalState(value);
    if (!state.source) {
      state.source = TEMPLATE.join(typeof key == 'string' ? key : '');
    }
  }
  if (O === global_1) {
    if (simple) O[key] = value;
    else setGlobal(key, value);
    return;
  } else if (!unsafe) {
    delete O[key];
  } else if (!noTargetGet && O[key]) {
    simple = true;
  }
  if (simple) O[key] = value;
  else createNonEnumerableProperty(O, key, value);
// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
})(Function.prototype, 'toString', function toString() {
  return typeof this == 'function' && getInternalState(this).source || inspectSource(this);
});
});

var path = global_1;

var aFunction = function (variable) {
  return typeof variable == 'function' ? variable : undefined;
};

var getBuiltIn = function (namespace, method) {
  return arguments.length < 2 ? aFunction(path[namespace]) || aFunction(global_1[namespace])
    : path[namespace] && path[namespace][method] || global_1[namespace] && global_1[namespace][method];
};

var ceil = Math.ceil;
var floor = Math.floor;

// `ToInteger` abstract operation
// https://tc39.es/ecma262/#sec-tointeger
var toInteger = function (argument) {
  return isNaN(argument = +argument) ? 0 : (argument > 0 ? floor : ceil)(argument);
};

var min = Math.min;

// `ToLength` abstract operation
// https://tc39.es/ecma262/#sec-tolength
var toLength = function (argument) {
  return argument > 0 ? min(toInteger(argument), 0x1FFFFFFFFFFFFF) : 0; // 2 ** 53 - 1 == 9007199254740991
};

var max = Math.max;
var min$1 = Math.min;

// Helper for a popular repeating case of the spec:
// Let integer be ? ToInteger(index).
// If integer < 0, let result be max((length + integer), 0); else let result be min(integer, length).
var toAbsoluteIndex = function (index, length) {
  var integer = toInteger(index);
  return integer < 0 ? max(integer + length, 0) : min$1(integer, length);
};

// `Array.prototype.{ indexOf, includes }` methods implementation
var createMethod = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIndexedObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare -- NaN check
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare -- NaN check
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) {
      if ((IS_INCLUDES || index in O) && O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};

var arrayIncludes = {
  // `Array.prototype.includes` method
  // https://tc39.es/ecma262/#sec-array.prototype.includes
  includes: createMethod(true),
  // `Array.prototype.indexOf` method
  // https://tc39.es/ecma262/#sec-array.prototype.indexof
  indexOf: createMethod(false)
};

var indexOf = arrayIncludes.indexOf;


var objectKeysInternal = function (object, names) {
  var O = toIndexedObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) !has(hiddenKeys, key) && has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~indexOf(result, key) || result.push(key);
  }
  return result;
};

// IE8- don't enum bug keys
var enumBugKeys = [
  'constructor',
  'hasOwnProperty',
  'isPrototypeOf',
  'propertyIsEnumerable',
  'toLocaleString',
  'toString',
  'valueOf'
];

var hiddenKeys$1 = enumBugKeys.concat('length', 'prototype');

// `Object.getOwnPropertyNames` method
// https://tc39.es/ecma262/#sec-object.getownpropertynames
// eslint-disable-next-line es/no-object-getownpropertynames -- safe
var f$3 = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return objectKeysInternal(O, hiddenKeys$1);
};

var objectGetOwnPropertyNames = {
	f: f$3
};

// eslint-disable-next-line es/no-object-getownpropertysymbols -- safe
var f$4 = Object.getOwnPropertySymbols;

var objectGetOwnPropertySymbols = {
	f: f$4
};

// all object keys, includes non-enumerable and symbols
var ownKeys = getBuiltIn('Reflect', 'ownKeys') || function ownKeys(it) {
  var keys = objectGetOwnPropertyNames.f(anObject(it));
  var getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
  return getOwnPropertySymbols ? keys.concat(getOwnPropertySymbols(it)) : keys;
};

var copyConstructorProperties = function (target, source) {
  var keys = ownKeys(source);
  var defineProperty = objectDefineProperty.f;
  var getOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    if (!has(target, key)) defineProperty(target, key, getOwnPropertyDescriptor(source, key));
  }
};

var replacement = /#|\.prototype\./;

var isForced = function (feature, detection) {
  var value = data[normalize(feature)];
  return value == POLYFILL ? true
    : value == NATIVE ? false
    : typeof detection == 'function' ? fails(detection)
    : !!detection;
};

var normalize = isForced.normalize = function (string) {
  return String(string).replace(replacement, '.').toLowerCase();
};

var data = isForced.data = {};
var NATIVE = isForced.NATIVE = 'N';
var POLYFILL = isForced.POLYFILL = 'P';

var isForced_1 = isForced;

var getOwnPropertyDescriptor$1 = objectGetOwnPropertyDescriptor.f;






/*
  options.target      - name of the target object
  options.global      - target is the global object
  options.stat        - export as static methods of target
  options.proto       - export as prototype methods of target
  options.real        - real prototype method for the `pure` version
  options.forced      - export even if the native feature is available
  options.bind        - bind methods to the target, required for the `pure` version
  options.wrap        - wrap constructors to preventing global pollution, required for the `pure` version
  options.unsafe      - use the simple assignment of property instead of delete + defineProperty
  options.sham        - add a flag to not completely full polyfills
  options.enumerable  - export as enumerable property
  options.noTargetGet - prevent calling a getter on target
*/
var _export = function (options, source) {
  var TARGET = options.target;
  var GLOBAL = options.global;
  var STATIC = options.stat;
  var FORCED, target, key, targetProperty, sourceProperty, descriptor;
  if (GLOBAL) {
    target = global_1;
  } else if (STATIC) {
    target = global_1[TARGET] || setGlobal(TARGET, {});
  } else {
    target = (global_1[TARGET] || {}).prototype;
  }
  if (target) for (key in source) {
    sourceProperty = source[key];
    if (options.noTargetGet) {
      descriptor = getOwnPropertyDescriptor$1(target, key);
      targetProperty = descriptor && descriptor.value;
    } else targetProperty = target[key];
    FORCED = isForced_1(GLOBAL ? key : TARGET + (STATIC ? '.' : '#') + key, options.forced);
    // contained in target
    if (!FORCED && targetProperty !== undefined) {
      if (typeof sourceProperty === typeof targetProperty) continue;
      copyConstructorProperties(sourceProperty, targetProperty);
    }
    // add a flag to not completely full polyfills
    if (options.sham || (targetProperty && targetProperty.sham)) {
      createNonEnumerableProperty(sourceProperty, 'sham', true);
    }
    // extend global
    redefine(target, key, sourceProperty, options);
  }
};

// `String.prototype.repeat` method implementation
// https://tc39.es/ecma262/#sec-string.prototype.repeat
var stringRepeat = function repeat(count) {
  var str = String(requireObjectCoercible(this));
  var result = '';
  var n = toInteger(count);
  if (n < 0 || n == Infinity) throw RangeError('Wrong number of repetitions');
  for (;n > 0; (n >>>= 1) && (str += str)) if (n & 1) result += str;
  return result;
};

// https://github.com/tc39/proposal-string-pad-start-end




var ceil$1 = Math.ceil;

// `String.prototype.{ padStart, padEnd }` methods implementation
var createMethod$1 = function (IS_END) {
  return function ($this, maxLength, fillString) {
    var S = String(requireObjectCoercible($this));
    var stringLength = S.length;
    var fillStr = fillString === undefined ? ' ' : String(fillString);
    var intMaxLength = toLength(maxLength);
    var fillLen, stringFiller;
    if (intMaxLength <= stringLength || fillStr == '') return S;
    fillLen = intMaxLength - stringLength;
    stringFiller = stringRepeat.call(fillStr, ceil$1(fillLen / fillStr.length));
    if (stringFiller.length > fillLen) stringFiller = stringFiller.slice(0, fillLen);
    return IS_END ? S + stringFiller : stringFiller + S;
  };
};

var stringPad = {
  // `String.prototype.padStart` method
  // https://tc39.es/ecma262/#sec-string.prototype.padstart
  start: createMethod$1(false),
  // `String.prototype.padEnd` method
  // https://tc39.es/ecma262/#sec-string.prototype.padend
  end: createMethod$1(true)
};

var engineUserAgent = getBuiltIn('navigator', 'userAgent') || '';

// https://github.com/zloirock/core-js/issues/280


// eslint-disable-next-line unicorn/no-unsafe-regex -- safe
var stringPadWebkitBug = /Version\/10(?:\.\d+){1,2}(?: [\w./]+)?(?: Mobile\/\w+)? Safari\//.test(engineUserAgent);

var $padStart = stringPad.start;


// `String.prototype.padStart` method
// https://tc39.es/ecma262/#sec-string.prototype.padstart
_export({ target: 'String', proto: true, forced: stringPadWebkitBug }, {
  padStart: function padStart(maxLength /* , fillString = ' ' */) {
    return $padStart(this, maxLength, arguments.length > 1 ? arguments[1] : undefined);
  }
});

var process = global_1.process;
var versions = process && process.versions;
var v8 = versions && versions.v8;
var match, version;

if (v8) {
  match = v8.split('.');
  version = match[0] < 4 ? 1 : match[0] + match[1];
} else if (engineUserAgent) {
  match = engineUserAgent.match(/Edge\/(\d+)/);
  if (!match || match[1] >= 74) {
    match = engineUserAgent.match(/Chrome\/(\d+)/);
    if (match) version = match[1];
  }
}

var engineV8Version = version && +version;

/* eslint-disable es/no-symbol -- required for testing */



// eslint-disable-next-line es/no-object-getownpropertysymbols -- required for testing
var nativeSymbol = !!Object.getOwnPropertySymbols && !fails(function () {
  var symbol = Symbol();
  // Chrome 38 Symbol has incorrect toString conversion
  // `get-own-property-symbols` polyfill symbols converted to object are not Symbol instances
  return !String(symbol) || !(Object(symbol) instanceof Symbol) ||
    // Chrome 38-40 symbols are not inherited from DOM collections prototypes to instances
    !Symbol.sham && engineV8Version && engineV8Version < 41;
});

/* eslint-disable es/no-symbol -- required for testing */


var useSymbolAsUid = nativeSymbol
  && !Symbol.sham
  && typeof Symbol.iterator == 'symbol';

var WellKnownSymbolsStore = shared('wks');
var Symbol$1 = global_1.Symbol;
var createWellKnownSymbol = useSymbolAsUid ? Symbol$1 : Symbol$1 && Symbol$1.withoutSetter || uid;

var wellKnownSymbol = function (name) {
  if (!has(WellKnownSymbolsStore, name) || !(nativeSymbol || typeof WellKnownSymbolsStore[name] == 'string')) {
    if (nativeSymbol && has(Symbol$1, name)) {
      WellKnownSymbolsStore[name] = Symbol$1[name];
    } else {
      WellKnownSymbolsStore[name] = createWellKnownSymbol('Symbol.' + name);
    }
  } return WellKnownSymbolsStore[name];
};

var TO_STRING_TAG = wellKnownSymbol('toStringTag');
var test = {};

test[TO_STRING_TAG] = 'z';

var toStringTagSupport = String(test) === '[object z]';

var TO_STRING_TAG$1 = wellKnownSymbol('toStringTag');
// ES3 wrong here
var CORRECT_ARGUMENTS = classofRaw(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (error) { /* empty */ }
};

// getting tag from ES6+ `Object.prototype.toString`
var classof = toStringTagSupport ? classofRaw : function (it) {
  var O, tag, result;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (tag = tryGet(O = Object(it), TO_STRING_TAG$1)) == 'string' ? tag
    // builtinTag case
    : CORRECT_ARGUMENTS ? classofRaw(O)
    // ES3 arguments fallback
    : (result = classofRaw(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : result;
};

// `Object.prototype.toString` method implementation
// https://tc39.es/ecma262/#sec-object.prototype.tostring
var objectToString = toStringTagSupport ? {}.toString : function toString() {
  return '[object ' + classof(this) + ']';
};

// `Object.prototype.toString` method
// https://tc39.es/ecma262/#sec-object.prototype.tostring
if (!toStringTagSupport) {
  redefine(Object.prototype, 'toString', objectToString, { unsafe: true });
}

// `RegExp.prototype.flags` getter implementation
// https://tc39.es/ecma262/#sec-get-regexp.prototype.flags
var regexpFlags = function () {
  var that = anObject(this);
  var result = '';
  if (that.global) result += 'g';
  if (that.ignoreCase) result += 'i';
  if (that.multiline) result += 'm';
  if (that.dotAll) result += 's';
  if (that.unicode) result += 'u';
  if (that.sticky) result += 'y';
  return result;
};

var TO_STRING = 'toString';
var RegExpPrototype = RegExp.prototype;
var nativeToString = RegExpPrototype[TO_STRING];

var NOT_GENERIC = fails(function () { return nativeToString.call({ source: 'a', flags: 'b' }) != '/a/b'; });
// FF44- RegExp#toString has a wrong name
var INCORRECT_NAME = nativeToString.name != TO_STRING;

// `RegExp.prototype.toString` method
// https://tc39.es/ecma262/#sec-regexp.prototype.tostring
if (NOT_GENERIC || INCORRECT_NAME) {
  redefine(RegExp.prototype, TO_STRING, function toString() {
    var R = anObject(this);
    var p = String(R.source);
    var rf = R.flags;
    var f = String(rf === undefined && R instanceof RegExp && !('flags' in RegExpPrototype) ? regexpFlags.call(R) : rf);
    return '/' + p + '/' + f;
  }, { unsafe: true });
}

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["HomeSecondSlider"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************************/
/***/function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHomeSecondSliderVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! vue-owl-carousel */"./node_modules/vue-owl-carousel/dist/vue-owl-carousel.js");
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
components:{
carousel:vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default()},

data:function data(){
return {
posts:[],
errors:{},
success:false,
loaded:true,
current:0};

},
mounted:function mounted(){
this.getPosts();
},
methods:{
callback:function callback(event){
console.log(event);
this.current=event.item.index-2;
},
getExtension:function getExtension(d){
if(d>3&&d<21)return 'th';

switch(d%10){
case 1:
return "st";

case 2:
return "nd";

case 3:
return "rd";

default:
return "th";}

},
niceDate:function niceDate(date){
var newdate=new Date(date);
var thedate=newdate.getDate();
var extension=this.getExtension(thedate);
var months=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
return thedate+extension+' '+months[newdate.getMonth()]+' '+newdate.getFullYear();// return moment(date).format('Do MMM YYYY');
},
moment:function moment(date){
var newdate=new Date(date);
var thedate=newdate.getDate();
var extension=this.getExtension(thedate);
return newdate.getFullYear()+'-'+(newdate.getMonth()+1).toString().padStart(2,'0')+'-'+thedate.toString().padStart(2,'0');// return moment(date).format('Do MMM YYYY');
},
getPosts:function getPosts(){
var _this=this;

this.loaded=false;
this.success=false;
this.errors={};
this.offset=0;
this.loadmore=true;
this.page=1;
axios.get('/news/get?limit=3&page=1&category=*').then(function(response){
// Empty fields and enable success message
_this.loaded=true;
_this.success=true;
_this.posts=response.data.data;

if(response.data.last_page==response.data.current_page){
_this.loadmore=false;
}

console.log(_this.response);
})["catch"](function(error){
// Error handling
_this.loaded=true;
console.log(error);
});
}}};



/***/},

/***/"./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesCssLoaderIndexJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesPostcssLoaderSrcIndexJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHomeSecondSliderVueVueTypeStyleIndex0Id1b60062eLangScssScopedTrue(module,exports,__webpack_require__){

exports=module.exports=__webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */"./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.id,"#home-second-slider[data-v-1b60062e] {\n  position: relative;\n  width: 100%;\n  max-width: 1600px;\n}\n#home-second-slider .smaller-sizes-container[data-v-1b60062e] {\n  width: 100%;\n  max-width: 1200px;\n  overflow: hidden;\n}\n#home-second-slider .slideshow-box[data-v-1b60062e] {\n  position: absolute;\n  top: calc(50% - 288px);\n  width: 670px;\n  height: 556px;\n  right: 0;\n  z-index: 2;\n  -webkit-box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n  -moz-box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n  box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n}\n#home-second-slider .second-slide[data-v-1b60062e] {\n  position: relative;\n  width: 1200px;\n  height: 811px;\n  background-repeat: no-repeat;\n  background-position: center center;\n  background-size: cover;\n  border-radius: 0;\n  text-align: left;\n  vertical-align: top;\n  overflow: hidden;\n  cursor: pointer;\n}\n#home-second-slider .second-slide img[data-v-1b60062e] {\n  z-index: 1;\n  position: relative;\n  width: 1200px;\n  display: block;\n  -webkit-box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n  -moz-box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n  box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n}\n#home-second-slider .owl-carousel .owl-stage-outer[data-v-1b60062e] {\n  overflow: hidden !important;\n}\n@media only screen and (max-device-width: 1200px) {\n#home-second-slider[data-v-1b60062e] {\n    position: relative;\n    width: 100vw;\n}\n#home-second-slider .smaller-sizes-container[data-v-1b60062e] {\n    width: 90vw;\n    overflow: hidden;\n}\n#home-second-slider .slideshow-box[data-v-1b60062e] {\n    top: calc(50% - 260px);\n    width: 500px;\n    height: auto;\n    right: 1rem;\n}\n#home-second-slider .slideshow-box p.larger[data-v-1b60062e] {\n    font-size: 1rem;\n}\n#home-second-slider .slideshow-box p.mimic-h1[data-v-1b60062e] {\n    font-size: 3rem;\n}\n#home-second-slider .second-slide[data-v-1b60062e] {\n    width: 90vw;\n    height: auto;\n}\n#home-second-slider .second-slide img[data-v-1b60062e] {\n    z-index: 1;\n    width: 90vw;\n}\n#home-second-slider .owl-carousel .owl-stage-outer[data-v-1b60062e] {\n    overflow: hidden !important;\n}\n}\n@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) {\n#home-second-slider[data-v-1b60062e] {\n    width: 100vw;\n}\n#home-second-slider .second-slide[data-v-1b60062e] {\n    width: 100vw;\n    height: 80vw;\n}\n#home-second-slider .second-slide img[data-v-1b60062e] {\n    width: 100vw;\n    height: 80vw;\n}\n#home-second-slider .second-slideshow-logo[data-v-1b60062e] {\n    width: 25vw;\n    top: 1rem;\n    left: 1rem;\n}\n#home-second-slider .owl-theme .owl-dots[data-v-1b60062e] {\n    margin: auto;\n    width: 80vw;\n    left: -8rem;\n}\n#home-second-slider .owl-theme .owl-dots .owl-dot span[data-v-1b60062e] {\n    width: 24px;\n    height: 6px;\n    margin: 3px;\n}\n#home-second-slider .owl-theme .owl-nav[data-v-1b60062e] {\n    margin: 1rem -33px -30px auto;\n}\n#home-second-slider .slideshow-box[data-v-1b60062e] {\n    top: auto;\n    width: calc(100vw - 2rem);\n    height: auto;\n    right: auto;\n    margin-left: 1rem;\n    position: relative;\n    margin-top: -20vw;\n}\n}\n@media only screen and (max-width: 992px) {\n#home-second-slider[data-v-1b60062e] {\n    width: 100vw;\n}\n#home-second-slider .smaller-sizes-container[data-v-1b60062e] {\n    width: 100vw;\n    overflow: hidden;\n}\n#home-second-slider .second-slide[data-v-1b60062e] {\n    width: 100vw;\n    height: 80vw;\n}\n#home-second-slider .second-slide img[data-v-1b60062e] {\n    width: 100vw;\n    height: 80vw;\n}\n#home-second-slider .owl-theme .owl-dots[data-v-1b60062e] {\n    margin: auto;\n    width: 80vw;\n    left: -8rem;\n}\n#home-second-slider .owl-theme .owl-dots .owl-dot span[data-v-1b60062e] {\n    width: 24px;\n    height: 6px;\n    margin: 3px;\n}\n#home-second-slider .owl-theme .owl-nav[data-v-1b60062e] {\n    margin: 1rem -33px -30px auto;\n}\n#home-second-slider .slideshow-box[data-v-1b60062e] {\n    top: auto;\n    width: calc(100vw - 2rem);\n    height: auto;\n    right: auto;\n    margin-left: 1rem;\n    position: relative;\n    margin-top: -20vw;\n}\n}\n@media only screen and (max-width: 767px) {\n#home-second-slider[data-v-1b60062e] {\n    width: 100vw;\n}\n#home-second-slider .smaller-sizes-container[data-v-1b60062e] {\n    width: 100vw;\n    overflow: hidden;\n}\n#home-second-slider .second-slide[data-v-1b60062e] {\n    width: 100vw;\n    height: 80vw;\n}\n#home-second-slider .second-slide img[data-v-1b60062e] {\n    width: 100vw;\n    height: 80vw;\n}\n#home-second-slider .owl-theme .owl-dots[data-v-1b60062e] {\n    margin: auto;\n    width: 80vw;\n    left: -8rem;\n}\n#home-second-slider .owl-theme .owl-dots .owl-dot span[data-v-1b60062e] {\n    width: 24px;\n    height: 6px;\n    margin: 3px;\n}\n#home-second-slider .owl-theme .owl-nav[data-v-1b60062e] {\n    margin: 1rem -33px -30px auto;\n}\n#home-second-slider .slideshow-box[data-v-1b60062e] {\n    top: auto;\n    width: calc(100vw - 2rem);\n    height: auto;\n    right: auto;\n    margin-left: 1rem;\n    position: relative;\n    margin-top: -20vw;\n}\n}\n",""]);

// exports


/***/},

/***/"./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesStyleLoaderIndexJsNode_modulesCssLoaderIndexJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesPostcssLoaderSrcIndexJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHomeSecondSliderVueVueTypeStyleIndex0Id1b60062eLangScssScopedTrue(module,__unused_webpack_exports,__webpack_require__){


var content=__webpack_require__(/*! !!../../../../node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true& */"./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true&");

if(typeof content==='string')content=[[module.id,content,'']];

var transform;



var options={"hmr":true};

options.transform=transform;
options.insertInto=undefined;

var update=__webpack_require__(/*! !../../../../node_modules/style-loader/lib/addStyles.js */"./node_modules/style-loader/lib/addStyles.js")(content,options);

if(content.locals)module.exports=content.locals;

/***/},

/***/"./resources/js/components/Home/HomeSecondSlider.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/Home/HomeSecondSlider.vue ***!
  \***********************************************************/
/***/function resourcesJsComponentsHomeHomeSecondSliderVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _HomeSecondSlider_vue_vue_type_template_id_1b60062e_scoped_true___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./HomeSecondSlider.vue?vue&type=template&id=1b60062e&scoped=true& */"./resources/js/components/Home/HomeSecondSlider.vue?vue&type=template&id=1b60062e&scoped=true&");
/* harmony import */var _HomeSecondSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./HomeSecondSlider.vue?vue&type=script&lang=js& */"./resources/js/components/Home/HomeSecondSlider.vue?vue&type=script&lang=js&");
/* harmony import */var _HomeSecondSlider_vue_vue_type_style_index_0_id_1b60062e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true& */"./resources/js/components/Home/HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
_HomeSecondSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
_HomeSecondSlider_vue_vue_type_template_id_1b60062e_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
_HomeSecondSlider_vue_vue_type_template_id_1b60062e_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
"1b60062e",
null);
component.options.__file="resources/js/components/Home/HomeSecondSlider.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Home/HomeSecondSlider.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/Home/HomeSecondSlider.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/***/function resourcesJsComponentsHomeHomeSecondSliderVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeSecondSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./HomeSecondSlider.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeSecondSlider_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default;

/***/},

/***/"./resources/js/components/Home/HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/components/Home/HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true& ***!
  \*********************************************************************************************************************/
/***/function resourcesJsComponentsHomeHomeSecondSliderVueVueTypeStyleIndex0Id1b60062eLangScssScopedTrue(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeSecondSlider_vue_vue_type_style_index_0_id_1b60062e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/style-loader/index.js!../../../../node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true& */"./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=style&index=0&id=1b60062e&lang=scss&scoped=true&");
/* harmony reexport (unknown) */var __WEBPACK_REEXPORT_OBJECT__={};
/* harmony reexport (unknown) */var _loop=function _loop(__WEBPACK_IMPORT_KEY__){if(__WEBPACK_IMPORT_KEY__!=="default")__WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__]=function(){return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeSecondSlider_vue_vue_type_style_index_0_id_1b60062e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__];};};for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeSecondSlider_vue_vue_type_style_index_0_id_1b60062e_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__){_loop(__WEBPACK_IMPORT_KEY__);}
/* harmony reexport (unknown) */__webpack_require__.d(__webpack_exports__,__WEBPACK_REEXPORT_OBJECT__);


/***/},

/***/"./resources/js/components/Home/HomeSecondSlider.vue?vue&type=template&id=1b60062e&scoped=true&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/components/Home/HomeSecondSlider.vue?vue&type=template&id=1b60062e&scoped=true& ***!
  \******************************************************************************************************/
/***/function resourcesJsComponentsHomeHomeSecondSliderVueVueTypeTemplateId1b60062eScopedTrue(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeSecondSlider_vue_vue_type_template_id_1b60062e_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeSecondSlider_vue_vue_type_template_id_1b60062e_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeSecondSlider_vue_vue_type_template_id_1b60062e_scoped_true___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./HomeSecondSlider.vue?vue&type=template&id=1b60062e&scoped=true& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=template&id=1b60062e&scoped=true&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=template&id=1b60062e&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeSecondSlider.vue?vue&type=template&id=1b60062e&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHomeSecondSliderVueVueTypeTemplateId1b60062eScopedTrue(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _vm.posts.length?
_c("div",{attrs:{id:"home-second-slider"}},[
_c(
"div",
{staticClass:"smaller-sizes-container"},
[
_c(
"carousel",
{
attrs:{
items:1,
margin:0,
center:false,
loop:true,
autoWidth:true,
nav:false,
dots:true,
autoplayTimeout:7000,
autoplayHoverPause:true,
autoplay:true,
"data-aos":"fade-right"},

on:{translate:_vm.callback}},

_vm._l(_vm.posts,function(post,key){
return _c("div",{key:key,staticClass:"second-slide"},[
_c("picture",[
_c("source",{
attrs:{
media:"(min-width: 1500px)",
srcset:post.doublewebp,
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
media:"(min-width: 1500px)",
srcset:post.double,
type:post.mimetype}}),


_vm._v(" "),
_c("source",{
attrs:{
media:"(min-width: 768px)",
srcset:post.normalwebp,
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
media:"(min-width: 768px)",
srcset:post.normal,
type:post.mimetype}}),


_vm._v(" "),
_c("source",{
attrs:{
media:"(min-width: 1px)",
srcset:post.featuredwebp,
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
media:"(min-width: 1px)",
srcset:post.featured,
type:post.mimetype}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy",
attrs:{
src:post.double,
type:post.mimetype,
alt:post.title+" - News - We Are Vertigo"}})])]);




}),
0)],


1),

_vm._v(" "),
_c(
"div",
{
staticClass:
"slideshow-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-left",
attrs:{"data-aos":"fade-left"}},

[
_c("div",{staticClass:"d-table w-100 h-100"},[
_c(
"div",
{staticClass:"d-table-cell align-middle w-100 h-100"},
[
_c("p",{staticClass:"box-title-top text-grey"},[
_vm._v(
"News - "+
_vm._s(_vm.niceDate(_vm.posts[_vm.current].created_at)))]),


_vm._v(" "),
_c("p",{staticClass:"mimic-h1 pr-5 mob-px-0"},[
_vm._v(_vm._s(_vm.posts[_vm.current].title))]),

_vm._v(" "),
_c("p",{staticClass:"text-large mb-4"},[
_vm._v(_vm._s(_vm.posts[_vm.current].excerpt))]),

_vm._v(" "),
_c("p",{staticClass:"mb-0"},[
_c(
"a",
{
staticClass:"d-inline",
attrs:{
href:
"/news/"+
_vm.moment(_vm.posts[_vm.current].created_at)+
"/"+
_vm.posts[_vm.current].slug}},


[_vm._m(0)])]),


_vm._v(" "),
_vm._m(1)])])])]):






_vm._e();
};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"button",
{
staticClass:"btn btn-black btn-icon mob-mt-2",
attrs:{type:"button"}},

[
_vm._v("Read Article "),
_c("i",{staticClass:"custom-icon chevron-double-right-white"})]);


},
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"p",
{staticClass:"button-after button-after-dark mt-1 pl-2 mob-mb-0"},
[
_c("a",{attrs:{href:"/news"}},[
_c("i",{staticClass:"return-arrow"}),
_vm._v(" All News")])]);



}];

_render._withStripped=true;



/***/}}]);

}());
