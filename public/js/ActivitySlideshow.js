(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["ActivitySlideshow"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsActivitiesActivitySlideshowVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! vue-owl-carousel */"./node_modules/vue-owl-carousel/dist/vue-owl-carousel.js");
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
components:{
carousel:vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default()},

props:{
slides:Array},

data:function data(){
return {
videos:[],
errors:{},
success:false,
loaded:true};

}};


/***/},

/***/"./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesCssLoaderIndexJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesPostcssLoaderSrcIndexJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsActivitiesActivitySlideshowVueVueTypeStyleIndex0LangScss(module,exports,__webpack_require__){

exports=module.exports=__webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */"./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.id,"#activity-slideshow {\n  width: 100%;\n  max-width: 1110px;\n}\n#activity-slideshow .activity-slide {\n  position: relative;\n  width: 1110px;\n  max-width: 1110px;\n  background-repeat: no-repeat;\n  background-position: center center;\n  background-size: cover;\n  border-radius: 0;\n  text-align: left;\n  vertical-align: top;\n  overflow: hidden;\n  cursor: pointer;\n}\n#activity-slideshow .activity-slide img {\n  max-width: 1110px;\n  z-index: 1;\n  position: relative;\n  width: 100%;\n  display: block;\n  margin-left: -1rem;\n  -webkit-box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n  -moz-box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n  box-shadow: 0rem 0rem 0.5rem rgba(0, 0, 0, 0.4) !important;\n}\n#activity-slideshow .owl-theme .owl-nav {\n  margin-top: 1rem;\n  margin-bottom: -30px;\n  position: absolute;\n  bottom: -4rem;\n  left: calc(50% - 191px);\n}\n#activity-slideshow .owl-theme .owl-nav .owl-prev {\n  margin-right: 318px;\n  font-size: 0;\n  position: relative;\n  color: #1D252D;\n  padding: 0;\n  width: 25px;\n  height: 33px;\n  background-color: transparent;\n}\n#activity-slideshow .owl-theme .owl-nav .owl-prev:after {\n  position: absolute;\n  content: \"\";\n  right: 0;\n  top: 0;\n  background-image: url(\"/img/icons/chevron-left.svg\");\n  background-size: contain;\n  width: 25px;\n  height: 33px;\n  background-repeat: no-repeat;\n}\n#activity-slideshow .owl-theme .owl-nav .owl-next {\n  font-size: 0;\n  position: relative;\n  color: #1D252D;\n  padding: 0;\n  width: 25px;\n  height: 33px;\n  background-color: transparent;\n}\n#activity-slideshow .owl-theme .owl-nav .owl-next:after {\n  position: absolute;\n  content: \"\";\n  left: 0;\n  top: 0;\n  background-image: url(\"/img/icons/chevron-right.svg\");\n  background-size: contain;\n  width: 25px;\n  height: 33px;\n  background-repeat: no-repeat;\n}\n#activity-slideshow .owl-theme .owl-dots {\n  width: 300px;\n  position: absolute;\n  bottom: -5.5rem;\n  left: calc(50% - 150px);\n}\n#activity-slideshow .owl-theme .owl-dots .owl-dot span {\n  width: 30px;\n  height: 8px;\n  background: #fff;\n  border-radius: 0;\n}\n#activity-slideshow .owl-theme .owl-dots .owl-dot.active span {\n  background: #F6CF3C;\n}\n@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) {\n#activity-slideshow {\n    width: calc(100vw - 2rem);\n}\n#activity-slideshow.ipadp-pr-4 {\n    padding-right: 2rem;\n}\n#activity-slideshow.backdrop .backdrop-back {\n    width: calc(100% - 3rem) !important;\n}\n#activity-slideshow .activity-slide {\n    width: calc(100vw - 1.5rem);\n    height: 54vw;\n}\n#activity-slideshow .activity-slide img {\n    width: calc(100vw - 1.5rem) !important;\n    height: auto;\n}\n#activity-slideshow .owl-theme .owl-dots {\n    margin: auto;\n    width: 52vw;\n    left: calc(50% - 24vw);\n}\n#activity-slideshow .owl-theme .owl-dots .owl-dot span {\n    width: 24px;\n    height: 6px;\n    margin: 3px;\n}\n#activity-slideshow .owl-theme .owl-nav {\n    margin: auto;\n    width: 100%;\n    bottom: -6rem;\n    left: 0.5rem;\n}\n#activity-slideshow .owl-theme .owl-nav .owl-prev {\n    margin-right: 70vw;\n}\n}\n@media only screen and (max-width: 767px) {\n#activity-slideshow {\n    width: calc(100vw - 2rem);\n}\n#activity-slideshow .activity-slide {\n    width: calc(100vw - 1.5rem);\n    height: 54vw;\n}\n#activity-slideshow .activity-slide img {\n    width: calc(100vw - 1.5rem) !important;\n    height: auto;\n}\n#activity-slideshow .owl-theme .owl-dots {\n    margin: auto;\n    width: 52vw;\n    left: calc(50% - 24vw);\n}\n#activity-slideshow .owl-theme .owl-dots .owl-dot span {\n    width: 24px;\n    height: 6px;\n    margin: 3px;\n}\n#activity-slideshow .owl-theme .owl-nav {\n    margin: auto;\n    width: 100%;\n    bottom: -6rem;\n    left: 0.5rem;\n}\n#activity-slideshow .owl-theme .owl-nav .owl-prev {\n    margin-right: 70vw;\n}\n}\n",""]);

// exports


/***/},

/***/"./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesStyleLoaderIndexJsNode_modulesCssLoaderIndexJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesPostcssLoaderSrcIndexJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsActivitiesActivitySlideshowVueVueTypeStyleIndex0LangScss(module,__unused_webpack_exports,__webpack_require__){


var content=__webpack_require__(/*! !!../../../../node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ActivitySlideshow.vue?vue&type=style&index=0&lang=scss& */"./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=style&index=0&lang=scss&");

if(typeof content==='string')content=[[module.id,content,'']];

var transform;



var options={"hmr":true};

options.transform=transform;
options.insertInto=undefined;

var update=__webpack_require__(/*! !../../../../node_modules/style-loader/lib/addStyles.js */"./node_modules/style-loader/lib/addStyles.js")(content,options);

if(content.locals)module.exports=content.locals;

/***/},

/***/"./resources/js/components/Activities/ActivitySlideshow.vue":
/*!******************************************************************!*\
  !*** ./resources/js/components/Activities/ActivitySlideshow.vue ***!
  \******************************************************************/
/***/function resourcesJsComponentsActivitiesActivitySlideshowVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _ActivitySlideshow_vue_vue_type_template_id_c11e48f0___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./ActivitySlideshow.vue?vue&type=template&id=c11e48f0& */"./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=template&id=c11e48f0&");
/* harmony import */var _ActivitySlideshow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./ActivitySlideshow.vue?vue&type=script&lang=js& */"./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=script&lang=js&");
/* harmony import */var _ActivitySlideshow_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./ActivitySlideshow.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
_ActivitySlideshow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
_ActivitySlideshow_vue_vue_type_template_id_c11e48f0___WEBPACK_IMPORTED_MODULE_0__.render,
_ActivitySlideshow_vue_vue_type_template_id_c11e48f0___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Activities/ActivitySlideshow.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/***/function resourcesJsComponentsActivitiesActivitySlideshowVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivitySlideshow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ActivitySlideshow.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivitySlideshow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default;

/***/},

/***/"./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************************/
/***/function resourcesJsComponentsActivitiesActivitySlideshowVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivitySlideshow_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/style-loader/index.js!../../../../node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ActivitySlideshow.vue?vue&type=style&index=0&lang=scss& */"./node_modules/style-loader/index.js!./node_modules/css-loader/index.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=style&index=0&lang=scss&");
/* harmony reexport (unknown) */var __WEBPACK_REEXPORT_OBJECT__={};
/* harmony reexport (unknown) */var _loop=function _loop(__WEBPACK_IMPORT_KEY__){if(__WEBPACK_IMPORT_KEY__!=="default")__WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__]=function(){return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivitySlideshow_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__];};};for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivitySlideshow_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__){_loop(__WEBPACK_IMPORT_KEY__);}
/* harmony reexport (unknown) */__webpack_require__.d(__webpack_exports__,__WEBPACK_REEXPORT_OBJECT__);


/***/},

/***/"./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=template&id=c11e48f0&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=template&id=c11e48f0& ***!
  \*************************************************************************************************/
/***/function resourcesJsComponentsActivitiesActivitySlideshowVueVueTypeTemplateIdC11e48f0(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivitySlideshow_vue_vue_type_template_id_c11e48f0___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivitySlideshow_vue_vue_type_template_id_c11e48f0___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ActivitySlideshow_vue_vue_type_template_id_c11e48f0___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ActivitySlideshow.vue?vue&type=template&id=c11e48f0& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=template&id=c11e48f0&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=template&id=c11e48f0&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Activities/ActivitySlideshow.vue?vue&type=template&id=c11e48f0& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsActivitiesActivitySlideshowVueVueTypeTemplateIdC11e48f0(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"div",
{staticClass:"backdrop ipadp-pr-4",attrs:{id:"activity-slideshow"}},
[
_c(
"div",
{staticClass:"backdrop-content"},
[
_c(
"carousel",
{
attrs:{
items:4,
margin:30,
center:false,
autoHeight:true,
autoWidth:true,
nav:true,
dots:true}},


_vm._l(_vm.slides,function(slide,key){
return _c("div",{key:key,staticClass:"activity-slide"},[
slide.video?
_c(
"div",
{
staticClass:"embed-responsive embed-responsive-16by9"},

[
_c("iframe",{
staticClass:"embed-responsive-item",
attrs:{src:slide.video,allowfullscreen:""}})]):



_c("div",[
_c("picture",[
_c("source",{
attrs:{
media:"(min-width: 1500px)",
srcset:slide.doublewebp,
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
media:"(min-width: 1500px)",
srcset:slide.double,
type:slide.mimetype}}),


_vm._v(" "),
_c("source",{
attrs:{
media:"(min-width: 768px)",
srcset:slide.normalwebp,
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
media:"(min-width: 768px)",
srcset:slide.normal,
type:slide.mimetype}}),


_vm._v(" "),
_c("source",{
attrs:{
media:"(min-width: 1px)",
srcset:slide.featuredwebp,
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
media:"(min-width: 1px)",
srcset:slide.featured,
type:slide.mimetype}}),


_vm._v(" "),
_c("img",{
staticClass:"w-100 lazy",
attrs:{src:slide.featured,alt:"Slide "+key}})])])]);




}),
0)],


1),

_vm._v(" "),
_c("div",{
staticClass:"backdrop-back",
attrs:{"data-aos":"fade-down-right"}})]);



};
var _staticRenderFns=[];
_render._withStripped=true;



/***/}}]);

}());
