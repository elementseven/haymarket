(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["HomeActivitiesMenu"],{

/***/"./resources/js/components/Home/HomeActivitiesMenu.vue":
/*!*************************************************************!*\
  !*** ./resources/js/components/Home/HomeActivitiesMenu.vue ***!
  \*************************************************************/
/***/function resourcesJsComponentsHomeHomeActivitiesMenuVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _HomeActivitiesMenu_vue_vue_type_template_id_3ff17240___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./HomeActivitiesMenu.vue?vue&type=template&id=3ff17240& */"./resources/js/components/Home/HomeActivitiesMenu.vue?vue&type=template&id=3ff17240&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script={}


/* normalize component */;

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__.default)(
script,
_HomeActivitiesMenu_vue_vue_type_template_id_3ff17240___WEBPACK_IMPORTED_MODULE_0__.render,
_HomeActivitiesMenu_vue_vue_type_template_id_3ff17240___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Home/HomeActivitiesMenu.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Home/HomeActivitiesMenu.vue?vue&type=template&id=3ff17240&":
/*!********************************************************************************************!*\
  !*** ./resources/js/components/Home/HomeActivitiesMenu.vue?vue&type=template&id=3ff17240& ***!
  \********************************************************************************************/
/***/function resourcesJsComponentsHomeHomeActivitiesMenuVueVueTypeTemplateId3ff17240(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeActivitiesMenu_vue_vue_type_template_id_3ff17240___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeActivitiesMenu_vue_vue_type_template_id_3ff17240___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HomeActivitiesMenu_vue_vue_type_template_id_3ff17240___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./HomeActivitiesMenu.vue?vue&type=template&id=3ff17240& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeActivitiesMenu.vue?vue&type=template&id=3ff17240&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeActivitiesMenu.vue?vue&type=template&id=3ff17240&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home/HomeActivitiesMenu.vue?vue&type=template&id=3ff17240& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsHomeHomeActivitiesMenuVueVueTypeTemplateId3ff17240(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _vm._m(0);
};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"div",
{staticClass:"container container-wide position-relative z-2"},
[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"container ml-0"},[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"col-12"},[
_c(
"div",
{
staticClass:
"card home-menu-card bg-primary text-dark py-3",
attrs:{"data-aos":"slide-up"}},

[
_c("div",{staticClass:"container"},[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"col-lg-5"},[
_c(
"p",
{staticClass:"mimic-h3 mb-2 text-center"},
[_vm._v("Titanic Park")]),

_vm._v(" "),
_c("div",{staticClass:"row text-center"},[
_c("div",{staticClass:"col-4"},[
_c("img",{
attrs:{
src:"/img/icons/indoor-skydiving.svg",
width:"80",
alt:"Indoor Skydiving Belfast icon"}}),


_vm._v(" "),
_c("p",{staticClass:"title mt-2"},[
_vm._v("Indoor Skydiving"),
_c("br"),
_vm._v("(Ages 4+)")])]),


_vm._v(" "),
_c("div",{staticClass:"col-4"},[
_c("img",{
attrs:{
src:"/img/icons/inflatable.svg",
width:"80",
alt:"Inflatable Park Belfast icon"}}),


_vm._v(" "),
_c("p",{staticClass:"title mt-2"},[
_vm._v("Inflatable Park"),
_c("br"),
_vm._v("(Ages 1+)")])]),


_vm._v(" "),
_c("div",{staticClass:"col-4"},[
_c("img",{
attrs:{
src:"/img/icons/ninja.svg",
width:"80",
alt:"Ninja master course Belfast icon"}}),


_vm._v(" "),
_c("p",{staticClass:"title mt-2"},[
_vm._v("Ninja Course"),
_c("br"),
_vm._v("(Ages 6+)")])])])]),




_vm._v(" "),
_c("div",{staticClass:"col-lg-4 border-left"},[
_c(
"p",
{staticClass:"mimic-h3 mb-2 text-center"},
[_vm._v("Newtownbreda")]),

_vm._v(" "),
_c("div",{staticClass:"row text-center"},[
_c("div",{staticClass:"col-6"},[
_c("img",{
attrs:{
src:"/img/icons/inflatable.svg",
width:"80",
alt:"Inflatable Park Belfast icon"}}),


_vm._v(" "),
_c("p",{staticClass:"title mt-2"},[
_vm._v("Inflatable Park"),
_c("br"),
_vm._v("(Ages 1+)")])]),


_vm._v(" "),
_c("div",{staticClass:"col-6"},[
_c("img",{
attrs:{
src:"/img/icons/adventure.svg",
width:"80",
alt:"Ninja master course Belfast icon"}}),


_vm._v(" "),
_c("p",{staticClass:"title mt-2"},[
_vm._v("Adventure"),
_c("br"),
_vm._v("(Ages 1-12)")])])])]),




_vm._v(" "),
_c(
"div",
{
staticClass:
"col-lg-3 py-3 border-left px-4 pt-5 text-center"},

[
_c("a",{attrs:{href:"/cargo"}},[
_c("img",{
attrs:{
src:"/img/logos/cargo-burger.svg",
width:"120",
alt:
"Cargo by Vertigo resteraunt food logo with burger Belfast Northern Ireland"}})])])])])])])])])])]);















}];

_render._withStripped=true;



/***/}}]);

}());
