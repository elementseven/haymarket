<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;
use Nova;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('web')->group(function () {

	// Main pages
  Route::get('/booking-confirmed', function () {
	  return view('bookingConfirmed');
	});

	Route::get('/new-home', [PageController::class, 'newHome'])->name('new-home');
	Route::get('/the-courtyard', [PageController::class, 'theCourtyard'])->name('the-courtyard');
	Route::get('/the-armoury', [PageController::class, 'theArmoury'])->name('the-armoury');
	Route::get('/the-armoury/info', [PageController::class, 'theArmouryInfo'])->name('the-armoury-info');

	Route::get('/the-stock-exchange', [PageController::class, 'theStockExchange'])->name('the-stock-exchange');
	Route::get('/the-stock-exchange/virtual-darts', [PageController::class, 'virtualDarts'])->name('the-stock-exchange.virtual-darts');
	Route::get('/the-stock-exchange/stock-market-bar', [PageController::class, 'stockMarketBar'])->name('the-stock-exchange.stock-market-bar');
	Route::get('/the-stock-exchange/shuffle-board', [PageController::class, 'shuffleBoard'])->name('the-stock-exchange.shuffle-board');

	Route::get('/', [PageController::class, 'welcome'])->name('welcome');
	Route::get('/resdiary', [PageController::class, 'resdiary'])->name('resdiary');
	Route::get('/menu', [MenuController::class, 'show'])->name('menu');
	Route::get('/food-menu', [MenuController::class, 'food'])->name('food.menu');
	Route::get('/cocktail-menu', [MenuController::class, 'cocktail'])->name('cocktail.menu');
	Route::get('/brunch-menu', [MenuController::class, 'brunch'])->name('brunch.menu');
	Route::get('/stock-food-menu', [MenuController::class, 'stockFood'])->name('stock.food.menu');
	Route::get('/stock-drinks-menu', [MenuController::class, 'stockDrinks'])->name('stock.drinks.menu');
	Route::get('/armoury-food-menu', [MenuController::class, 'armouryFood'])->name('armoury.food.menu');
	Route::get('/armoury-drinks-menu', [MenuController::class, 'armouryDrinks'])->name('armoury.drinks.menu');
	Route::get('/drinks-menu', [DrinksMenuController::class, 'show'])->name('drinks-menu');
	Route::get('/parties', [PageController::class, 'parties'])->name('parties');
	Route::get('/contact', [PageController::class, 'contact'])->name('contact');
	Route::get('/privacy-policy', [PageController::class, 'privacyPolicy'])->name('privacy-policy');
	Route::get('/terms-and-conditions', [PageController::class, 'tandcs'])->name('tandcs');
	Route::get('/work-for-us', [PageController::class, 'workForUs'])->name('work-for-us');
	Route::get('/vouchers', [PageController::class, 'vouchers'])->name('vouchers');
	// Route::get('/christmas-vouchers', [PageController::class, 'xmasvouchers'])->name('xmasvouchers');

	// FAQs
	Route::get('/faqs', [FaqController::class, 'index'])->name('faqs');
	Route::get('/faqs/get/{faqcategory}', [FaqController::class, 'get'])->name('faqs.get');

	// News
	Route::get('/news', [NewsController::class, 'index'])->name('news.index');
	Route::get('/news/get', [NewsController::class, 'get'])->name('news.get');
	Route::get('/news/{date}/{slug}', [NewsController::class, 'show'])->name('news.show');

	// Instagram feed
	Route::get('/instagram', [PageController::class, 'instagram'])->name('instagram');
	
	// Offers & Events 
	Route::get('/offers', [OfferController::class, 'index'])->name('offers.index');
	Route::get('/offers/get', [OfferController::class, 'get'])->name('offers.get');
	Route::get('/win-or-booze', [PageController::class, 'winOrBooze'])->name('winOrBooze');
	Route::get('/throwback-thursday', [PageController::class, 'throwbackThursday'])->name('throwbackThursday');
	Route::get('/fridays', [PageController::class, 'fridays'])->name('fridays');
	Route::get('/saturdays', [PageController::class, 'saturdays'])->name('saturdays');
	Route::get('/peep-show', [PageController::class, 'peepShow'])->name('peepShow');
	Route::get('/bottomless-brunch-karaoke', [PageController::class, 'karaoke'])->name('karaoke');
	Route::get('/select-your-brunch', [PageController::class, 'selectBrunch'])->name('selectBrunch');
	Route::get('/bottomless-brunch', [PageController::class, 'bottomlessBrunchnew'])->name('bottomlessBrunch');
	Route::get('/bottomless-brunch-new', [PageController::class, 'bottomlessBrunchnew'])->name('bottomlessBrunchnew');
	Route::get('/boozy-brunch-club', function(){
		return redirect()->to('/bottomless-brunch');
	});
	Route::get('/january-saver-brunch', function(){
		return view('offers.january-saver-brunch');
	})->name('january-saver-brunch');
	Route::get('/mothers-day', [PageController::class, 'mothersday'])->name('mothersday');
	Route::get('/st-patricks-day', [PageController::class, 'patricks'])->name('patricks');
	Route::get('/six-nations', [PageController::class, 'sixNations'])->name('offers.six-nations');
	Route::get('/easter', [PageController::class, 'easter'])->name('easter');
	Route::get('/offers/{offer}/{slug}', [OfferController::class, 'show'])->name('offers.show');

	// Student Deals
	// Route::get('/student-deals', [PageController::class, 'studentDeals'])->name('student-deals');
	Route::get('/student-deals/student-brunch', [PageController::class, 'studentBrunch'])->name('student-brunch');
	// Route::get('/student-deals/student-quiz-night', [PageController::class, 'studentQuiz'])->name('student-quiz');

	// Bottomless Brunch Takeovers
	Route::get('/bottomless-brunch/takeovers', [PageController::class, 'takeovers'])->name('offers.takeovers-bottomless-brunch');
	Route::get('/bottomless-brunch/takeovers/lady-gaga-general-admission', [PageController::class, 'ladyGen'])->name('offers.lady-gaga-bottomless-brunch-gen');
	Route::get('/bottomless-brunch/takeovers/lady-gaga', [PageController::class, 'ladygaga'])->name('offers.lady-gaga-bottomless-brunch');
	Route::get('/bottomless-brunch/takeovers/the-iconettes', [PageController::class, 'iconettes'])->name('offers.cher-bottomless-brunch');
		Route::get('/bottomless-brunch/takeovers/the-iconettes-general-admission', [PageController::class, 'iconettesgen'])->name('offers.iconettesgen-bottomless-brunch');
	Route::get('/bottomless-brunch/takeovers/michael-buble', [PageController::class, 'buble'])->name('offers.abba-bottomless-brunch');
	Route::get('/bottomless-brunch/takeovers/michael-buble-general-admission', [PageController::class, 'bublegen'])->name('offers.abba-bottomless-brunch');
	Route::get('/bottomless-brunch/takeovers/abba', [PageController::class, 'abba'])->name('offers.lady-gaga-bottomless-brunch');
	Route::get('/bottomless-brunch/takeovers/shania-twain', [PageController::class, 'shania'])->name('offers.shania-twain-bottomless-brunch');
	Route::get('/bottomless-brunch/takeovers/taylor-made', [PageController::class, 'taylor'])->name('offers.taylor-made-bottomless-brunch');
	Route::get('/bottomless-brunch/takeovers/lust-in-music', [PageController::class, 'lust'])->name('offers.lust-in-music');
	Route::get('/bottomless-brunch/takeovers/jonny-brookes', [PageController::class, 'brookes'])->name('offers.brookes');
});

// Send enquiry form
Route::post('/send-message', [SendMail::class, 'enquiry'])->name('send-message');
Route::post('/send-party-enquiry', [SendMail::class, 'partyEnquiry'])->name('send-party-enquiry');
Route::post('/send-armoury-enquiry', [SendMail::class, 'armouryEnquiry'])->name('send-armoury-enquiry');
Route::post('/mailinglist', [SendMail::class, 'mailingListSignup'])->name('mailing-list');
Route::post('/send-christmas-booking-request', [SendMail::class, 'christmasEnquiry'])->name('send-christmas-request');
Route::post('/submit-cv', [ApplicationController::class, 'store'])->name('submit-cv');

// Logout
Route::get('/logout', function(){
	Auth::logout();
	return redirect()->to('/login');
});

Route::get('/christmas', [PageController::class, 'christmas'])->name('christmas');


Nova::routes();