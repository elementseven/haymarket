@php
$page = 'The Armoury';
$pagetitle = "The Armoury @ Haymarket - Belfast's vibrant indoor/outdoor bar & street food hangout";
$metadescription = "Interactive shooting gallery using state-of-the-art technology. With loads of different game modes, we’ll keep you and your squad entertained all night long with refreshments from our bar & kitchen.";
$pagetype = 'armoury';
$pagename = 'armoury';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative z-1 pt-5 mt-5 mob-mt-0 text-center">
  <img src="/img/logos/the-armoury.svg" class="mb-3 h-auto pt-5 mt-5 mob-mt-0 w-100 mob-px-4" alt="The Armoury - Haymarket Belfast Logo" width="500" height="114" style="max-width: 500px" />
  <picture>
    <source srcset="/img/graphics/guns.webp" type="image/webp"/> 
    <source srcset="/img/graphics/guns.png" type="image/png"/> 
    <img src="/img/graphics/guns.png" type="image/png" width="550" height="1156" alt="The Armoury - Haymarket Belfast" class="guns-top-right h-auto"/>
  </picture>
</header>
@endsection
@section('content')
<div class="container pb-5 ipadp-pt-0 position-relative z-2">
  <div class="row text-center text-lg-left pt-5 mob-pt-0">
    <div class="col-12 text-center mb-5">
      <div class="pre-title-lines mb-4 mt-5 mx-auto"></div>
      <h1 class="">How it works</h1>
    </div>
    <div class="col-12 pt-2 pl-4 mob-px-3 text-center text-lg-left">
      <h3 class="mb-4">From Booking to booth</h3>
      <ul class="text-left">
        <li class="mb-3"><p>To enquire about bookings for larger groups, corporate events and private hire options, please complete our booking enquiry form below, telling us as much as possible about your group event, party or celebration so we can design the best package for you!</p></li>
        <li class="mb-3"><p>Make sure to add your desired date and time-slot so we can ensure to check the booking availability. Please add into the notes if you have any alternative dates or times.</p></li>
        <li class="mb-3"><p>Our team will come back to you via email or phone-call to confirm availability, chat through time slots and ensure that they have all the relevant details. They will then send over a payment link to confirm your booking with key pre-visit information.</p></li>
        <li class="mb-3"><p class="mb-0">You're ready to start shooting!</p></li>
      </ul>
     {{--  <ul class="text-left">
        <li class="mb-3">
          <p class="mb-0">For groups of up to 12 guests, you can use our Book Now function on our website. </p>
        </li>
        <li class="mb-3">
          <p class="mb-0">For larger groups, corporate events and private hire options, please complete our booking enquiry form below, telling us as much as possible about your group event, party or celebration so we can design the best package for you!</p>
        </li>
        <li class="mb-3">
          <p class="mb-0">Make sure to add your desired date and time-slot so we can ensure to check the booking availability. Please do add into the notes if you there are any alternative dates and times</p>
        </li>
        <li class="mb-3">
          <p class="mb-0">Our team will come back to you via email or phone-call to confirm availability, chat through time slots and ensure that they have all the relevant details. They will then send over a payment link to confirm your booking and send over key pre-visit information. </p>
        </li>
        <li class="mb-3">
          <p class="mb-0">You're ready to start shooting!</p>
        </li>
      </ul> --}}
      <button type="button" class="btn btn-primary mt-3 booknowbtn">Enquire Now</button>
    </div>
  </div>
  <div class="row pt-5 mt-5">
    <div class="col-12 pt-2 pl-4 mob-px-3 text-center text-lg-left">
      <h3 class="mb-4">On The Day:</h3>
      <ul class="text-left">
        <li><p>We ask all guests to arrive at the venue (Haymarket Belfast, 84 Royal Avenue) 30 minutes prior to the start time to ensure that you are ready to ‘fire away’ as soon as your session starts (and maybe have a cheeky cocktail to get started!)</p></li>
        <li><p>Our gun-hosts will take you to your dedicated booth and run through the short demo which includes how to use our guns and technology, what is in your personal armoury and how to master the skill of precision shooting.</p></li>
        <li><p>Our gun-hosts will unlock the armoury and issue you with your hand pistol and rifle which is included in your package. You will be able to upgrade your guns at this point, to include the pump shotgun or M4 if you would like, for just £20 for the entire group.</p></li>
        <li><p>A bank card must be provided by the lead booker to be secured locked in the armoury, which will be released once your booking is complete and all guns are returned safely.</p></li>
        <li><p>Now it is time to start shooting! You and your competitors will take it in turns 1-by-1 to shoot out against our amazing simulations. Our serving team will be on hand to take a food and drinks order, delivered straight to your table.</p></li>
        <li><p>Once finished, our gun host will do a quick debrief, lock up the guns and retrieve your secured bank card. You will then be able to move out into ‘The Haymarket Courtyard’ to enjoy some more post-activity drinks.</p></li>
      </ul>
    <button type="button" class="btn btn-primary mt-3 booknowbtn">Enquire Now</button>
    </div>
  </div>
  {{-- <div class="row my-5">
    <div class="col-12 pt-2 pl-4 mob-px-3 text-center text-lg-left">
      <h3 class="mb-4">Some Important Information:</h3>
      <ul class="text-left">
        <li><p>Each booking includes a private shooting range, with luxurious booth seating, private armoury and 9ft screens which can hold up to 6 guests at a time. Multiple booths can be booked for larger groups. Each package includes 2 standard guns (hand pistol and shotgun) with gun upgrades available on the day.</p></li>
        <li><p>A bank card must be provided by the lead booker to be secured locked in the armoury, which will be released once your booking is complete. There is no charge placed on this card, it is purely for gun security.</p></li>
        <li><p>Our team takes great care in the safety and controlling of simulated pistols, with each fire-arm being electronically tagged and locked within The Armoury at the start and end of each session and must not be removed from the booth at any time. The venue is fitted with an electronic tagging system and CCTV monitioring so they are extremely well guarded so under no circumstance are they to be removed from The Armoury venue.</p></li>
        <li><p>Please note that we do not allow any caps, tracksuits, tracksuit bottoms or any offensive fancy dress.</p></li>
        <li><p>Whilst this is an extremely safe activity, we ask all guests to be respectful, abide by the rules set out by the gun-hosts and be sensible whilst having fun. Our guns are delicate pieces of kit so make sure to treat the venue, our amazing staff and our high-tech equipment with respect.</p></li>
        <li><p>Food and drinks can be ordered directly to your table - please note that these must be kept on the table in our booth at all times and not brought into the shooting area (your gun host will run you through all of this!).</p></li>
        <li><p>Please note that currently all sessions are for ages 18+. We will soon be launching dedicated day-time family friendly sessions over school holidays, so make sure to subscribe to our newsletter to be the first to hear about these sessions!</p></li>
      </ul>
      <button type="button" class="btn btn-primary mt-3 booknowbtn">Enquire Now</button>
    </div>
  </div> --}}
</div>
<armoury-faqs :categories="{{$categories}}"></armoury-faqs>
<div class="container mt-4">
  <p class="text-small">Our 'COMEAGAIN50' offer cannot be used in conjunction with any other offer, child friendly sessions, 3for2 or Fri-Sun bookings</p>
</div>
<div id="bookonline" class="container-fluid position-relative pt-5 z-1 mob-pt-4 mob-pb-0 ipadp-py-0">
  <div class="row pt-5 mob-pt-0">
    <div class="container py-5 position-relative z-2 mob-pb-0">
      <div class="row">
        <div class="col-12 pb-4 mob-px-4">
          <div class="pre-title-lines mob-mx-auto mb-4"></div>
          <p class="mimic-h3 text-center text-lg-left mb-3">Enquire now</p>
          <armoury-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></armoury-form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="py-5 mt-5 mob-py-0"></div>
@endsection
@section('modals')
{{-- <div class="modal fade mx-auto" id="midweekModal" tabindex="-1" role="dialog" aria-labelledby="midweekModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content text-center">
      <picture>
        <source srcset="/img/christmas/newyears.webp?v=2023-12-19" type="image/webp"/> 
        <source srcset="/img/christmas/newyears.jpg?v=2023-12-19" type="image/jpg"/> 
        <img src="/img/christmas/newyears.jpg?v=2023-12-19" width="507" height="507" type="image/jpg" alt="Bottomless Brunch Takeovers - Haymarket Belfast" class="mw-100 h-auto d-inline-block" />
      </picture>
      <a href="/christmas/newyears">
        <button type="button" class="btn btn-primary mx-auto mt-3">Book Now</button>
      </a>
      <div class="btn-text cursor-pointer text-center d-block w-100 mt-2 position-relative z-200" data-dismiss="modal" aria-label="Close Modal">[X] Close</div>
    </div>
  </div>
</div> --}}
@endsection
@section('scripts')
{{--  <script>
  if (!window.location.hash.length){
    $('#midweekModal').modal('show');
  }
  </script> --}}
  <input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/26681?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('#midweekModal').modal('hide');
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });

</script>
@endsection