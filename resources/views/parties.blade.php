@php
$page = 'Homepage';
$pagetitle = "Parties at Haymarket Belfast | Belfast's Newest Outdoor Restaurant";
$metadescription = "Got a party to organise? Then we've got you! Whether it's an intimate birthday celebration for you and your friends, a team building night out or celebration of any kind we're here to make it memorable for you!";
$pagetype = 'light';
$pagename = 'parties';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<picture>
  <source srcset="/img/graphics/burger-beer.webp" type="image/webp"/> 
  <source srcset="/img/graphics/burger-beer.png" type="image/jpeg"/> 
  <img src="/img/graphics/burger-beer.png" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy burger-beer-top-left"/>
</picture>
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center py-5 mob-pt-0 ipadp-pt-0">
        <div class="col-12 text-center mob-px-4 mob-mb-3">
          <div class="pre-title-lines mx-auto mb-4 mob-my-45 mt-5"></div>
          <h1 class="mb-4 mob-mb-0">Private Hire Packages</h1>
        </div><!-- end col -->
        <div class="col-lg-4 pt-5 text-center">
          <div class="mb-4">
            <a href="/docs/parties.pdf?v=2025-01-10" target="_blank">
              <picture>
                <source srcset="/img/venues/squares/courtyard.webp" type="image/webp"/> 
                <source srcset="/img/venues/squares/courtyard.jpg" type="image/jpeg"/> 
                <img src="/img/venues/squares/courtyard.jpg" type="image/jpeg" alt="Courtyard Parties Menu - Haymarket Belfast" class="w-100 h-auto cursor-pointer position-relative z-2" style="-webkit-box-shadow: 2px 10px 25px -9px rgba(213,65,72,0.6); -moz-box-shadow: 2px 10px 25px -9px rgba(213,65,72,0.4); box-shadow: 2px 10px 25px -9px rgba(213,65,72,0.4);"/>
              </picture>
            </a>
            <a href="/docs/parties.pdf?v=2025-01-10" target="_blank">
              <button class="btn btn-primary mb-2 mt-3 px-0" style="min-width: 250px;" type="button">Download Brochure</button>
            </a>
            <!-- <p class="text-small mb-5">*From 1st Nov - 1st Jan we will move to our Christmas brochure for private hire bookings. This is available to view on our <a href="/christmas">Christmas page</a>.</p> -->
          </div> 
        </div><!-- end col -->
        <div class="col-lg-4 pt-5 text-center">
          <div class="mb-4">
            <a href="/docs/armoury-packages.pdf?v=2024-06-20" target="_blank">
              <picture>
                <source srcset="/img/venues/squares/armoury.webp" type="image/webp"/> 
                <source srcset="/img/venues/squares/armoury.jpg" type="image/jpeg"/> 
                <img src="/img/venues/squares/armoury.jpg" type="image/jpeg" alt="Armoury Parties Menu - Haymarket Belfast" class="w-100 h-auto cursor-pointer position-relative z-2" style="-webkit-box-shadow: 2px 10px 25px -9px rgba(158,130,85,1); -moz-box-shadow: 2px 10px 25px -9px rgba(158,130,85,0.4); box-shadow: 2px 10px 25px -9px rgba(158,130,85,0.4);"/>
              </picture>
            </a>
            <a href="/docs/armoury-packages.pdf?v=2024-06-20" target="_blank">
              <button class="btn btn-gold mb-2 mt-3 px-0" style="min-width: 250px;" type="button">Download Brochure</button>
            </a>
          </div>
        </div><!-- end col -->

        <div class="col-lg-4 pt-5 text-center">
          <div class="mb-4">
            <a href="/docs/groups-stock-exchange.pdf?v=2025-01-08-v2" target="_blank">
              <picture>
                <source srcset="/img/venues/squares/stock.webp" type="image/webp"/> 
              <source srcset="/img/venues/squares/stock.jpg" type="image/jpeg"/> 
              <img src="/img/venues/squares/stock.jpg" type="image/jpeg" alt="Stock Exchange Parties Menu - Haymarket Belfast" class="w-100 h-auto cursor-pointer position-relative z-2" style="-webkit-box-shadow: 2px 10px 25px -9px rgba(183,85,3,0.5); -moz-box-shadow: 2px 10px 25px -9px rgba(183,85,3,0.5); box-shadow: 2px 10px 25px -9px rgba(183,85,3,0.5);"/>
            </picture>
          </a>
          <a href="/docs/groups-stock-exchange.pdf?v=2025-01-08-v2" target="_blank">
            <button class="btn btn-orange mb-2 mt-3 px-0" style="min-width: 250px;" type="button">Download Brochure</button>
            </a>
          </div> 
        </div><!-- end col -->

        <div class="col-12 text-center text-lg-left mob-px-4 mob-mb-3 pt-5">
          <!-- <div class="pre-title-lines mob-mx-auto mb-4 mob-my-45 mt-5"></div>
          <h1 class="mb-4 mob-mb-0">Parties</h1> -->
        </div><!-- end col -->

        <div class="col-lg-6 text-center text-lg-left mob-px-4 mob-mb-3">
          <p class="text-large mb-3">Got a party to organise? Then we've got you! Whether it's an intimate birthday celebration for you and your friends, a team building night out or celebration of any kind we're here to make it memorable for you!</p>
          <picture>
            <source srcset="/img/parties/party.webp" type="image/webp"/> 
            <source srcset="/img/parties/party.jpg" type="image/jpeg"/> 
            <img src="/img/parties/party.jpg" type="image/jpeg" alt="Party at Haymarket Belfast" class="w-100 h-auto my-3"/>
          </picture>
        </div><!-- end col -->
        <div class="col-lg-6 pl-5 mob-px-4 text-center text-lg-left">
          <p class="text-large mb-4">With a range of private and semi-private spaces available for hire from groups of 20 and the perfect packages to go along with it. So, you have nothing to worry about, we'll do all the hard work for you.</p>
          <p class="text-large mb-2"><b>"What does my room hire include?"</b></p>
          <ul class="text-left mob-pl-5">
            <li class="mb-0">Complete private hire of room. </li>
            <li class="mb-0">Drinks & Food packages available to add.</li>
            <li class="mb-0"> Option to decorate space before arrival.<br><span style="font-size:14px;">(subject to availability)</span></li>
          </ul>
          {{-- <p class="text-primary"><b>Please note November & December room hire pricing comes under Christmas Packages.</b></p> --}}
          <button type="button" class="btn btn-primary booknowbtn">Enquire Now </button>
        </div><!-- end col -->
      </div><!-- end row -->
    </div><!-- end container -->
    
    <div class="full-bg overlay w-100 pb-3 mb-5 py-sm-5 my-sm-5" style="background-image: url('/img/parties/room2.webp');">
      <div class="container">
      <div class="pre-title-lines mb-4 mt-5"></div>
      <h2>Looking for a hotel?</h2>
        <div class="row g-5">
          <div class="col-lg-5 pb-5 ipadp-pb-0 mob-pb-0">
            <div class="py-5">
              <p>Choose room2! Whether you're seeking a room for a night or a long-term stay away from home, there's a room perfectly suited for everyone. We love room2 because they are fully committed to creating environments that have a positive impact on the health and wellbeing of all our visitors, employees and our planet, not to mention the interior design! Celebrating a birthday, stag, hen or milestone head over to room2 and get your boujee room booked now!</p>
              <p><strong>USE CODE 'HAYMARKET' AT CHECK OUT FOR 10% OFF YOUR STAY</strong></p>
              <a class="d-inline-block mt-5" href="https://room2.com/belfast/" target="_blank">
                <button type="button" class="btn btn-primary">Find out more</button>
              </a>
            </div>
          </div><!-- end col -->

          <div class="col-lg-7">
            <image-slider class="pt-0 pb-5 py-lg-5"
              :images="[
                '/img/parties/room2-1-2',
                '/img/parties/room2-2-2',
                '/img/parties/room2-3-2',
              ]"
              ></image-slider>
          </div><!-- end col -->
        </div><!-- end row -->
      </div><!-- end container -->
    </div><!-- end full-bg -->
  </div><!-- end header -->
</header>
@endsection
@section('content')
<div class="container container-wide pb-5 mb-5 mob-mb-0">
  <div class="row justify-content-center bar-videos">
    <div class="col-lg-3 col-6 mb-4">
      <div class="position-relative">
        <picture class="video-thumbnail w-100" style="cursor: pointer; z-index: 1; display: block;" onclick="let container=document.querySelector('#heel-video-embed'); let iframe=document.querySelector('#heel-video'); container.style.display='block'; iframe.style.display='block'; iframe.style.zIndex='2'; iframe.src=iframe.dataset.src;">
          <source srcset="/img/parties/shorts/heel.webp" type="image/webp"/> 
          <source srcset="/img/parties/shorts/heel.jpg" type="image/jpeg"/> 
          <img src="/img/parties/shorts/heel.jpg" type="image/jpeg" alt="Heel Bar at Haymarket Belfast" class="w-100"/>
        </picture>
        <div id="heel-video-embed" class="embed-responsive embed-responsive-16by9" style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
          <iframe 
            id="heel-video"
            width="423" 
            height="752" 
            class="embed-responsive-item" 
            data-src="https://www.youtube.com/embed/A4GQ_mopiq0?autoplay=1"
            style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
            allowfullscreen>
          </iframe>
        </div>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">HEEL BAR</p>
    </div>
    <div class="col-lg-3 col-6 mb-4">
      <div class="position-relative">
        <picture class="video-thumbnail w-100" style="cursor: pointer; z-index: 1; display: block;" onclick="let container=document.querySelector('#gin-video-embed'); let iframe=document.querySelector('#gin-video'); container.style.display='block'; iframe.style.display='block'; iframe.style.zIndex='2'; iframe.src=iframe.dataset.src;">
          <source srcset="/img/parties/shorts/gin.webp" type="image/webp"/> 
          <source srcset="/img/parties/shorts/gin.jpg" type="image/jpeg"/> 
          <img src="/img/parties/shorts/gin.jpg" type="image/jpeg" alt="Gin Bar at Haymarket Belfast" class="w-100"/>
        </picture>
        <div id="gin-video-embed" class="embed-responsive embed-responsive-16by9" style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
          <iframe 
            id="gin-video"
            width="423" 
            height="752" 
            class="embed-responsive-item" 
            data-src="https://www.youtube.com/embed/qsgF7dAd3w4?autoplay=1"
            style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
            allowfullscreen>
          </iframe>
        </div>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">THE GIN BAR</p>
    </div>
    <div class="col-lg-3 col-6 mb-4">
      <div class="position-relative">
        <picture class="video-thumbnail w-100" style="cursor: pointer; z-index: 1; display: block;" onclick="let container=document.querySelector('#cocktail-video-embed'); let iframe=document.querySelector('#cocktail-video'); container.style.display='block'; iframe.style.display='block'; iframe.style.zIndex='2'; iframe.src=iframe.dataset.src;">
          <source srcset="/img/parties/shorts/cocktail.webp" type="image/webp"/> 
          <source srcset="/img/parties/shorts/cocktail.jpg" type="image/jpeg"/> 
          <img src="/img/parties/shorts/cocktail.jpg" type="image/jpeg" alt="Cocktail Bar at Haymarket Belfast" class="w-100"/>
        </picture>
        <div id="cocktail-video-embed" class="embed-responsive embed-responsive-16by9" style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
          <iframe 
            id="cocktail-video"
            width="423" 
            height="752" 
            class="embed-responsive-item" 
            data-src="https://www.youtube.com/embed/hvZEkpro5s4?autoplay=1"
            style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
            allowfullscreen>
          </iframe>
        </div>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">THE COCKTAIL BAR</p>
    </div>
    <div class="col-lg-3 col-6 d-lg-none">
      <div class="position-relative">
        <picture class="video-thumbnail w-100" style="cursor: pointer; z-index: 1; display: block;" onclick="let container=document.querySelector('#gresham-video-embed'); let iframe=document.querySelector('#gresham-video'); container.style.display='block'; iframe.style.display='block'; iframe.style.zIndex='2'; iframe.src=iframe.dataset.src;">
          <source srcset="/img/parties/shorts/gresham.webp" type="image/webp"/> 
          <source srcset="/img/parties/shorts/gresham.jpg" type="image/jpeg"/> 
          <img src="/img/parties/shorts/gresham.jpg" type="image/jpeg" alt="Gresham Bar at Haymarket Belfast" class="w-100"/>
        </picture>
        <div id="gresham-video-embed" class="embed-responsive embed-responsive-16by9" style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
          <iframe 
            id="gresham-video"
            width="423" 
            height="752" 
            class="embed-responsive-item" 
            data-src="https://www.youtube.com/embed/YizThumeC44?autoplay=1"
            style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
            allowfullscreen>
          </iframe>
        </div>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">GRESHAM BAR</p>
    </div>
  </div>
  <div class="row justify-content-center bar-videos">
    <div class="col-lg-3 col-6 d-none d-lg-block">
      <div class="position-relative">
        <picture class="video-thumbnail w-100" style="cursor: pointer; z-index: 1; display: block;" onclick="let container=document.querySelector('#gresham-video-embed-2'); let iframe=document.querySelector('#gresham-video-2'); container.style.display='block'; iframe.style.display='block'; iframe.style.zIndex='2'; iframe.src=iframe.dataset.src;">
          <source srcset="/img/parties/shorts/gresham.webp" type="image/webp"/> 
          <source srcset="/img/parties/shorts/gresham.jpg" type="image/jpeg"/> 
          <img src="/img/parties/shorts/gresham.jpg" type="image/jpeg" alt="Gresham Bar at Haymarket Belfast" class="w-100"/>
        </picture>
        <div id="gresham-video-embed-2" class="embed-responsive embed-responsive-16by9" style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
          <iframe 
            id="gresham-video-2"
            width="423" 
            height="752" 
            class="embed-responsive-item" 
            data-src="https://www.youtube.com/embed/YizThumeC44?autoplay=1"
            style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
            allowfullscreen>
          </iframe>
        </div>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">GRESHAM BAR</p>
    </div>
    <div class="col-lg-3 col-6">
      <div class="position-relative">
        <picture class="video-thumbnail w-100" style="cursor: pointer; z-index: 1; display: block;" onclick="let container=document.querySelector('#armoury-video-embed'); let iframe=document.querySelector('#armoury-video'); container.style.display='block'; iframe.style.display='block'; iframe.style.zIndex='2'; iframe.src=iframe.dataset.src;">
          <source srcset="/img/parties/shorts/armoury.webp" type="image/webp"/> 
          <source srcset="/img/parties/shorts/armoury.jpg" type="image/jpeg"/> 
          <img src="/img/parties/shorts/armoury.jpg" type="image/jpeg" alt="Armoury Bar at Haymarket Belfast" class="w-100"/>
        </picture>
        <div id="armoury-video-embed" class="embed-responsive embed-responsive-16by9" style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
          <iframe 
            id="armoury-video"
            width="423" 
            height="752" 
            class="embed-responsive-item" 
            data-src="https://www.youtube.com/embed/VkX7oVaKcZU?autoplay=1"
            style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
            allowfullscreen>
          </iframe>
        </div>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">THE ARMOURY</p>
    </div>
    <div class="col-lg-3 col-6">
      <div class="position-relative">
        <picture class="video-thumbnail w-100" style="cursor: pointer; z-index: 1; display: block;" onclick="let container=document.querySelector('#boardroom-video-embed'); let iframe=document.querySelector('#boardroom-video'); container.style.display='block'; iframe.style.display='block'; iframe.style.zIndex='2'; iframe.src=iframe.dataset.src;">
          <source srcset="/img/parties/shorts/boardroom.webp" type="image/webp"/> 
          <source srcset="/img/parties/shorts/boardroom.jpg" type="image/jpeg"/> 
          <img src="/img/parties/shorts/boardroom.jpg" type="image/jpeg" alt="Board Room at Haymarket Belfast" class="w-100"/>
        </picture>
        <div id="boardroom-video-embed" class="embed-responsive embed-responsive-16by9" style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;">
          <iframe 
            id="boardroom-video"
            width="423" 
            height="752" 
            class="embed-responsive-item" 
            data-src="https://www.youtube.com/embed/bl82jxnH4-0?autoplay=1"
            style="display: none; position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" 
            allowfullscreen> 
          </iframe>
        </div>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">THE BOARD ROOM</p>   
    </div>
  </div>
</div>
<div class="container pb-5 position-relative z-2 mob-pb-0">
  <div class="row text-center text-lg-left">
    <div class="col-lg-5 pb-5 ipadp-pb-0 mob-pb-0">
      <div class="haybale-bg py-5 ">
        <div class="pre-title-lines mb-4 mob-mx-auto"></div>
        <h2>Great for all parties & Corporate events</h2>
      </div>
    </div>
    <div class="col-lg-7 py-5 mob-py-0 mob-px-4  ipadp-pt-0">
      <p class="text-larger mb-5">Our indoor bars are spread over three floors, offering the perfect spot for a cosy date night or spontaneous cocktail or two! The outdoor area is the go to location in the City Centre for an alfresco drink. Banging food, banging drinks & a banging atmosphere. There's nothing more you need. </p>
      <button type="button" class="btn btn-primary booknowbtn">Enquire Now </button>
    </div>
    <div class="col-12">
      <picture>
        <source srcset="/img/home/monday-photo.webp" type="image/webp"/> 
        <source srcset="/img/home/monday-photo.jpg" type="image/jpeg"/> 
        <img src="/img/home/monday-photo.jpg" width="507" height="507" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy w-100 h-auto mt-5"/>
      </picture>
    </div>
  </div>
</div>
<div id="bookonline" class="container-fluid position-relative py-5 z-1 mob-pt-4 mob-pb-0 ipadp-py-0">
  <picture>
    <source srcset="/img/graphics/chips-burger.webp" type="image/webp"/> 
    <source srcset="/img/graphics/chips-burger.png" type="image/jpeg"/> 
    <img src="/img/graphics/chips-burger.png" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy chips-burger-top-right d-none d-lg-block"/>
  </picture>
  <div class="row pb-5 mob-pt-0">
    <div class="container py-5 position-relative z-2 mob-pb-0">
      <div class="row">
        <div class="col-12 pb-4 mob-px-4">
          <div class="pre-title-lines mob-mx-auto mb-4"></div>
          <p class="mimic-h3 text-center text-lg-left mb-3">Enquire now</p>
          <p class="mb-4 text-center text-lg-left">We have a number of packages available or can create one just for your event. Enquire below today and a member of our events team will be in touch to make sure your party won't be a night to forget!</p>
          <parties-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></parties-form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/CARGOByVertigo/22894?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
  document.addEventListener('DOMContentLoaded', function() {
    document.querySelector('.booknowbtn').addEventListener('click', function() {
      const element = document.getElementById('bookonline');
      window.scrollTo({
        top: element.offsetTop,
        behavior: 'smooth'
      });
    });
  });
</script>
<style>
  .bar-videos .embed-responsive-16by9::before{
    padding-top: 178.25%;
  }
</style>

@endsection