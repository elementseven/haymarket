@php
$page = 'FAQs';
$pagetitle = 'Frequently asked questions | Haymarket Belfast';
$metadescription = "Take a look at some of our most Frequently Asked Questions, if there's something we haven't answered be sure to get in touch.";
$pagetype = 'white';
$pagename = 'faqs';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('content')
<picture>
  <source srcset="/img/graphics/burger-beer.webp" type="image/webp"/> 
  <source srcset="/img/graphics/burger-beer.png" type="image/jpeg"/> 
  <img src="/img/graphics/burger-beer.png" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy burger-beer-top-left"/>
</picture>
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<faqs :categories="{{$categories}}"></faqs>
<div class="container py-5 mob-px-4 mob-py-0">
  <div class="row my-5">
    <div class="col-lg-6 py-5 mob-py-0 text-center text-lg-left">
      <div class="pre-title-lines mob-mx-auto mb-4"></div>
      <p class="mimic-h2 mob-smaller"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline ">Have a question for us?</span></p>
      <a href="{{route('contact')}}"><button type="button" class="btn btn-primary">Get in touch</button></a>
    </div>
  </div>
</div>
@endsection