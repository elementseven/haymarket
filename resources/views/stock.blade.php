@php
$page = 'The Stock Exchange';
$pagetitle = "The Stock Exchange @ Haymarket - Belfast's vibrant indoor/outdoor bar & street food hangout";
$metadescription = "Experience the thrill of The Stock Exchange in Belfast with our dynamic stock market bar, Interactive darts, and electric shuffleboards. Enjoy crafted cocktails and shareable bites in a lively atmosphere.";
$pagetype = 'stock';
$pagename = 'stock';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preload" as="image" href="/img/venues/stock/top.webp">
<style>
  .stock-option .btn{
    max-width: 42%;
    min-width: 0;
    margin-top: -25px;
  }
</style>
@endsection
@section('header')
<header id="stock-header" class="container-fluid position-relative bg bg-down-up z-1 px-0">
  <div class="d-flex overflow-hidden bg stock-top stock-vid" style="min-height: 545px">
    <div class="container-fluid px-0 h-100" style="top: 0;left: 0;min-width: 970px;">
      <div id="videoContainer" class="embed-responsive embed-responsive-16by9">
        <iframe id="deferredIframe" width="640" height="361" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen loading="lazy" src="https://player.vimeo.com/video/1036098873?h=5433e291e4&autoplay=1&loop=1&autopause=0&background=1"></iframe>
        <div class="stock-top-bg"></div>
      </div>
    </div>
    <div class="container new-home-top text-center pt-5">
      <img src="/img/logos/the-stock-exchange.svg" class="mb-4 h-auto" alt="The Armoury - Haymarket Belfast Logo" width="500" height="114"/>
      <p class="text-one mb-4">Welcome to the Stock Exchange. From Interactive Darts, and Shuffle Boards, to our Stock Market Bar where drinks prices crash with the market! Elevate your night out with some competitive fun.</p>
      <a href="https://ecom.roller.app/haymarketbelfast/stockexchange/en/products" target="_blank" class="d-inline-block">
        <button type="button" class="btn btn-primary position-relative mb-2 z-2">BOOK SHUFFLE/DARTS</button>
      </a>
    </div>
  </div>
  <div class="video-grad"></div>
  <div class="video-gradient"></div>
</header>
@endsection
@section('content')
<div class="graphics-holder">
  <picture>
    <source srcset="/img/venues/stock/cocktail.webp" type="image/webp"/> 
    <source srcset="/img/venues/stock/cocktail.png" type="image/png"/> 
    <img src="/img/venues/stock/cocktail.png" type="image/png" width="550" height="1156" alt="The Stock Exchange - Interactive Darts at - Haymarket Belfast" class="cocktail-top-right h-auto"/>
  </picture>
  <picture>
    <source srcset="/img/venues/stock/dartboard.webp" type="image/webp"/> 
    <source srcset="/img/venues/stock/dartboard.png" type="image/png"/> 
    <img src="/img/venues/stock/dartboard.png" type="image/png" width="550" height="1156" alt="The Stock Exchange - Interactive Darts at - Haymarket Belfast" class="dartboard-bottom-left h-auto"/>
  </picture>
</div>
<div class="contianer-fluid py-lg-5 pb-5 overflow-hidden text-center position-relative z-2 venue-offers">
  <div class="container container-wide">
    <div class="row">
      <div class="col-12 mb-4">
        <div class="pre-title-lines mt-2 mb-4 mx-auto d-inline-block"></div>
        <h2>Activities</h2>
      </div>
      <div class="col-lg-4 mob-mb-5">
        <div class="stock-option text-center" data-aos="fade-up">
          <picture>
            <source srcset="/img/venues/stock/stock-market-bar.webp?v=2024-12-02" type="image/webp"/> 
            <source srcset="/img/venues/stock/stock-market-bar.jpg?v=2024-12-02" type="image/jpeg"/> 
            <img src="/img/venues/stock/stock-market-bar.jpg?v=2024-12-02" type="image/jpeg" width="350" height="350" alt="Stockmarket Bar at Haymarket Belfast" class="lazy w-100 h-auto op-45 shadow"/>
          </picture>
          <div class="card w-100 px-4 py-5 mx-auto position-relative z-1">
            <p class="mimic-h3 mb-2">STOCKMARKET <br/>BAR</p>
            <div class="pre-title-lines mt-2 mb-4 mx-auto d-inline-block"></div>
            <p class="text-small">We've quite literally brought the stock market to Royal Avenue. Watch as drink prices drop, just like stocks. Keep an eye on the board as you don't know when the market will crash! Civil unrest in Mexico when tequila prices plumet? Yes PLEASE</p>
          </div>
          <a href="/the-stock-exchange/stock-market-bar">
            <button type="button" class="btn btn-primary mr-2 z-2">More Info</button>
          </a>
{{--           <a href="https://ecom.roller.app/haymarketbelfast/testarmoury/en/products" target="_blank">
            <button type="button" class="btn btn-primary mx-auto z-2">Book Now</button>
          </a> --}}
        </div>
      </div>
      <div class="col-lg-4 mob-mb-5">
        <div class="stock-option text-center" data-aos="fade-up">
          <picture>
            <source srcset="/img/venues/stock/virtual-darts.webp?v=2024-12-02" type="image/webp"/> 
            <source srcset="/img/venues/stock/virtual-darts.jpg?v=2024-12-02" type="image/jpeg"/> 
            <img src="/img/venues/stock/virtual-darts.jpg?v=2024-12-02" type="image/jpeg" width="350" height="350" alt="Interactive Darts at Haymarket Belfast" class="lazy w-100 h-auto op-45 shadow"/>
          </picture>
          <div class="card w-100 px-4 py-5 mx-auto position-relative z-1">
            <p class="mimic-h3 mb-2">INTERACTIVE <br/>DARTS</p>
            <div class="pre-title-lines mt-2 mb-4 mx-auto d-inline-block"></div>
            <p class="text-small">Darts, reinvented. Enjoy fast paced games with the latest tracking tech to give unrivalled fun for all abilities! Try your hand, ideal for date night or something a bit different with friends! Plus hand crafted cocktails & tasty sharing plates!</p>
          </div>
          <a href="/the-stock-exchange/virtual-darts">
            <button type="button" class="btn btn-primary mr-2 z-2">More Info</button>
          </a>
          <a href="https://ecom.roller.app/haymarketbelfast/stockexchange/en/products" target="_blank">
            <button type="button" class="btn btn-primary mx-auto z-2">Book Now</button>
          </a>
        </div>
      </div>
      <div class="col-lg-4 mob-my-5">
        <div class="stock-option text-center">
          <picture>
            <source srcset="/img/venues/stock/shuffle-board.webp?v=2024-12-02" type="image/webp"/> 
            <source srcset="/img/venues/stock/shuffle-board.jpg?v=2024-12-02" type="image/jpeg"/> 
            <img src="/img/venues/stock/shuffle-board.jpg?v=2024-12-02" type="image/jpeg" width="350" height="350" alt="Shuffle Board at Haymarket Belfast" class="lazy w-100 h-auto op-45 shadow"/>
          </picture>
          <div class="card w-100 px-4 py-5 mx-auto position-relative z-1">
            <p class="mimic-h3 mb-2">SHUFFLE <br/>BOARDS</p>
            <div class="pre-title-lines mt-2 mb-4 mx-auto d-inline-block"></div>
            <p class="text-small">Gather your friends, challenge your enemies, and get ready for a pucking good time with these state of the art boards! Rival friends and colleagues and enjoy a few beers while you play!</p>
          </div>
            <a href="/the-stock-exchange/shuffle-board">
              <button type="button" class="btn btn-primary mr-2 z-2">More Info</button>
            </a>
            <a href="https://ecom.roller.app/haymarketbelfast/stockexchange/en/products" target="_blank">
              <button type="button" class="btn btn-primary mx-auto z-2">Book Now</button>
            </a>
        </div>
      </div> 
    </div>
  </div>
</div>
<div class="container container ipadp-pt-0 position-relative z-2">
  <div class="row text-left pt-5">
    <div class="col-lg-6 position-relative z-2">
      <stock-slider></stock-slider>
    </div>
    <div class="col-lg-5 mob-pt-0 pl-4 mob-px-3 text-lg-left text-center mob-pt-5">
      <div class=" mob-pt-5"></div>
      <div class="pre-title-lines mb-4 mob-mx-auto"></div>
      <h3 class="">PLANNING A PARTY?</h3>
      <p class="mb-3">Got a party to organise? Whether it’s a birthday celebration or a team building night out we’re here to make it memorable for you! Check out our packages below, and enquire now with our dedicated group bookings team.</p>
      <button type="button" class="btn btn-primary mx-auto z-2 booknowbtn mb-2">Enquire Now</button>
      <p class=""><a href="/docs/groups-stock-exchange.pdf?v=2025-01-08-v2" target="_blank"><i class="fa fa-download mr-2"></i><b><u>View Packages</u></b></a></p>
    </div>
  </div>
</div>
{{-- <div class="contianer-fluid py-5 mt-5 overflow-hidden text-center position-relative z-2 venue-offers">
  <div class="pre-title-lines mb-4 mx-auto"></div>
  <h2 class="mb-4">Offers & events</h2>
  <offers-slider :venue="2"></offers-slider>
</div> --}}
<div id="bookonline" class="container-fluid position-relative z-1 mob-pb-0 ipadp-py-0">
  <div class="row pt-5 mob-pt-0">
    <div class="container py-5 position-relative z-2 mob-pb-0">
      <div class="row">
        <div class="col-12 pb-4 mob-px-4">
          <div class="pre-title-lines mob-mx-auto mb-4"></div>
          <p class="mimic-h3 text-center text-lg-left mb-3">Enquire now</p>
          <armoury-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></armoury-form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="py-5"></div>
<seating-options-stock></seating-options-stock>
<div class="container position-relative z-2 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 py-5">
      <mailing-list :id="'ml-1-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
{{-- <offer-modal></offer-modal> --}}
@endsection
@section('scripts')
 <script>
  window.addEventListener('load', function() {
    document.querySelectorAll('.booknowbtn').forEach(function(button) {
      button.addEventListener('click', function() {
        var targetElement = document.getElementById("bookonline");
        var offset = 100;
        var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
        window.scrollTo({
          top: targetPosition - offset,
          behavior: 'smooth'
        });
      });
    });
  });
</script>
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/haymarketbelfast/testarmoury/en/products"></script>
@endsection