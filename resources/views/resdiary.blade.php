@php
$page = 'Homepage';
$pagetitle = "Haymarket - Belfast's newest indoor/outdoor bar & street food hangout";
$metadescription = "Haymarket Belfast is Belfast's newest indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.";
$pagetype = 'light';
$pagename = 'resdiary';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<homepage-header-resdiary></homepage-header-resdiary>
@endsection
@section('content')
<seating-options></seating-options>
<div class="container py-5 ipadp-pt-0 position-relative z-2 mob-pb-0">
  <div class="row px-0 d-lg-none mt-4">
    <div class="col-12 px-0">
      <picture>
        <source srcset="/img/home/walk-in.webp" type="image/webp"/> 
        <source srcset="/img/home/walk-in.jpg" type="image/jpeg"/> 
        <img src="/img/home/walk-in.jpg" width="507" height="507" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy w-100 h-auto mt-5"/>
      </picture>
    </div>
  </div>
  <div class="row text-center text-lg-left">
    <div class="col-lg-5 py-5 mob-pb-0 ipadp-pb-0">
      <div class="haybale-bg py-5">
        <div class="pre-title-lines mb-4 mob-mx-auto"></div>
        <h2>Craft,<br/>Cocktails & Much More</h2>
      </div>
    </div>
    <div class="col-lg-7 py-5 mob-py-0 mob-px-4 ipadp-pt-0">
      <p class="text-larger mt-4 mob-mt-0">Haymarket Belfast is Belfast's newest indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.</p>
      <p class="text-larger mb-0">The indoor bar is spread over three floors, offering the perfect spot for a cosy date night or spontaneous cocktail or two! The outdoor area is the go to location in the City Centre for an alfresco drink. Banging food, banging drinks & a banging atmosphere. There's nothing more you need. </p>
      <button type="button" class="btn btn-primary booknowbtn mt-4">Book now</button>
    </div>
  </div>
</div>
<div class="container position-relative z-2 d-none d-lg-block">
  <div class="row">
    <div class="col-12 py-5">
      <mailing-list :id="'ml-1-'"></mailing-list>
    </div>
  </div>
</div>
<div class="container-fluid position-relative py-5 mob-pt-4 z-1">
  <picture>
    <source data-srcset="/img/graphics/chips-burger.webp" type="image/webp"/> 
    <source data-srcset="/img/graphics/chips-burger.png" type="image/png"/> 
    <img data-src="/img/graphics/chips-burger.png" type="image/png" width="550" height="1156" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy chips-burger-top-right h-auto"/>
  </picture>
  <div class="row py-5 ipadp-pt-0">
    <news-inline></news-inline>
  </div>
</div>
<div class="container position-relative z-2 d-lg-none mb-5">
  <div class="row px-0">
    <div class="col-12 px-0">
      <mailing-list :id="'ml-2-'"></mailing-list>
    </div>
  </div>
</div>
<div class="container py-5 position-relative z-2 mob-pb-0">
  <div class="row text-center text-lg-left">
    <div class="col-12 mb-5 3pb-4">
      <div class="pre-title-lines mb-4 mob-mx-auto mob-mb-3"><span class="d-none d-lg-inline"><a href="https://instagram.com/haymarketbelfast" class="text-lowercase" target="_blank" rel="noreferrer" aria-label="Instagram">@haymarketbelfast <i class="fa fa-instagram ml-2"></i></a></span></div>
      <p class="d-lg-none text-large"><b><a href="https://instagram.com/haymarketbelfast" target="_blank" rel="noreferrer" aria-label="Instagram">@haymarketbelfast <i class="fa fa-instagram ml-2"></i></a></b></p>
      <p class="mimic-h3 mb-4">FOLLOW US ON INSTAGRAM</p>
      <instagram-feed></instagram-feed>
    </div>
  </div>
</div>
@endsection
@section('modals')
<div class="modal fade mx-auto" id="midweekModal" tabindex="-1" role="dialog" aria-labelledby="midweekModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <p class="mimic-h2">Mid-week at<br/>Haymarket</p>
      <div id="midweekCarouselIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#midweekCarouselIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#midweekCarouselIndicators" data-slide-to="1"></li>
{{--           <li data-target="#midweekCarouselIndicators" data-slide-to="2"></li>
 --}}        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active text-center">
            <picture>
              <source srcset="/img/midweek/student-tuesdays.webp" type="image/webp"/> 
              <source srcset="/img/midweek/student-tuesdays.jpg" type="image/jpeg"/> 
              <img src="/img/midweek/student-tuesdays.jpg" type="image/jpeg" width="500" height="500" alt="Haymarket Taco-Tuesdays" class="h-auto d-none d-lg-inline-block"/>
            </picture>
            <picture>
              <source srcset="/img/midweek/student-tuesdays-mob.webp" type="image/webp"/> 
              <source srcset="/img/midweek/student-tuesdays-mob.jpg" type="image/jpeg"/> 
              <img src="/img/midweek/student-tuesdays-mob.jpg" type="image/jpeg" width="400" height="400" alt="Haymarket Taco-Tuesdays" class="w-100 h-auto d-lg-none"/>
            </picture>
          </div>
          <div class="carousel-item text-center">
            <picture>
              <source srcset="/img/midweek/student-thursdays.webp" type="image/webp"/> 
              <source srcset="/img/midweek/student-thursdays.jpg" type="image/jpeg"/> 
              <img src="/img/midweek/student-thursdays.jpg" type="image/jpeg" width="500" height="500" alt="Haymarket Throwback-Thursdays" class="h-auto d-none d-lg-inline-block"/>
            </picture>
            <picture>
              <source srcset="/img/midweek/student-thursdays-mob.webp" type="image/webp"/> 
              <source srcset="/img/midweek/student-thursdays-mob.jpg" type="image/jpeg"/> 
              <img src="/img/midweek/student-thursdays-mob.jpg" type="image/jpeg" width="400" height="400" alt="Haymarket Throwback-Thursdays" class="w-100 h-auto d-lg-none"/>
            </picture>
          </div>
        </div>
        <a class="carousel-control-prev d-none d-lg-flex" href="#midweekCarouselIndicators" role="button" data-slide="prev" aria-label="Previous Slide">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next d-none d-lg-flex" href="#midweekCarouselIndicators" role="button" data-slide="next" aria-label="Next Slide">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <div class="btn-text cursor-pointer text-center d-block w-100 mt-5 position-relative z-200" data-dismiss="modal" aria-label="Close Modal">[X] Close</div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
 {{-- <script>
    $('#midweekModal').modal('show');
  </script> --}}
  <input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/26681?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
</script>
@endsection