@php
$page = 'The Armoury';
$pagetitle = "The Armoury @ Haymarket - Belfast's vibrant indoor/outdoor bar & street food hangout";
$metadescription = "Interactive shooting gallery using state-of-the-art technology. With loads of different game modes, we’ll keep you and your squad entertained all night long with refreshments from our bar & kitchen.";
$pagetype = 'armoury';
$pagename = 'armoury';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preload" as="image" href="https://haymarketbelfast.com/build/assets/armoury-top-bg-Buez5CKC.webp">
<style>
  .armoury-option .btn{
    max-width: 42%;
    min-width: 0;
  }
</style>
@endsection
@section('header')

<header class="container-fluid position-relative bg bg-down-up z-1 px-0">
  <div class="d-flex overflow-hidden bg armoury-top armoury-vid" style="min-height: 545px">
    <div class="container-fluid px-0 h-100" style="top: 0;left: 0;min-width: 970px;">
      <div id="videoContainer" class="embed-responsive embed-responsive-16by9">
        <iframe id="deferredIframe" width="640" height="361" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen loading="lazy" src="https://player.vimeo.com/video/942007451?h=5433e291e4&autoplay=1&loop=1&autopause=0&background=1"></iframe>
        <div class="armoury-top-bg"></div>
      </div>
    </div>
    <div class="new-home-top text-center pt-5">
      <img src="/img/logos/the-armoury.svg" class="mb-4 h-auto" alt="The Armoury - Haymarket Belfast Logo" width="500" height="114"/>
        <p class="text-one mb-4">Time to get competitive in our simulated shooting range! With replica firearms and state of the art technology, we'll keep you and your 'squad' entertained all night long! Enjoy this experience in your own private booth, with delicious sharing dishes, cocktails & draught beer brought straight to your table.</p>
        <a href="https://ecom.roller.app/haymarketbelfast/testarmoury/en/products" target="_blank">
          <button type="button" class="btn btn-gold mx-auto position-relative z-2">Book Now</button>
        </a>
    </div>
  </div>
  <div class="video-grad"></div>
  <div class="video-gradient"></div>
</header>
{{-- <header class="container position-relative z-1 pt-5 text-center logo-lines mob-px-4 mob-mb-3">
  <div class="row half_row justify-content-center pt-5 pb-3 mob-px-3">
    <div class="col-lg-8 half_col pt-5 mob-pt-0">
      <div class="venue-top-no-vid">
        <img src="/img/logos/the-armoury.svg" class="mb-4 h-auto" alt="The Armoury - Haymarket Belfast Logo" width="500" height="114"/>
        <p class="text-one mb-4">Time to get competitive in our simulated shooting range! With replica firearms and state of the art technology, we'll keep you and your 'squad' entertained all night long! Enjoy this experience in your own private booth, with delicious sharing dishes, cocktails & draught beer brought straight to your table.</p>
        <a href="https://ecom.roller.app/haymarketbelfast/testarmoury/en/products" target="_blank">
          <button type="button" class="btn btn-gold mx-auto position-relative z-2">Book Now</button>
        </a>
      </div>
    </div>
  </div>
   <picture>
    <source srcset="/img/graphics/guns.webp" type="image/webp"/> 
    <source srcset="/img/graphics/guns.png" type="image/png"/> 
    <img src="/img/graphics/guns.png" type="image/png" width="550" height="1156" alt="The Armoury - Haymarket Belfast" class="guns-top-right h-auto"/>
  </picture>
</header> --}}
@endsection
@section('content')
<div class="contianer-fluid py-5 overflow-hidden text-center position-relative z-2 venue-offers">
{{--   <div class="pre-title-lines mb-4 mx-auto"></div>
  <h2 class="mb-4">Offers & events</h2> --}}
  <div class="container container-wide">
    <div class="row">
      <div class="col-lg-4 mob-mb-5">
        <div class="armoury-option text-center" data-aos="fade-up">
          <picture>
            <source srcset="/img/venues/large-groups.webp" type="image/webp"/> 
            <source srcset="/img/venues/large-groups.jpg" type="image/jpeg"/> 
            <img src="/img/venues/large-groups.jpg" type="image/jpeg" width="350" height="350" alt="Large Groups Private Venue Hire  & Interactive Shooting at The Armoury - Haymarket Belfast" class="lazy w-100 h-auto op-45 shadow"/>
          </picture>
          <div class="card w-100 px-4 pb-5 pt-4 mx-auto position-relative z-1">
            <p class="text-primary text-small text-uppercase letter-spacing mb-1">2 - 24 guests</p>
            <p class="mimic-h3 mb-2">Private Booth Hire</p>
            <div class="pre-title-lines mt-2 mb-4 mx-auto d-inline-block"></div>
            <p>Whether it's date night or you're wanting to switch it up The Armoury is the ideal spot for your night out! Go head to head with our interactive simulated range and put your skills to the test from the comfort of your own private booth!</p>
            <div class="collapse" id="readMore">
              <p>Shooting can be hard work, but don't worry! Our team will keep you topped up with tasty cocktails, draught beer and sharing plates!</p>
              <p>Ready, aim, book your table now!</p>
            </div>
            <p class="mb-0"><a class="text-primary" data-toggle="collapse" href="#readMore" role="button" aria-expanded="false" aria-controls="readMore">Read More<i class="fa fa-angle-down"></i></a></p>
          </div>
          <a href="https://ecom.roller.app/haymarketbelfast/testarmoury/en/products" target="_blank">
            <button type="button" class="btn btn-primary mx-auto z-2">Book Now</button>
          </a>
        </div>
      </div>
      <div class="col-lg-4 mob-mb-5">
        <div class="armoury-option text-center" data-aos="fade-up">
          <picture>
            <source srcset="/img/venues/small-groups.webp?v=2024-05-21" type="image/webp"/> 
            <source srcset="/img/venues/small-groups.jpg?v=2024-05-21" type="image/jpeg"/> 
            <img src="/img/venues/small-groups.jpg?v=2024-05-21" type="image/jpeg" width="350" height="350" alt="Small Groups Private Venue Hire  & Interactive Shooting at The Armoury - Haymarket Belfast" class="lazy w-100 h-auto op-45 shadow"/>
          </picture>
          <div class="card w-100 px-4 pb-5 pt-4 mx-auto position-relative z-1">
            <p class="text-primary text-small text-uppercase letter-spacing mb-1">GROUPS OF 6+</p>
            <p class="mimic-h3 mb-2">Celebrations</p>
            <div class="pre-title-lines mt-2 mb-4 mx-auto d-inline-block"></div>
            <p>Whether it is a stag, hen or birthday celebration The Armoury is the place to be! Check out our package offers to make your day extra special!</p>
            <p>With private booth hire, drinks on arrival and the option to pre order tasty sharing plates & drinks to your table! All you have to do is turn up looking 10/10!</p>
           {{--  <div class="collapse" id="readMore3">
              <p>Test you aim in our private booths with the option to grab some tasty food & drinks whilst you battle against our simulated shooting scenarios! The main question is who are you bringing along?</p>
              <p>For groups of less than 4 guests, just come on down and our team will let you know the next available slot. Booking of 4 or less are taken on a first come, first serve basis so make sure to get down early (and even enjoy a pre-shooting cocktail). </p>
            </div>
            <p class="mb-0"><a class="text-primary" data-toggle="collapse" href="#readMore3" role="button" aria-expanded="false" aria-controls="readMore3">Read More<i class="fa fa-angle-down"></i></a></p> --}}
          </div>
          <a href="/docs/armoury-packages.pdf?v=2024-06-20" target="_blank">
            <button type="button" class="btn btn-primary mr-2 z-2">Packages</button>
          </a>
          <a href="https://ecom.roller.app/haymarketbelfast/testarmoury/en/products" target="_blank">
            <button type="button" class="btn btn-primary mx-auto z-2">Book Now</button>
          </a>
        </div>
      </div>
      <div class="col-lg-4 mob-my-5">
        <div class="armoury-option text-center">
          <picture>
            <source srcset="/img/venues/private-venue-hire.webp" type="image/webp"/> 
            <source srcset="/img/venues/private-venue-hire.jpg" type="image/jpeg"/> 
            <img src="/img/venues/private-venue-hire.jpg" type="image/jpeg" width="350" height="350" alt="Private Venue Hire  & Interactive Shooting at The Armoury - Haymarket Belfast" class="lazy w-100 h-auto op-45 shadow"/>
          </picture>
          <div class="card w-100 px-4 py-5 mx-auto position-relative z-1">
            <p class="mimic-h3 mb-2">Private Venue Hire</p>
            <div class="pre-title-lines mt-2 mb-4 mx-auto d-inline-block"></div>
            <p>The new hot spot for team bonding has arrived! Enjoy full private access to The Armoury,  perfect for groups of 40+ guests!  There is nothing like a little ‘friendly fire’ and head to head competition to get to know your colleagues better! Semi private hire also available!</p>
            <div class="collapse" id="readMore2">
              <p>Get into teams, or challenge each other 1 v 1 and shoot your shot at the highest score! The perfect venue with private range hire, food and beverage packages and private bar.</p>
              <p>Your fully loaded night out starts here! Enquire now!</p>
            </div>
            <p class="mb-0"><a class="text-primary" data-toggle="collapse" href="#readMore2" role="button" aria-expanded="false" aria-controls="readMore2">Read More <i class="fa fa-angle-down"></i></a></p>
          </div>
            <a href="/docs/armoury-corp-packages.pdf?v=2024-09-02" target="_blank">
              <button type="button" class="btn btn-primary mr-2 z-2">Packages</button>
            </a>
            <button type="button" class="btn btn-primary mx-auto z-2 booknowbtn">Enquire Now</button>
        </div>
      </div> 
    </div>
  </div>
</div>
<div class="contianer-fluid py-5 mt-5 overflow-hidden text-center position-relative z-2 venue-offers">
  <div class="pre-title-lines mb-4 mx-auto"></div>
  <h2 class="mb-4">Offers & events</h2>
  <offers-slider :venue="2"></offers-slider>
</div>
<div class="container container ipadp-pt-0 position-relative z-2">
  <div class="row text-left pt-5">
    <div class="col-12 text-center mb-5 d-none d-lg-block">
      <div class="pre-title-lines mb-4 mt-5 mx-auto"></div>
      <h1 class="">How it works</h1>
      <p>From Booking to Sharp Shooting!</p>
    </div>
    <div class="col-lg-6">
        <picture>
          <source srcset="/img/venues/how-it-works.webp?v=2024-04-24" type="image/webp"/> 
          <source srcset="/img/venues/how-it-works.jpg?v=2024-04-24" type="image/jpeg"/> 
          <img src="/img/venues/how-it-works.jpg?v=2024-04-24" type="image/jpeg" width="350" height="350" alt="Interactive Shooting at The Armoury - Haymarket Belfast" class="lazy w-100 h-auto op-45 shadow"/>
        </picture>
    </div>
    <div class="col-12 text-center d-lg-none">
      <div class="pre-title-lines mb-4 mt-5 mx-auto"></div>
      <h1 class="">How it works</h1>
      <p>From Booking to Sharp Shooting!</p>
    </div>
    <div class="col-lg-6 pt-4 mob-pt-0 pl-4 mob-px-3 text-lg-left text-center">
      <p class="mb-3">Interested? Good! Any important information can be found here, please have a read through!</p>
      <p class="mb-3">For booking enquiries of 16+ and private hire please fill out an enquiry form and our team will get back to you to confirm availability. Catch you on the range!</p>
      <a href="/the-armoury/info">
        <button type="button" class="btn btn-primary mt-3">More Info</button>
      </a>
    </div>
  </div>
</div>
{{-- <div class="container py-5 ipadp-pt-0 position-relative z-2">
  <div class="row text-center text-lg-left pt-5 mob-pt-0">
    <div class="col-lg-7 pr-5 mob-px-3">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/3uj3sMRbFFY?si=WhOaAeSW5d23uvWr" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen class="embed-responsive-item"></iframe>
      </div>
    </div>
    <div class="col-lg-5 pt-2">
      <div class="pre-title-lines mb-4 mob-mx-auto"></div>
      <h3 class="">How does it work?</h3>
      <p class="text-larger mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
      <a href="/the-armoury/info">
        <button type="button" class="btn btn-primary">More Info</button>
      </a>
    </div>
  </div>
</div> --}}
{{-- <div class="container py-5 my-5">
  <armoury-slider></armoury-slider>
  <div class="pb-5"></div>
</div> --}}
<div class="py-5"></div>
<seating-options-armoury></seating-options-armoury>
<div id="bookonline" class="container-fluid position-relative z-1 mob-pb-0 ipadp-py-0">
  <div class="row pt-5 mob-pt-0">
    <div class="container py-5 position-relative z-2 mob-pb-0">
      <div class="row">
        <div class="col-12 pb-4 mob-px-4">
          <div class="pre-title-lines mob-mx-auto mb-4"></div>
          <p class="mimic-h3 text-center text-lg-left mb-3">Enquire now</p>
          <armoury-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></armoury-form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container position-relative z-2 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 py-5">
      <mailing-list :id="'ml-1-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
{{-- <offer-modal></offer-modal> --}}
{{-- <div class="modal fade mx-auto" id="midweekModal" tabindex="-1" role="dialog" aria-labelledby="midweekModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content text-center">
      <picture>
        <source srcset="/img/offers/halloween/normal-webp.webp" type="image/webp"/> 
        <source srcset="/img/offers/halloween/normal.jpg" type="image/jpg"/> 
        <img src="/img/offers/halloween/normal.jpg" width="507" height="507" type="image/jpg" alt="Halloween - Haymarket Belfast" class="mw-100 h-auto d-inline-block" />
      </picture>
      <a href="/offers/14/family-friendly-sessions">
        <button type="button" class="btn btn-primary mx-auto mt-3">Book Now</button>
      </a>
      <div class="btn-text cursor-pointer text-center d-block w-100 mt-2 position-relative z-200" data-dismiss="modal" aria-label="Close Modal">[X] Close</div>
    </div>
  </div>
</div> --}}
@endsection
@section('scripts')
 <script>

  window.addEventListener('load', function() {
    document.querySelectorAll('.booknowbtn').forEach(function(button) {
      button.addEventListener('click', function() {
        var targetElement = document.getElementById("bookonline");
        var offset = 100;
        var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
        window.scrollTo({
          top: targetPosition - offset,
          behavior: 'smooth'
        });
      });
    });
  });
</script>
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/haymarketbelfast/testarmoury/en/products"></script>
@endsection