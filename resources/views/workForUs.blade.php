@php
$page = 'Work For Us';
$pagetitle = 'Work For Us | Haymarket Belfast';
$metadescription = 'Work For Us at Haymarket Belfast';
$pagetype = 'white';
$pagename = 'contact';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 px-0">
  <div class="bg work-for-us-bg d-flex work-for-us-vid d-flex overflow-hidden" style=";max-height:640px;overflow: hidden;display: flex;align-items: center;justify-content: center;">
    <div class="container-fluid px-0 h-100" style="top: 0;left: 0;">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe src="https://player.vimeo.com/video/689769645?h=d6570b4562&autoplay=1&loop=1&autopause=0&background=1" width="640" height="361" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
      </div>
      
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container position-relative">
  <div class="row justify-content-center mt-5 py-5 mob-mt-0 mob-pt-0">
    <div class="col-lg-6 pr-5 mob-px-3 d-none d-lg-block">
      <picture>
        <source src="/img/work-for-us/img11.webp" type="image/webp"/>
        <source src="/img/work-for-us/img11.jpg" type="image/jpeg"/>
        <img src="/img/work-for-us/img11.jpg" alt="Work at Haymarket Belfast" class="w-100 h-auto"/>
      </picture>
    </div>
    <div class="col-lg-6 mob-px-4 text-center text-lg-left">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 h-100">
          <div class="pre-title-lines mob-mx-auto mb-4 mob-my-45"></div>
          <h1 class="mt-0">Want to Join Our Team?</h1>
          <p>We'd love to have you! All you need is a great personality and natural ability to deliver service that makes a night out something to remember! We know you have both!</p>
          <button type="button" class="btn btn-primary mt-3" onClick="scrollToElement('vaccancies')">View Vacancies</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="vaccancies" class="container-fluid position-relative pb-5 mob-pb-0">
  <div class="row">
    <div class="container pt-4">
      <div class="row">
        <div class="col-12 text-center">
          <div class="pre-title-lines mx-auto mb-4 mob-my-45"></div>
          <p class="mimic-h2 mb-5 mob-mt-4">Vacancies</p>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-6 pr-5 mob-px-3">
          <picture>
            <source src="/img/work-for-us/img9.webp" type="image/webp"/>
            <source src="/img/work-for-us/img9.jpg" type="image/jpeg"/>
            <img src="/img/work-for-us/img9.jpg" alt="Work at Haymarket Belfast" class="w-100 h-auto"/>
          </picture>
        </div>
        <div class="col-lg-6 mob-px-4">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <p class=" text-largest mt-0 mb-2 mob-mt-4"><b>Restaurant Manager</b></p>
              <p class="mb-4">Be part of Belfast's Newest exciting bar and restaurant at Haymarket Belfast! We are recruiting a Restaurant Manager to manage our busy bar & restaurant! Our Restaurant Managers are required to lead all members of their team and to constantly demonstrate skills, behaviours and attitudes necessary to consistently deliver a great service and atmosphere.</p>
              <p class="mb-2">Think you're right for the job?</p>
              <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#vaccancy1" aria-expanded="false" aria-controls="vaccancy1" onClick="scrollToElement('vaccancy1div')">Learn More</button>
            </div>
          </div>
        </div>
        <div id="vaccancy1div" class="col-12">
          <div class="pt-5 collapse" id="vaccancy1">
            <p><b>Key Responsibilities:</b></p>
            <p>As Restaurant Manager, you will be responsible for managing operations to deliver an exceptional customer experience is our busy venue. A key part of the role is to manage a large team, encourage positive reviews and repeat customer and develop processes for efficient restaurant operations, with an attention to cleanliness, detail and individual customer needs.</p>
            <ul>
              <li class="mb-0"><p class="mb-0">Maintain exceptional levels of customer service</p></li>
              <li class="mb-0"><p class="mb-0">Recruit, manage, train and develop the team</p></li>
              <li class="mb-0"><p class="mb-0">Drive sales to maximize budgeted revenue</p></li>
              <li class="mb-0"><p class="mb-0">Develop menus and generate offers and promotions to boost sales</p></li>
              <li class="mb-0"><p class="mb-0">Set departmental targets and objectives, work schedules, budgets, and policies and procedures</p></li>
              <li class="mb-0"><p class="mb-0">Constantly review customer experience, with a focus on 5-star service and constant improvement</p></li>
            </ul>

            <p class="mt-5"><b>Who are we looking for?</b></p>
            <p>We are looking for an experienced manager, with a track record in the F&B Industry. A quick reactor, who can develop a team to deliver 5-star service, grow the brand and have a strong attention to detail</p>
            <ul>
              <li class="mb-0"><p class="mb-0">Minimum of 2 years' experience in F&B management role in</p></li>
              <li class="mb-0"><p class="mb-0">Ability to work under pressure and manage a large team</p></li>
              <li class="mb-0"><p class="mb-0">Attention to cleanliness and detail</p></li>
              <li class="mb-0"><p class="mb-0">Willingness & desire to develop team members and self</p></li>
              <li class="mb-0"><p class="mb-0">Flexibility to respond to a range of different work situations</p></li>
            </ul>

            <p class="mt-5"><b>What are we able to offer you?</b></p>
            <ul>
              <li class="mb-0"><p class="mb-0">Competitive Salary</p></li>
              <li class="mb-0"><p class="mb-0">Career Progression</p></li>
              <li class="mb-0"><p class="mb-0">Regular staff incentives, prizes and bonuses</p></li>
              <li class="mb-0"><p class="mb-0">Fun, fast-paced working environment</p></li>
              <li class="mb-0"><p class="mb-0">Training, Development and Progression opportunities</p></li>
              <li class="mb-0"><p class="mb-0">Employee Discounts</p></li>
            </ul>
            <button class="btn btn-primary mt-4" onClick="scrollToElement('applyonline')">Apply Now</button>
          </div>
        </div>
        <div class="col-12">
          <hr class="line-primary my-5"/>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-6 pr-5 mob-px-3">
          <picture>
            <source src="/img/work-for-us/img10.webp" type="image/webp"/>
            <source src="/img/work-for-us/img10.jpg" type="image/jpeg"/>
            <img src="/img/work-for-us/img10.jpg" alt="Work at Haymarket Belfast" class="w-100 h-auto"/>
          </picture>
        </div>
        <div class="col-lg-6 mob-px-4">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <p class=" text-largest mt-0 mb-2 mob-mt-4"><b>Food & Beverage Manager</b></p>
              <p class="mb-4">We are looking for a hands-on Food and Beverage Manager who can take lead in our catering outlets across four of our hospitality/ leisure sites in Belfast and help with brand expansion in other parts of the UK. As Food and Beverage Manager you will be required to manage all operational duties of the kitchen by supervising and directing the workforce, ensuring high product quality, managing the kitchens recipe book and SOPs and driving customer service and queue management at peak times.</p>
              <p class="mb-2">Think you're right for the job?</p>
              <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#vaccancy5" aria-expanded="false" aria-controls="vaccancy5" onClick="scrollToElement('vaccancy5div')">Learn More</button>
            </div>
          </div>
        </div>
        <div id="vaccancy5div" class="col-12">
          <div class="pt-5 collapse" id="vaccancy5">
            <p><b>Key Responsibilities:</b></p>
            <ul>
              <li class="mb-0"><p class="mb-0">Delivering exceptional customer service in a fast-paced environment, with great attention to detail.</p></li>
              <li class="mb-0"><p class="mb-0">To take a pro-active approach to customer journey with a focus on look and feel, wait times and customer needs.</p></li>
              <li class="mb-0"><p class="mb-0">To proactively design and develop menus and recipes in line with company standards and customer expectation to maintain consistency.</p></li>
              <li class="mb-0"><p class="mb-0">To cost all dishes accordingly and maintain good GP and wastage levels.</p></li>
              <li class="mb-0"><p class="mb-0">To report all financials to head of finance on weekly basis.</p></li>
              <li class="mb-0"><p class="mb-0">To maintain current adequate allergen data.</p></li>
              <li class="mb-0"><p class="mb-0">To manage suppliers in line with best possible price for products and look after supply chain and orders.</p></li>
              <li class="mb-0"><p class="mb-0">To look after deliveries, stock rotation and weekly inventory.</p></li>
              <li class="mb-0"><p class="mb-0">To be responsible for all matters of all compliance and kitchen related aspects of health and safety/food safety, create and implement processes and ensure these are being followed.</p></li>
              <li class="mb-0"><p class="mb-0">To ensure 5* hygiene rating is maintained as per the current rating.</p></li>
              <li class="mb-0"><p class="mb-0">To create, implement and follow daily cleaning schedules & comply our opening and end of day/close down procedures.</p></li>
              <li class="mb-0"><p class="mb-0">To ensure that all equipment is safely maintained and where equipment is not functioning, the appropriate action is taken to remove or repair the item.</p></li>
              <li class="mb-0"><p class="mb-0">To promote a culture of safety and compliance of the whole team.</p></li>
              <li class="mb-0"><p class="mb-0">To supervise staff members, create training & develop them in all areas of service.</p></li>
              <li class="mb-0"><p class="mb-0">To remain up to date with all current health and safety and food safety legislation.</p></li>
              <li class="mb-0"><p class="mb-0">To maintain a professional and positive attitude at all times.</p></li>
              <li class="mb-0"><p class="mb-0">To attend and contribute to regular review meetings with senior management to ensure communications are regular and constructive.</p></li>
              <li class="mb-0"><p class="mb-0">To design and implement standard operating procedure for each day of the week with guidelines on quantities of products being displayed at what times to maximise sales and reduce waste.</p></li>
              <li class="mb-0"><p class="mb-0">To develop food offerings for new sites and set up kitchen layout and supply chain in line with budget.</p></li>
              
            </ul>
            <p>*Please note; This is not an exhaustive list of duties and responsibilities but will evolve and change depending on business needs.</p>
            <button class="btn btn-primary mt-4" onClick="scrollToElement('applyonline')">Apply Now</button>
          </div>
        </div>
        <div class="col-12">
          <hr class="line-primary my-5"/>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-6 pr-5 mob-px-3">
          <picture>
            <source src="/img/work-for-us/img12.webp" type="image/webp"/>
            <source src="/img/work-for-us/img12.jpg" type="image/jpeg"/>
            <img src="/img/work-for-us/img12.jpg" alt="Work at Haymarket Belfast" class="w-100 h-auto"/>
          </picture>
        </div>
        <div class="col-lg-6 mob-px-4">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <p class=" text-largest mt-0 mb-2 mob-mt-4"><b>Bar Tender</b></p>
              <p class="mb-4">What would a bar be without a good bar tender! We are looking for an energetic and passionate person to join our bar tending team! Expand your industry knowledge, develop skills in cocktail making and work with a likeminded team with flexible schedule, training & development programme and opportunities for professional development!</p>
              <p class="mb-2">Think you're right for the job?</p>
              <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#vaccancy2" aria-expanded="false" aria-controls="vaccancy2" onClick="scrollToElement('vaccancy2div')">Learn More</button>
            </div>
          </div>
        </div>
        <div id="vaccancy2div" class="col-12">
          <div class="pt-5 collapse" id="vaccancy2">
            <p><b>Who are we looking for?</b></p>
            <ul>
              <li class="mb-0"><p class="mb-0">Bar and Cocktail Experience with a passion to learn and develop</p></li>
              <li class="mb-0"><p class="mb-0">Strong serving and waiting experience</p></li>
              <li class="mb-0"><p class="mb-0">Strong work ethic and passion for customer service</p></li>
              <li class="mb-0"><p class="mb-0">Excellent interpersonal and communication skills</p></li>
              <li class="mb-0"><p class="mb-0">Strong organisation skills</p></li>
            </ul>

            <p class="mt-5"><b>What are we able to offer you?</b></p>
            <ul>
              <li class="mb-0"><p class="mb-0">Competitive Salary</p></li>
              <li class="mb-0"><p class="mb-0">Career Progression</p></li>
              <li class="mb-0"><p class="mb-0">Regular staff incentives, prizes and bonuses</p></li>
              <li class="mb-0"><p class="mb-0">Fun, fast-paced working environment</p></li>
              <li class="mb-0"><p class="mb-0">Training, Development and Progression opportunities</p></li>
              <li class="mb-0"><p class="mb-0">Employee Discounts</p></li>
            </ul>
            <button class="btn btn-primary mt-4" onClick="scrollToElement('applyonline')">Apply Now</button>
          </div>
        </div>
        <div class="col-12">
          <hr class="line-primary my-5"/>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-6 pr-5 mob-px-3">
          <picture>
            <source src="/img/work-for-us/img3.webp" type="image/webp"/>
            <source src="/img/work-for-us/img3.jpg" type="image/jpeg"/>
            <img src="/img/work-for-us/img3.jpg" alt="Work at Haymarket Belfast" class="w-100 h-auto"/>
          </picture>
        </div>
        <div class="col-lg-6 mob-px-4">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <p class=" text-largest mt-0 mb-2 mob-mt-4"><b>Server/Host</b></p>
              <p class="mb-4">We are looking for outgoing, passionate and hard-working individuals to join our energetic team! Being the face of our company, we need you to have natural ability to deliver a great service! Make new friends, gain valuable life skills and progress in one of Northern Ireland's Most Innovative Bars and Restaurant!</p>
              <p class="mb-2">Think you're right for the job?</p>
              <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#vaccancy3" aria-expanded="false" aria-controls="vaccancy3" onClick="scrollToElement('vaccancy3div')">Learn More</button>
            </div>
          </div>
        </div>
        <div id="vaccancy3div" class="col-12">
          <div class="pt-5 collapse" id="vaccancy3">

            <p><b>Who are we looking for?</b></p>
            <ul>
              <li class="mb-0"><p class="mb-0">Passionate and Outgoing Individual with a willingness to learn and development</p></li>
              <li class="mb-0"><p class="mb-0">Excellent communication and interpersonal skills</p></li>
              <li class="mb-0"><p class="mb-0">Someone who wants to gain new skills, make new friends and working in a fun team</p></li>
            </ul>

            <p class="mt-5"><b>Key Responsibilities:</b></p>
            <ul>
              <li class="mb-0"><p class="mb-0">Following directions and examples of senior staff and managers</p></li>
              <li class="mb-0"><p class="mb-0">Recommending dishes and drinks with full knowledge of product and daily special</p></li>
              <li class="mb-0"><p class="mb-0">Communicating with customers to resolve complaints and ensure satisfaction. Relay all issues to manager on duty</p></li>
              <li class="mb-0"><p class="mb-0">Adhering to checklists provided, keeping the restaurant clean and orderly at all times</p></li>
              <li class="mb-0"><p class="mb-0">Running food, drinks and resetting tables</p></li>
              <li class="mb-0"><p class="mb-0">Up selling products to enhance sales and customers experience</p></li>
              <li class="mb-0"><p class="mb-0">Following training plans set out by managers</p></li>
              <li class="mb-0"><p class="mb-0">Greeting customers in a timely manner</p></li>
              <li class="mb-0"><p class="mb-0">Building rapport with the customers, getting to know locals and chatting in a friendly professional manner to guests</p></li>
            </ul>

            <p class="mt-5"><b>What are we able to offer you?</b></p>
            <ul>
              <li class="mb-0"><p class="mb-0">Flexible schedule, ideal for students</p></li>
              <li class="mb-0"><p class="mb-0">Staff incentives, bonuses and rewards</p></li>
              <li class="mb-0"><p class="mb-0">Career progression, training and gain valuable life skills</p></li>
              <li class="mb-0"><p class="mb-0">Free Employee meals and other amazing group benefits</p></li>
            </ul>
            <button class="btn btn-primary mt-4" onClick="scrollToElement('applyonline')">Apply Now</button>
          </div>
        </div>
        <div class="col-12">
          <hr class="line-primary my-5"/>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-6 pr-5 mob-px-3">
          <picture>
            <source src="/img/work-for-us/img6.webp" type="image/webp"/>
            <source src="/img/work-for-us/img6.jpg" type="image/jpeg"/>
            <img src="/img/work-for-us/img6.jpg" alt="Work at Haymarket Belfast" class="w-100 h-auto"/>
          </picture>
        </div>
        <div class="col-lg-6 mob-px-4">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <p class=" text-largest mt-0 mb-2 mob-mt-4"><b>Chef de Partie </b></p>
              <p class="mb-4">We are looking for experienced chefs to join our team in the running of one of Belfast hottest street food Bars. We want you to have a passion and care of presentation of food bringing your own take and creativity to the table! </p>
              <p class="mb-2">Think you're right for the job?</p>
              <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#vaccancy4" aria-expanded="false" aria-controls="vaccancy4" onClick="scrollToElement('vaccancy4div')">Learn More</button>
            </div>
          </div>
        </div>
        <div id="vaccancy4div" class="col-12">
          <div class="pt-5 collapse" id="vaccancy4">
            <p><b>Key Responsibilities:</b></p>
            <ul>
              <li class="mb-0"><p class="mb-0">Setting up the kitchen for service</p></li>
              <li class="mb-0"><p class="mb-0">Ensuring appropriate food prepared for service daily</p></li>
              <li class="mb-0"><p class="mb-0">Preparing specific food items and components at each station within the kitchen.</p></li>
              <li class="mb-0"><p class="mb-0">Good knowledge of food and a passion for casual dining</p></li>
              <li class="mb-0"><p class="mb-0">Following directions provided by the head chef and other senior kitchen members</p></li>
              <li class="mb-0"><p class="mb-0">Collaborating with the rest of the culinary team to ensure high-quality food and service.</p></li>
              <li class="mb-0"><p class="mb-0">Always keeping all areas of the kitchen safe and sanitary</p></li>
              <li class="mb-0"><p class="mb-0">Stocktaking and ordering supplies for your station</p></li>
              <li class="mb-0"><p class="mb-0">Improving your food preparation methods based on feedback from management</p></li>
              <li class="mb-0"><p class="mb-0">Assisting in and confident in all other areas of the kitchen as required</p></li>
              <li class="mb-0"><p class="mb-0">Ensuring all food is always stored correctly, during and after service</p></li>
              <li class="mb-0"><p class="mb-0">Labelling of food items and ensuring stock rotation</p></li>
              <li class="mb-0"><p class="mb-0">Confident in using the kitchen equipment</p></li>
              <li class="mb-0"><p class="mb-0">Ensuring all health and safety form are completed regularly</p></li>
              <li class="mb-0"><p class="mb-0">Adhering to uniform standards</p></li>
              <li class="mb-0"><p class="mb-0">Pizza making skills desirable but not essential as training will be provided</p></li>
            </ul>

            <p class="mt-5"><b>What are we able to offer you?</b></p>
            <ul>
              <li class="mb-0"><p class="mb-0">Competitive Salary</p></li>
              <li class="mb-0"><p class="mb-0">Career Progression</p></li>
              <li class="mb-0"><p class="mb-0">Regular staff incentives</p></li>
              <li class="mb-0"><p class="mb-0">Fun, fast-paced working environment</p></li>
              <li class="mb-0"><p class="mb-0">Training, Development and Progression opportunities</p></li>
              <li class="mb-0"><p class="mb-0">Free staff meals</p></li>
              <li class="mb-0"><p class="mb-0">Regular training including Food and Hygiene Levels 2 & 3</p></li>
            </ul>
            <button class="btn btn-primary mt-4" onClick="scrollToElement('applyonline')">Apply Now</button>
          </div>
        </div>
        <div class="col-12">
          <hr class="line-primary my-5"/>
        </div>
      </div>
      
    </div>
  </div>
</div>
<div id="applyonline" class="container pb-5">
  <div class="row">
    <div class="col-12">
      <p class="mimic-h2">Apply</p>
      <p>Tell us who you are and send us your CV using this short form.</p>
      <apply-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'"></apply-form>
    </div>
  </div>
</div>
<div class="mb-5"></div>
@endsection
@section('scripts')
<script>
  function scrollToElement(element){
    $('html, body').animate({
      scrollTop: $("#"+element).offset().top -100
    }, 500);
  }
</script>
@endsection