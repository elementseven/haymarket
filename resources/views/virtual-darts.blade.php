@php
$page = 'Interactive Darts';
$pagetitle = "Interactive Darts @ The Stock Exchange @ Haymarket - Belfast's vibrant indoor/outdoor bar & street food hangout";
$metadescription = "Experience the thrill of Interactive darts at The Stock Exchange in Haymarket, Belfast! Our high-tech electric dart boards come equipped with automatic scoring, letting you focus on fun and drinks. Perfect for experts and beginners alike, choose from a variety of dart games for an unforgettable night out!";
$pagetype = 'stock';
$pagename = 'stock';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
  .stock-top-logo{
    width: 500px;
  }
  @media only screen and (max-width : 767px){
    .stock-top-logo{
      width: 80vw;
    }
  }
</style>
@endsection
@section('header')
<header class="container-fluid position-relative z-1 pt-5 mt-5 mob-mt-0 text-center">
  <img src="/img/logos/the-stock-exchange.svg" class="mb-4 mt-5 h-auto stock-top-logo" alt="The Armoury - Haymarket Belfast Logo" width="500" height="114" />
  <picture>
    <source srcset="/img/venues/stock/cocktail.webp" type="image/webp"/> 
    <source srcset="/img/venues/stock/cocktail.png" type="image/png"/> 
    <img src="/img/venues/stock/cocktail.png" type="image/png" width="550" height="1156" alt="The Stock Exchange - Interactive Darts at - Haymarket Belfast" class="cocktail-top-right h-auto"/>
  </picture>
</header>
@endsection
@section('content')
<div class="container pb-lg-5 ipadp-pt-0 position-relative z-2">
  <div class="row text-center text-lg-left pt-5 mob-pt-0">
    <div class="col-12 text-center mb-5">
      <div class="pre-title-lines mb-4 mt-5 mx-auto"></div>
      <h1 class="">Interactive Darts</h1>
      <p class=""><b>How does it work?</b></p>
    </div>
    <div class="col-lg-6">
      <picture>
        <source srcset="/img/venues/stock/darts-more-info.webp?v=2024-11-19" type="image/webp"/> 
        <source srcset="/img/venues/stock/darts-more-info.jpg?v=2024-11-19" type="image/jpeg"/> 
        <img src="/img/venues/stock/darts-more-info.jpg?v=2024-11-19" type="image/jpeg" width="350" height="350" alt="Interactive Darts at Haymarket Belfast" class="lazy w-100 h-auto op-45 shadow mb-5"/>
      </picture>
    </div>
    <div class="col-lg-6 pl-4 mob-px-3 text-center text-lg-left">
      <p>Enjoy a interactive game of classic darts digitized to entertain everyone from beginners to professionals. With 6 exciting state of the art games to try, there's something for everyone.</p>
      <p>Go for the big score with High Striker, knock out your opponents with Killer, take on the board with classic 501, or hone your accuracy with Shanghai! Our range of games will challenge everyone from the beginners to the pros! Get the whole group involved with accessible challenges and easy, medium and pro toe lines. Make friends, defeat your rivals and have an unforgettable evening!</p>
      <a href="https://ecom.roller.app/haymarketbelfast/stockexchange/en/products" target="_blank">
        <button type="button" class="btn btn-primary mt-3 booknowbtn">Book Now</button>
      </a>
    </div>
  </div>
  <div class="row pt-5 mt-5">
    <div class="col-12 pt-2 pl-4 mob-px-3 text-center text-lg-left">
      <h3 class="mb-4">FAQs</h3>
      <p class="text-primary"><b>Which games can I play?</b></p>
      <p class="mb-4">We have 6 great Interactive Darts games to enjoy during your visit and you can play as many games as you can fit into your booked time slot of 90 minutes. Chat to our dart hosts on arrival for some helpful tips and tricks.</p>
      <p class="text-primary"><b>We have a large group; will we have multiple playing areas?</b></p>
      <p class="mb-4">Absolutely! For larger groups, you'll be allocated multiple lanes (we will try our best to ensure these are beside each other). For large groups or corporate enquiries, please chat to our events team at <a href="mailto:info@haymarketbelfast.com">info@haymarketbelfast.com</a></p>
      <p class="text-primary"><b>Can anyone play Interactive Darts?</b></p>
      <p class="mb-4">Absolutely - from oche novices to aiming pros, interactive darts is perfect for groups of all ages and abilities! Please note we are an 18+ venue! </p>
      <p class="text-primary"><b>What time should we arrive for our booking?</b></p>
      <p class="mb-4 pb-lg-5">We recommend arriving 10 minutes before your booked slot, so we can get you set up and playing as soon as possible. You are always able to arrive early and enjoy some delicious food and drinks in our bar.</p>
      <h3 class="mb-4 mt-5">On The Day:</h3>
      <p>We ask all guests to arrive at the venue (Haymarket Belfast, 84 Royal Avenue) 10 minutes prior to the start time to ensure that you are ready to take flight, as soon as your session starts (and maybe have a cheeky cocktail to get started!)</p>
      <p>Our hosts will take you to your lane and run through some quick hints and tips, along with our 6 epic games. Then it is time to take aim and enjoy your 90-minute session! You and your competitors will take it in turns to hit the target. Our serving team will be on hand to take a food and drinks order, delivered straight to your table, and you can take advantage of our epic stock market bar.</p>
      <p>Once finished, our hosts will do a quick debrief and retrieve the darts. You will then be able to move out into ‘The Haymarket Courtyard’ to enjoy some more post-activity drinks. During weekends and holiday periods, we recommend booking and securing a table <a href="/the-courtyard#bookonline">HERE</a>.</p>
      <a href="https://ecom.roller.app/haymarketbelfast/stockexchange/en/products" target="_blank">
        <button type="button" class="btn btn-primary mt-3 booknowbtn">Book Now</button>
      </a>
    </div>
  </div>
</div>
<div class="pt-5"></div>
<seating-options-stock :activity="'darts'"></seating-options-stock>
<div class="container position-relative z-2 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 py-5">
      <mailing-list :id="'ml-1-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')

@endsection
@section('scripts')

@endsection