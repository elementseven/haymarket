@php
$page = 'Homepage';
$pagetitle = "Vouchers - Haymarket Belfast";
$metadescription = "Haymarket Belfast Vouchers";
$pagetype = 'vouchers-page';
$pagename = 'Vouchers';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
{{-- <div class="christmas-snow"></div> --}}
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mt-5 pt-5 mob-my-0">
  <div class="row">
    <div class="container container-wide">
      <div class="row justify-content-center mob-py-0">
        <div class="col-xl-7 col-lg-9 pt-3 mt-5 ipad-py-5 text-center">
          <div class="pre-title-lines mx-auto mb-4"></div>
  				<h1 class="page-title mob-page-title-smaller mb-2">Gift Vouchers</h1>
  				<p>Give the gift of food & drink with a monetary value voucher! Treat them to a big night out or romantic meal in one of Belfast's most lively venues! All the vouchers below are dispatched via email and have a 6 months expiry.</p>
  				<p class="mb-4">We have vouchers availble for each of our venues, scroll down or select a venue below.</p>
  				<a href="#haymarket-vouchers">
  					<button class="btn btn-primary mr-2 mob-mx-0 mob-mb-3">The Courtyard</button>
  				</a>
  				<br class="d-lg-none d-none" />
  				<a href="#armoury-vouchers">
  					<button class="btn btn-gold">The Armoury</button>
  				</a>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div id="haymarket-vouchers" class="container container-wide pb-5 position-relative z-2">
  <div class="row justify-content-center mt-5 mob-mt-0">
  	<div class="col-12 text-center mb-5 px-5">
		  <img src="/img/logos/the-courtyard.svg" class="mt-5 pt-5 h-auto" alt="Haymarket belfast Logo" width="400" height="163" style="max-width: 70vw;" />
		</div>
  	<div class="col-lg-3 mob-px-3 mb-4 pb-5">
    	<div class="voucher">
	      <picture>
			   	<source srcset="/img/vouchers/bottomless-brunch.webp" type="image/webp"/> 
			    <source srcset="/img/vouchers/bottomless-brunch.jpg" type="image/jpeg"/> 
			    <img src="/img/vouchers/bottomless-brunch.jpg" type="image/jpeg" alt="bottomless brunch vouchers at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
			  </picture>
	      <div class="voucher-offer-content">
	      	<p class="title text-large text-white mb-2">Bottomless<br/>Brunch Voucher</p>
	      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
	      	<div class="mb-3"></div>
	      	{{-- <p class="subtext mb-4">They don't want socks!! Trust us, everyone loves a boozy brunch you'll get major brownie points for this!</p> --}}
	      	<a href="https://giveavoucher.com/haymarket" target="_blank">
	      		<button type="button" class="btn btn-primary shadow">Buy Now </button>
	      	</a>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
    	<div class="voucher">
	      <picture>
			    <source srcset="/img/vouchers/50.webp" type="image/webp"/> 
			    <source srcset="/img/vouchers/50.jpg" type="image/jpeg"/> 
			    <img src="/img/vouchers/50.jpg" type="image/jpeg" alt="vouchers at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
			  </picture>
	      <div class="voucher-offer-content">
	      	<p class="title text-large text-white mb-2">£50 <br/>voucher</p>
	      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
	      	<div class="mb-3"></div>
	      	{{-- <p class="subtext mb-4">The perfect gift for the couple that have no similar interests, the serial dater, active partier or someone who appreciates good food & even better cocktails! </p> --}}
	      	<a href="https://giveavoucher.com/haymarket" target="_blank">
	      		<button type="button" class="btn btn-primary shadow">Buy Now </button>
	      	</a>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
    	<div class="voucher">
	      <picture>
			    <source srcset="/img/vouchers/cash.webp" type="image/webp"/> 
			    <source srcset="/img/vouchers/cash.jpg" type="image/jpeg"/> 
			    <img src="/img/vouchers/cash.jpg" type="image/jpeg" alt="vouchers at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
			  </picture>
	      <div class="voucher-offer-content">
	      	<p class="title text-large text-white mb-2">Customer<br/>Value Voucher</p>
	      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
	      	<div class="mb-3"></div>
	      	{{-- <p class="subtext mb-4">Just remember...the more the merrier! They'll be able to spend this fabulous voucher any time on our banging food & drinks menu!</p> --}}
	      	<a href="https://giveavoucher.com/haymarket" target="_blank">
	      		<button type="button" class="btn btn-primary shadow">Buy Now </button>
	      	</a>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
  </div>
</div>
<div id="armoury-vouchers" class="py-5 mb-5" style="background-color: #002030;">
	<div class="container container-wide">
	  <div class="row justify-content-center">
	  	<div class="col-12 text-center pt-5 px-5">
	  		<img src="/img/logos/the-armoury.svg" class="mb-5 h-auto" alt="The Armoury - Haymarket Belfast Logo" width="400" height="114" style="max-width: 70vw;"/>
	  	</div>
	    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
	    	<div class="voucher">
		      <picture>
				    <source srcset="/img/vouchers/armoury-free-game.webp" type="image/webp"/> 
				    <source srcset="/img/vouchers/armoury-free-game.jpg" type="image/jpeg"/> 
				    <img src="/img/vouchers/armoury-free-game.jpg" type="image/jpeg" alt="The Armoury Free game at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
				  </picture>
		      <div class="voucher-offer-content" style="background-color:#0D3B52">
		      	<p class="title text-large text-white mb-2">General<br/>Admission Voucher</p>
		      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
		      	<div class="mb-3"></div>
		      	{{-- <p class="subtext mb-4">Just remember...the more the merrier! They'll be able to spend this fabulous voucher any time on our banging food & drinks menu!</p> --}}
		      	<a href="https://ecom.roller.app/haymarketbelfast/giftvouchers/en/products" target="_blank">
		      		<button type="button" class="btn btn-gold shadow">Buy Now </button>
		      	</a>
		      </div>
		      <div class="backdrop-back"></div>
		    </div>
	    </div>
	    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
	    	<div class="voucher">
		      <picture>
				    <source srcset="/img/vouchers/armoury-brunch.webp" type="image/webp"/> 
				    <source srcset="/img/vouchers/armoury-brunch.jpg" type="image/jpeg"/> 
				    <img src="/img/vouchers/armoury-brunch.jpg" type="image/jpeg" alt="The Armoury Bottomless Brunch at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
				  </picture>
		      <div class="voucher-offer-content" style="background-color:#0D3B52">
		      	<p class="title text-large text-white mb-2">Bottomless<br/>Brunch Voucher</p>
		      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
		      	<div class="mb-3"></div>
		      	{{-- <p class="subtext mb-4">Just remember...the more the merrier! They'll be able to spend this fabulous voucher any time on our banging food & drinks menu!</p> --}}
		      	<a href="https://ecom.roller.app/haymarketbelfast/giftvouchers/en/products" target="_blank">
		      		<button type="button" class="btn btn-gold shadow">Buy Now </button>
		      	</a>
		      </div>
		      <div class="backdrop-back"></div>
		    </div>
	    </div>
	    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
	    	<div class="voucher">
		      <picture>
				    <source srcset="/img/vouchers/armoury-cash.webp" type="image/webp"/> 
				    <source srcset="/img/vouchers/armoury-cash.jpg" type="image/jpeg"/> 
				    <img src="/img/vouchers/armoury-cash.jpg" type="image/jpeg" alt="The Armoury Cash Voucher at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
				  </picture>
		      <div class="voucher-offer-content" style="background-color:#0D3B52">
		      	<p class="title text-large text-white mb-2">Cash<br/>Voucher</p>
		      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
		      	<div class="mb-3"></div>
		      	{{-- <p class="subtext mb-4">Just remember...the more the merrier! They'll be able to spend this fabulous voucher any time on our banging food & drinks menu!</p> --}}
		      	<a href="https://ecom.roller.app/haymarketbelfast/giftvouchers/en/products" target="_blank">
		      		<button type="button" class="btn btn-gold shadow">Buy Now </button>
		      	</a>
		      </div>
		      <div class="backdrop-back"></div>
		    </div>
	    </div>
	  </div>
	</div>
</div>
<div class="py-5 mob-py-0"></div>
@endsection
@section('modals')

@endsection
@section('scripts')
<style>
	body.vouchers-page{
		background-color: #262624;
	}
	body.vouchers-page .btn-primary{
		background-color: #d54148;
	}
	body.vouchers-page .btn-primary:hover{
		background-color: #93181d;
	}
	.vouchers-page #scroll-menu {
		background-color: #262624;
	}
	body.vouchers-page .opening-hours .today p{
		color: #d54148;
	}
	body.vouchers-page footer .opening-hours .today.mob-show p, 
	body.vouchers-page footer .text-primary {
		color: #d54148 !important;
	}
	body.vouchers-page .christmas-snow {
    position: fixed;
    width: 100vw;
    height: 100%;
    top: 0;
    left: 0;
    background-image: url(/img/graphics/snow.svg);
    background-size: 690px;
    background-repeat: initial;
    opacity: .3;
	}
	.voucher .voucher-offer-content{
		position: relative;
		z-index: 2;
		background-color: #343432;
		text-align: center;
		width: calc(100% - 2rem);
		padding: 1.5rem 1.5rem;
    margin-left: 1rem;
		margin-top: -75px;
		-webkit-box-shadow: 0 0 2rem rgba(0,0,0,.3)!important;
    -moz-box-shadow: 0 0 2rem rgba(0,0,0,.3)!important;
    box-shadow: 0 0 2rem rgba(0,0,0,.3)!important;
	}
	.voucher img{
		position: relative;
		z-index: 1;
	}
	.voucher .btn{
		position: absolute;
    width: 230px;
    left: calc(50% - 115px);
	}
</style>
@endsection