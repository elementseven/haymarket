@php
$page = 'Homepage';
$pagetitle = "Throwback Thursday - Every Thursday at Haymarket - Belfast's newest indoor/outdoor bar & street food hangout";
$metadescription = "Haymarket Belfast is Belfast's newest indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.";
$pagetype = 'throwback-thursday';
$pagename = 'throwback-thursday';
$ogimage = 'https://haymarketbelfast.com/img/events/throwback-thursday/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=din:wght@500&display=swap" rel="stylesheet">
@endsection
@section('header')
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 menu-padding  ">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center py-5 mob-py-0">
        <div class="col-12 col-md-6 text-center mob-px-0 mob-mb-3">
          <picture>
            <source srcset="/img/events/throwback-thursday/logo.webp" type="image/webp"/> 
            <source srcset="/img/events/throwback-thursday/logo.png" type="image/png"/> 
            <img src="/img/events/throwback-thursday/logo.png" type="image/png" width="350" height="350" alt="Haymarket Throwback Thursday" class="lazy mb-4 px-4 mob-px-5 mt-5 w-100 h-auto"/>
          </picture>
          <p class="text-largest din text-uppercase mb-3 line-height-1">Throwback Shot Roulette<br/>DJ BRINGING YOU THE ULTIMATE TBT BANGERS</p>
          <p class="mimic-h2 din text-uppercase text-pink mb-4 line-height-1"><b>8PM TO LATE <br/>FREE ENTRY ALL NIGHT!</b></p>
          <p class="text-larger mb-5 text-uppercase mb-0 din"><b class="text-pink">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode('Throwback Thursday at Haymarket Belfast')}}&amp;summary={{urlencode('Throwback Thursday at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode('Throwback Thursday at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode('Throwback Thursday at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 pb-5">
      <parties-slider :bg="'#4A2A5F'" :text="'#fff'" :lines="'#fff'"></parties-slider>
    </div>
  </div>
</div>
<seating-options :bg="'#4A2A5F'" :text="'#fff'" :lines="'#fff'"></seating-options>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :id="'ml-1-'" :bg="'#4A2A5F'" :text="'#fff'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/29664?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
</script>
@endsection