@php
$page = 'Homepage';
$pagetitle = "Boozy Brunch Club - Every Sunday at Haymarket - Belfast's newest indoor/outdoor bar & street food hangout";
$metadescription = "Haymarket Belfast is Belfast's newest indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.";
$pagetype = 'boozy-brunch-club';
$pagename = 'boozy-brunch-club';
$ogimage = 'https://haymarketbelfast.com/img/events/boozy-brunch-club/og.png';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=din:wght@500&display=swap" rel="stylesheet">
<style>
  #main-menu button,
  .seating-option button{
    background-color:#000 !important;
    box-shadow: 0rem 0rem 2rem rgba(0, 0, 0, 0.3) !important;
  }
  .opening-hours .today p{
    color: #fff !important;
  }
</style>
@endsection
@section('header')
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo-white.png" class="mt-5 pt-5 h-auto header-top-logo position-relative" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center py-5 mob-py-0">
        <div class="col-12 col-md-8 text-center text-lg-left mob-px-0 mob-mb-3 d-lg-none">
          <img src="/img/logos/bb-logo.svg" alt="Haymarket Boozy Brunch Club logo" width="280" class="w-100 h-auto px-5 mt-3 mob-mt-5 mb-3" />
          <p class="text-larger text-uppercase mb-0 din"><b>Every Fri | 5pm - 6:30pm & 7pm - 8:30pm*</b></p>
          <p class="text-larger text-uppercase mb-0 din"><b>Every Sat | 12:30pm - 2pm, 2:30pm - 4pm & 5pm - 6:30pm*</b></p>
          <p class="text-larger text-uppercase mb-0 din"><b>Every Sun | 3:30pm - 5pm*</b></p>
          <p class="text-larger text-uppercase din mb-2"><b>Enjoy bottomless cocktails, prosecco & beer <br/>& tasty street food from just £35</b></p>
          <p class="din mb-0">*non-alcoholic options are available upon request!</p>
          <img src="/img/events/boozy-brunch-club/squiggle.svg?v3.0" class="w-100 px-3 mt-3 mb-4" alt="squiggle"/>
          <button class="btn btn-primary booknowbtn mb-4">Book Now</button>
          <p class="text-small"><a href="/terms-and-conditions" class="text-dark"><b><u>Please see our terms & conditions to our brunch</u></b></a></p>
          <p class="text-small">*Reservations must be paid in advance for booking to be valid.</p>
          <p class="text-small mb-0">*Please note, you may be required to give the table back after your session has finished. </p>
          <p class="text-small text-uppercase letter-spacing din">limited menu</p>
          <p class="text-larger text-uppercase mb-1 mb-5 din"><b class="text-pink">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-red">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}&amp;summary={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-red">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-red">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-red">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
        <div class="col-lg-5 col-md-8 mob-pl-0 mob-mb-5 mt-5 ipadp-mt-0 mob-mt-0 pr-5 mob-px-3 ipadp-px-3">
          <div id="bookonline" class="res-diary-holder d-inline-block shadow">
            <div class="res-diary-inner">
              <div class="loader loader-inner d-table">
                <div class="d-table-cell align-middle">
                  <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
              </div>
              <div class="position-relative z-2">
                <div id="rd-widget-frame" style="max-width: 600px; margin: auto;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 pt-5 mob-mt-5 text-center px-0 mob-mb-3 d-none d-lg-block">
          <img src="/img/logos/bb-logo.svg" alt="Haymarket Boozy Brunch Club logo" width="350" height="267" class="w-100 mt-3 mob-mt-5 mb-4 h-auto" />
          <p class="text-larger text-uppercase mb-0 din"><b>Every Fri | 5pm - 6:30pm & 7pm - 8:30pm*</b></p>
          <p class="text-larger text-uppercase mb-0 din"><b>Every Sat | 12:30pm - 2pm, 2:30pm - 4pm & 5pm - 6:30pm*</b></p>
          <p class="text-larger text-uppercase mb-0 din"><b>Every Sun | 3:30pm - 5pm*</b></p>
          <p class="text-larger text-uppercase din mb-2"><b>Enjoy bottomless cocktails, prosecco & beer & tasty street food from just £35</b></p>
          <p class="din mb-0">*non-alcoholic options are available upon request!</p>
          <img src="/img/events/boozy-brunch-club/squiggle.svg?v3.0" class="w-100 px-3 mt-2 mb-4" alt="squiggle"/>
          <p class="text-small"><a href="/terms-and-conditions" class="text-dark"><b><u>Please see our terms & conditions to our brunch</u></b></a></p>
          <p class="text-small">*Reservations must be paid in advance for booking to be valid.</p>
          <p class="text-small mb-0">*Please note, you may be required to give the table back after your session has finished.</p>
          <p class="text-small text-uppercase letter-spacing din">limited menu</p>
          <p class="text-larger text-uppercase mb-1 din"><b class="text-pink">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-red">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}&amp;summary={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-red">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-red">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-red">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')

<div class="container pb-5 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 pb-5">
      <bb-slider :bg="'#c74d48'" :text="'#000'" :lines="'#fff'"></bb-slider>
    </div>
  </div>
</div>
<seating-options-brunch :bg="'#c74d48'" :text="'#000'" :lines="'#fff'" :food="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2023-08-04'" :cocktails="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2023-08-04'"></seating-options-brunch>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :id="'ml-1-'" :bg="'#c74d48'" :text="'#000'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/29665?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
</script>
@endsection