@php
$page = 'Homepage';
$pagetitle = "Win or Booze Quiz Night - Every Wednesday at Haymarket - Belfast's newest indoor/outdoor bar & street food hangout";
$metadescription = "Haymarket Belfast is Belfast's newest indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.";
$pagetype = 'win-or-booze';
$pagename = 'win-or-booze';
$ogimage = 'https://haymarketbelfast.com/img/events/win-or-booze/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=din:wght@500&display=swap" rel="stylesheet">
@endsection
@section('header')
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center py-5 mob-py-0">
        <div class="col-12 col-md-8 text-center text-lg-left mob-px-0 mob-mb-3 d-lg-none">
          <picture>
            <source srcset="/img/events/win-or-booze/logo.webp" type="image/webp"/> 
            <source srcset="/img/events/win-or-booze/logo.png" type="image/png"/> 
            <img src="/img/events/win-or-booze/logo.png" type="image/png" width="350" height="350" alt="Haymarket Win or Booze Quiz night" class="lazy px-4 mt-5 w-100 h-auto"/>
          </picture>
          <p class="text-largest din text-uppercase letter-spacing text-pink mb-0">Wednesdays from 5pm</p>
          <p class="mimic-h1 din text-uppercase mb-4 text-beage">2 for 1 COCKTAILS!</p>
          <p class="text-larger mb-5 text-uppercase mb-1 din"><b class="text-pink">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode('Win or Booze Quiz Night at Haymarket Belfast')}}&amp;summary={{urlencode('Win or Booze Quiz Night at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode('Win or Booze Quiz Night at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode('Win or Booze Quiz Night at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
        <div class="col-lg-5 col-md-8 mob-pl-0 mob-mb-5 mt-5 ipadp-mt-0 mob-mt-0 pr-5 mob-px-3 ipadp-px-3">
          <div id="bookonline" class="res-diary-holder d-inline-block shadow">
            <div class="res-diary-inner">
              <div class="loader loader-inner d-table">
                <div class="d-table-cell align-middle">
                  <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
              </div>
              <div class="position-relative z-2">
                <div id="rd-widget-frame" style="max-width: 600px; margin: auto;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 pt-5 mob-mt-5 text-center mob-px-0 mob-mb-3 d-none d-lg-block">
          <picture>
            <source srcset="/img/events/win-or-booze/logo.webp" type="image/webp"/> 
            <source srcset="/img/events/win-or-booze/logo.png" type="image/png"/> 
            <img src="/img/events/win-or-booze/logo.png" type="image/png" width="350" height="350" alt="Haymarket Win or Booze Quiz night" class="lazy w-100 h-auto"/>
          </picture>
          <p class="text-largest din text-uppercase letter-spacing text-pink mb-0">Wednesdays from 5pm</p>
          <p class="mimic-h1 din text-uppercase mb-4 text-beage">2 for 1 COCKTAILS!</p>
          <p class="text-larger text-uppercase mb-1 din"><b class="text-pink">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode('Win or Booze Quiz Night at Haymarket Belfast')}}&amp;summary={{urlencode('Win or Booze Quiz Night at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode('Win or Booze Quiz Night at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode('Win or Booze Quiz Night at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 pb-5">
      <parties-slider :bg="'#141414'" :text="'#fff'" :lines="'#fff'"></parties-slider>
    </div>
  </div>
</div>
<seating-options :bg="'#141414'" :text="'#fff'" :lines="'#fff'"></seating-options>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :id="'ml-1-'" :bg="'#141414'" :text="'#fff'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/26681?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
</script>
@endsection