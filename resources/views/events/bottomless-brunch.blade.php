@php
$page = 'Homepage';
$pagetitle = "Bottomless Brunch - Every Friday, Saturday & Sunday at Haymarket Belfast. Enjoy 90 mins of bottomless cocktails, beer, prosecco & tasty street food!";
$metadescription = "Enjoy 90 mins of bottomless cocktails, beer, prosecco & tasty street food every Friday, Saturday & Sunday!";
$pagetype = 'bottomless-brunch';
$pagename = 'bottomless-brunch';
$ogimage = 'https://haymarketbelfast.com/img/events/boozy-brunch-club/og.png';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=din:wght@500&display=swap" rel="stylesheet">
@endsection
@section('header')
<div class="bottomless-brunch-top-bg"></div>
<header class="container position-relative z-1 pt-5 text-center logo-lines mob-px-4 mob-mb-3">
  <div class="row half_row justify-content-center pt-5 pb-3 mob-px-3">
    <div class="col-lg-8 half_col pt-5 mob-pt-0">
      <div class="venue-top-no-vid">
      	<img src="/img/logos/bb-logo-white.svg" alt="Haymarket Bottomless Brunch logo" width="500" height="114" class="mb-4 h-auto" />
        <p class="text-white mb-4">Enjoy 90 mins of bottomless cocktails, beer, prosecco & a tasty main, followed by live music in The Courtyard every Friday, Saturday & Sunday!</p>
        <button type="button" class="btn btn-primary booknowbtn mx-auto position-relative z-2">Book Now</button>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div id="homepage-top" class="container-fluid position-relative z-1 mob-pt-5">
  <div class="row">
    <div class="container mb-5 mob-mb-0">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-8 mob-pl-0 mt-5 ipadp-mt-0 mob-mt-0 pr-5 mob-px-3 ipadp-px-3">
          <div id="bookonline" class="res-diary-holder d-inline-block shadow">
            <div class="res-diary-inner">
              <div class="loader loader-inner d-table">
                <div class="d-table-cell align-middle">
                  <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
              </div>
              <div class="position-relative z-2">
                <div id="rd-widget-frame" style="max-width: 600px; margin: auto;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 pt-5 mob-pt-4">
        	<img src="/img/events/boozy-brunch-club/squiggle.svg?v3.0" class="w-100 my-4" alt="squiggle"/>
          <p class="mb-0" style="font-size: 1rem;"><b>Friday - £35pp:</b>  5pm - 6:30pm & 7pm - 8:30pm*</p>
          <p class="mb-0" style="font-size: 1rem;"><b>Saturday - £39.95pp:</b> 12:30pm - 2pm, 2:30pm - 4pm, 5pm - 6:30pm*</p>
          <p class="mb-0" style="font-size: 1rem;"><b>Sunday - £35pp:</b> 3:30pm - 5pm*</p>
          <p class="mb-2">Enjoy bottomless cocktails, prosecco & beer & tasty street food from just £35! Non-alcoholic options are available upon request!</p>
          <img src="/img/events/boozy-brunch-club/squiggle.svg?v3.0" class="w-100 my-4" alt="squiggle"/>
          <p class="text-small"><a href="/terms-and-conditions" class="text-primary"><b><u>Please see our brunch terms & conditions</u></b></a></p>
          <p class="text-small mb-0">*Limited menu.</p>
          <p class="text-small mb-0">*Reservations must be paid in advance for booking to be valid.</p>
          <p class="text-small mb-3">*Please note, you may be required to give the table back after your session has finished.</p>
          <img src="/img/events/boozy-brunch-club/squiggle.svg?v3.0" class="w-100 my-4" alt="squiggle"/>
          <p class="text-larger text-uppercase mb-1 din"><b class="text-pink">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}&amp;summary={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode('Boozy Brunch Club - Every Sunday at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- <div class="container pb-5 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 pb-5">
      <bb-slider :bg="'#f29a30'" :text="'#fff'" :lines="'#fff'"></bb-slider>
    </div>
  </div>
</div> --}}
<seating-options-brunch :bg="'#f29a30'" :text="'#000'" :lines="'#000'" :food="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2024-05-15'" :cocktails="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2024-05-15'"></seating-options-brunch>
<div class="container position-relative z-2">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :id="'ml-1-'" :bg="'#f29a30'" :text="'#000'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/29665?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
window.addEventListener('load', function() {
  document.querySelectorAll('.booknowbtn').forEach(function(button) {
    button.addEventListener('click', function() {
      var targetElement = document.getElementById("bookonline");
      var offset = 100;
      var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
      window.scrollTo({
        top: targetPosition - offset,
        behavior: 'smooth'
      });
    });
  });
});
</script>
@endsection