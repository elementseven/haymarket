@php
$page = 'Homepage';
$pagetitle = "Select Your Bottomless Brunch | Haymarket - Belfast's best indoor/outdoor bar & street food hangout";
$metadescription = "Check out our Bottomless Brunch offers at Haymarket Belfast";
$pagetype = 'offers';
$pagename = 'offers';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=din:wght@500&display=swap" rel="stylesheet">
<style type="text/css">
  /*body{
    background-color: #0d0b12 !important;
  }*/
  /*#scroll-menu{
    background-color: #0c0c0c !important;
  }
  .btn-primary{
    background-color: #f3ba38 !important;
  }*/
  /*.mailing-list-signup a,
  .text-primary,
  .opening-hours .today p{
    color: #f3ba38 !important;
  }
  .menu .menu-links .menu-item,
  footer a{
    color: #fff !important;
  }
  .price{
    position:absolute;
    top:0.5rem;
    right:1.5rem;
    padding:18px 0 0;
    font-size:26px;
    color:#fff;
    background-image:url('/img/graphics/price.svg');
    background-size: contain;
    background-repeat: no-repeat;
    width: 80px;
    height:80px;
    font-family: "din", sans-serif;
    font-weight:900;
    text-align:center;
    transform: rotate(13deg);
  }*/
  .bb-logo{
    max-width: 80vw !important;
  }
</style>
@endsection
@section('header')
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <picture>
    <source data-srcset="/img/graphics/chips-burger.webp" type="image/webp"/> 
    <source data-srcset="/img/graphics/chips-burger.png" type="image/png"/> 
    <img data-src="/img/graphics/chips-burger.png" type="image/png" width="550" height="1156" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy chips-burger-top-right h-auto"/>
  </picture>
  <div class="row">
    <div class="container">
      <div class="row justify-content-center pt-5 mob-py-0 mob-mb-5">
        <div class="col-lg-9 text-center pt-5 mob-pt-0">
          <div class="text-center mt-5 mob-mt-0 d-lg-none">
            <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>

          </div>

          <img src="/img/logos/bb-logo-white.svg" width="600" class="bb-logo mt-5 pt-5 mob-mt-0" alt="Bottomless Brunch Takeovers logo" />
          <p class="text-larger text-uppercase mt-3"><b>Which are you feeling?</b></p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container container-wide position-relative z-2">
  <div class="row justify-content-center">
    @foreach($brunches as $b)
    <div class="col-lg-4 mb-5 text-center">
      <picture>
        <source srcset="{{$b->webp}}" type="image/webp"/> 
        <source srcset="{{$b->normal}}" type="{{$b->mime}}"/> 
        <img src="{{$b->normal}}" width="507" height="507" type="image/jpg" alt="{{$b->title}}" class="w-100 h-auto"/>
      </picture>
      {{-- <div class="price">£45</div> --}}
      @if($b->link)
      <a href="{{$b->link}}">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
      @elseif($b->roller)
      <a href="{{$b->roller}}">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
      @else
      <a href="/offers/{{$b->id}}/{{$b->slug}}">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
      @endif
    </div>
    @endforeach
    <div class="col-12 mt-5 text-center">
      <p class="text-large mb-4">Please see T&C's of all Brunch Bookings</p>
      <a href="/terms-and-conditions">
        <button class="btn btn-primary" type="button">FAQs</button>
      </a>
    </div>
  </div>
</div>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :id="'ml-2-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')

@endsection