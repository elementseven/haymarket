@php
$page = 'Privacy Policy';
$pagetitle = 'Privacy Policy | Haymarket Belfast';
$metadescription = 'Infortmation collection & use';
$pagetype = 'white';
$pagename = 'privacy-policy';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header class="container position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-12 pt-5 mob-pt-0 ipadp-pt-0 text-center text-lg-left">
      <div class="pre-title-lines mob-mx-auto mb-4 mob-my-45"></div>
      <h1 class="mob-mt-0">Privacy Policy</h1>
      <p>Last updated: 28th June 2021</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-1">
  <div class="row">
    <div class="container pt-4">
        <h4 class="mb-4">Infortmation collection & use</h4>
        
        <p>We collect information from our users and their computers when they view our websites. We collect both personally identifiable and non-personally identifiable information. We also collect information from you when you request support or make enquiries about our products or services. </p>

        <p>We may ask you to provide us with personally identifiable information at various points while you use our websites, such as when you register for a website account or when you purchase products or services from us. Examples of the types of personally identifiable information we collect range from your email address to your name, address, phone or fax number, email address, and credit card information. </p>

        <p>We use the personally identifiable information you provide about yourself to fulfil your requests for products and services, to contact you about your account, to complete and support your activities, to respond to your enquiries about our offerings, and to customise the content of our websites. We use your email address and other personally identifiable information to send you service notifications about your account, about the services you currently use, if any, and about changes to our Privacy Statements, Acceptable Use Policy, Website Terms of Use, or other legal or policy matters. If you have given us permission, we will use your email address and other personally identifiable information to send you our newsletter or to communicate with you about products and services we believe may be of interest to you. </p>

        <p>We also may use some of this information in anonymous or statistical forms, such as aggregating geographic information from user addresses or types of industries in which users work, to better understand our customers. We also may ask you to provide us with optional information about your interests to be used for similar purposes. We will ask your permission before using this type of information in a way that personally identifies you. </p>

        <p class="mt-5"><b>Log Files</b></p>

        <p>We use the information in our server log files to analyse trends in the usage of our websites, to administer and maintain our websites, to track a user’s path through our websites, and to gather broad demographic statistics about our websites and the ways in which they are used. Most of the information in these files is not unique to you as an individual, such as the files requested and pages you visit on our websites, the websites from which you may have been referred to our websites, and information about the type of browser you use, and its settings. Some of the log files contain IP addresses and other personally identifiable information, which are used to identify user connections and to resolve technical issues related to administration of our websites. </p>

        <p class="mt-5"><b>Cookies</b></p>

        <p>Changes to online privacy laws across the EU since May 2011 mean that visitors to all EU websites will be asked for their consent for the use of ‘cookies’ and other similar web technologies. The new legislation was introduced to help you choose whether or not to allow each website you visit to use cookies when you visit them. All of the countries in the EU have similar laws, and all websites are required to gain your consent.</p>

        <p>‘Cookies’ are a type of file stored on your internet device (PC, phone or tablet). They are used by us and other websites to enable you to log in, shop and personalise your online experience. Cookies can also be used to track your activity and behaviour online. They will provide us with important insights into the quality of our service, which will enable us to improve our services and provide a better experience for you. </p>

        <p>Some types of cookies can also be used to track your activity across lots of different websites. This builds up a profile of your interests. It also enables companies to target you with adverts that they think you are likely to be interested in. </p>

        <p>It is your right to choose if you give your consent or not, but you should be aware that in some cases you may not be able to use or see all of a website if you do not allow the use of cookies. </p>


        <p class="mt-5"><b>Transparent GIF files</b></p>

        <p>In addition to our own cookies, we use pixels, or transparent GIF files, on our sites to help manage our online advertising and email marketing. These GIF files are provided by one or more third-party service providers, based on our specification as to where they are used. The files enable our service providers to recognise a unique cookie in your web browser, which in turn enables us and our service providers to learn which advertisements and emails bring you to our website and how you use the site.</p>
        <p>Examples of the ways in which Haymarket Belfast and our third-party service providers use GIF files include:</p>

        <ul>
          <li><p>Tracking customer response to our advertisements, emails and site content</p></li>
          <li><p>Determining your ability to receive HTML-based email messages. This capability helps us, or our service providers, send you email in a format you can read</p></li>
          <li><p>Knowing how many users open an email, and allowing our service providers to compile aggregated statistics about an email campaign for us</p></li>
          <li><p>Allowing us to better target interactive advertising; enhancing customer support and site usability; and providing offers and promotions that we believe would be of interest to you – if you’ve given us permission to do so by opting in to receiving them.</p></li>
          <li><p>To conduct these or other activities, we or our service providers sometimes may link personal information you have previously provided us (such as, for example, your name, email address, and the version of our products that you have purchased) to the information the GIF files provide about how you arrive at, navigate through, and leave our sites.</p></li>
        </ul>

        <p class="mt-5"><b>Disclosure of information</b></p>

        <p>We will not disclose personally identifiable information we collect from you to third parties without your permission, except to the extent necessary:</p>

        <ul>
          <li><p>to fulfil your requests for products or services</p></li>
          <li><p>to protect the security of our software, services, websites, and users</p></li>
          <li><p>to protect ourselves from liability</p></li>
          <li><p>to respond to legal process or comply with law</p></li>
          <li><p>in connection with a merger, acquisition, or liquidation of the company.</p></li>
        </ul>

        <p>We share statistical information about users of our websites with third parties, including our resellers, customers, potential customers, distributors, and third parties that offer products or services for use with the Response Mail software. We do not link this information to any personally identifiable information. </p>
        <p>We occasionally contract with other companies to provide limited services to us or on our behalf, such as billing or customer support services. When we do so, we provide these companies with only limited access to personally identifiable information.</p>
        <p>In addition, we may occasionally join with other reputable companies to provide users with third-party products and services. We will notify you at the time that you sign up for these products or services that these third parties will obtain personally identifiable information to provide the requested products or services. This Privacy Statement does not cover these companies’ use of the information.</p>

        <p class="mt-5"><b>Security</b></p>

        <p>We take measures to protect the security of personally identifiable information we hold about you. These measures include:</p>

        <ul>
          <li><p>Encrypting sensitive information during transmission;</p></li>
          <li><p>Limiting the number of our employees who have access to personally identifiable information;</p></li>
          <li><p>Requiring employees with access to personally identifiable information to take appropriate measures to safeguard it;</p></li>
          <li><p>Storing your information in secure places that are protected using standard security technologies.</p></li>
        </ul>

        <p class="mt-5"><b>Changes to this privacy statement</b></p>

        <p>Haymarket Belfast will occasionally update this Privacy Statement as our business and products evolve. When we do, we will also revise the ‘last updated’ date at the top of the Privacy Statement. For material changes to this Privacy Statement, Haymarket Belfast will notify you by placing a prominent notice on the website. </p>

        <p class="mt-5"><b>Contact us</b></p>

        <p>If you have questions about this Privacy Statement, its implementation, or our practices, please contact us.</p>

    </div>
  </div>
</div>
<div class="container py-5 mob-px-4 mob-py-0">
  <div class="row my-5">
    <div class="col-lg-6 py-5 mob-py-0 text-center text-lg-left">
      <div class="pre-title-lines mob-mx-auto mb-4"></div>
      <p class="mimic-h2 mob-smaller"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline ">Have a question for us?</span></p>
      <a href="{{route('contact')}}"><button type="button" class="btn btn-primary">Get in touch</button></a>
    </div>
  </div>
</div>
@endsection