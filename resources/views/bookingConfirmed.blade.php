@php
$page = 'Haymarket Belfast - Booking Confirmed';
$pagetitle = 'Booking Confirmed | Haymarket Belfast';
$metadescription = 'Booking Confirmed | Haymarket Belfast';
$pagetype = 'white';
$pagename = 'bookingConfirmed';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('head_section')
<script>
  dataLayer.push({
    'transactionId': "{{$_GET['bookingReference']}}"
  });
</script>
@endsection
@section('header')
<picture>
  <source srcset="/img/graphics/burger-beer.webp" type="image/webp"/> 
  <source srcset="/img/graphics/burger-beer.png" type="image/jpeg"/> 
  <img src="/img/graphics/burger-beer.png" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy burger-beer-top-left"/>
</picture>
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header class="container position-relative pb-5 mb-5 z-2">
  <div class="row pt-5 mob-pt-0">
    <div class="col-lg-12 mt-5 text-center text-lg-left">
      <p class="text-primary text-uppercase mb-2"><b>We'll see you soon</b></p>
      <h1>Booking<br/>Confirmed</h1>
      <p class="text-large mb-3">We look forward to seeing you at Haymarket soon! If you need directions, click the button below.</p>
      <p class="mb-4">Transaction reference: {{$_GET['bookingReference']}}</p>
      <a href="https://www.google.com/maps/dir//haymarket+belfast/data=!4m6!4m5!1m1!4e2!1m2!1m1!1s0x4861095ef56dd2d9:0x7272466393a309bb?sa=X&ved=2ahUKEwjkqMvPwbzxAhVloFwKHXg_CQ4Q9RcwAHoECAcQBA" target="_blank" class="text-primary"><div class="btn btn-primary">Get Directions</div></a>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container position-relative z-1">
  <div class="row">
    <div class="col-12 pt-4">
    
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection