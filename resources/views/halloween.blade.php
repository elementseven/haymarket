@php
$page = 'Homepage';
$pagetitle = "Halloween 2021 at Haymarket Belfast - Till Death do us Party!";
$metadescription = "Halloween at Haymarket Belfast Belfast's newest indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.";
$pagetype = 'black';
$pagename = 'halloween';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<halloween-header></halloween-header>
@endsection
@section('content')
<div class="py-5 my-5 mob-my-0 mob-py-0"></div>
@endsection
@section('modals')

@endsection
@section('scripts')
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/29274?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -250
      }, 500);
    });
  });
</script>
@endsection