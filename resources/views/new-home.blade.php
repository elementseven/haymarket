@php
$page = 'Homepage';
$pagetitle = "Haymarket - Belfast's vibrant indoor/outdoor bar & street food hangout";
$metadescription = "Haymarket Belfast is Belfast's vibrant indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.";
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preload" media="(max-width: 767px)" as="image" href="https://haymarketbelfast.com/build/assets/home-top-bg-mob-DNuVvZoU.webp">
<link rel="preload" media="(min-width: 768px)" as="image" href="https://haymarketbelfast.com/build/assets/home-top-bg-B91YvCEf.webp">
@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 px-0">
  <div class="d-flex overflow-hidden bg home-top-bg home-vid" style="min-height: 545px">
    <div class="container-fluid px-0 h-100" style="top: 0;left: 0;min-width: 970px;">
      <div id="videoContainer" class="embed-responsive embed-responsive-16by9 d-none d-lg-block">
        <iframe id="deferredIframe" width="640" height="361" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen loading="lazy" src="https://player.vimeo.com/video/689775525?h=5433e291e4&autoplay=1&loop=1&autopause=0&background=1"></iframe>
      </div>
    </div>
    <div class="new-home-top text-center pt-5">
      <img src="/img/logos/logo.svg" class="mob-mt-5 mb-4 h-auto w-100 mob-mx-auto" alt="haymarket belfast Logo" width="438" height="163" style="max-width: 400px;" />
      <p class="mb-5">Haymarket Belfast has something for everyone in the heart of Belfast's City Centre! Enjoy great drinks, food and live music and sport in <a href="/the-courtyard"><b>The Courtyard</b></a>; host a party to remember in our <a href="/parties"><b>Private Rooms</b></a>; try out our <a href="/bottomless-brunch-new"><b>Bottomless Brunch</b></a> with free-flowing cocktails and a delicious meal! or take a shot at <a href="/the-armoury"><b>The Armoury</b></a> - our brand-new simulated shooting range with drinks and food brought straight to your private booth! Find out more below!</p>
    </div>
  </div>
  <div class="video-grad"></div>
  <div class="video-gradient"></div>
</header>
@endsection

@section('content')
<div class="container container-wide position-relative z-2 home-venues">
  <div class="row half_row mb-3">
    <div class="col-lg-6 half_col mob-mb-4">
      <div class="home-venue bg shadow d-table w-100 lazy" style="min-height: 400px;" data-bg-class="stock-exchange-bg">
        <div class="venue-details d-table-cell align-middle">
          <div class="venue-details-inner px-4">
            <img src="/img/logos/the-stock-exchange.svg" alt="Haymarket - The Stock Exhchange logo" width="400" height="91" class="mb-3 h-auto" style="max-width: 90%;" />
            <p class="text-one mb-4">Welcome to the Stock Exchange. From Interactive Darts, and Shuffle Boards, to our Stock Market Bar where drinks prices crash with the market! Elevate your night out with some competitive fun.</p>
            <a href="/the-stock-exchange">
              <button type="button" class="btn btn-primary" style="background-color: #b75503;">The Stock Exchange</button>
            </a>
          </div>
        </div>
      </div>
    </div><!-- end col -->

    <div class="col-lg-6 half_col">
      <div class="home-venue bg shadow d-table w-100 lazy" style="min-height: 400px;" data-bg-class="armoury-bg">
        <div class="venue-details d-table-cell align-middle">
          <div class="venue-details-inner px-4">
            <img src="/img/logos/the-armoury.svg" alt="Haymarket - The Armoury logo" width="400" height="91" class="mb-3 h-auto" />
            <p class="text-one mb-4">Test your aim at Northern Ireland's first state-of-the-art simulated shooting range! Where competition meets cocktails! Perfect for corporate days out, alternative nights with friends or a milestone celebration! </p>
            <a href="/the-armoury">
              <button type="button" class="btn btn-gold">THE ARMOURY</button>
            </a>
          </div>
        </div>
      </div>
    </div><!-- end col -->
  </div><!-- end row -->

  <div class="row half_row mb-3 justify-content-center">
    <div class="col-lg-6 half_col mob-mb-4">
      <div class="home-venue bg shadow d-table w-100 lazy" style="min-height: 400px;" data-bg-class="courtyard-bg">
        <div class="venue-details d-table-cell align-middle">
          <div class="venue-details-inner px-4">
            <img src="/img/logos/the-courtyard.svg" alt="Haymarket - The Courtyard logo" width="417" height="78" class="mb-3 h-auto" />
            <p class="text-one mb-4 courtyard-text-home">The Courtyard is a vibrant indoor/outdoor bar & street food hangout at Haymarket Belfast. Serving up a range of delicious cocktails, draught beer & tasty street food!</p>
            <a href="/the-courtyard">
              <button type="button" class="btn btn-primary">THE COURTYARD</button>
            </a>
          </div>
        </div>
      </div>
    </div><!-- end col -->

    <div class="col-lg-6 half_col mob-mb-4">
      <div class="home-venue bg shadow d-table w-100 lazy" style="min-height: 400px;" data-bg-class="bottomless-brunch-bg">
        <div class="venue-details d-table-cell align-middle">
          <div class="venue-details-inner px-4">
            <img src="/img/logos/bb-logo-white.svg" alt="Haymarket - Bottomless Brunch logo" width="450" height="163" class="mb-3 h-auto" />
            <p class="text-one mb-4">Enjoy 90 mins of bottomless cocktails, beer, prosecco & tasty street food from just £35 per person every Friday, Saturday & Sunday!</p>
            <a href="/select-your-brunch">
              <button type="button" class="btn btn-primary">Book Now</button>
            </a>
          </div>
        </div>
      </div>
    </div><!-- end col -->  
  </div><!-- end row -->

  <div class="row half_row mb-3 justify-content-center">
    <div class="col-lg-6 half_col mob-mb-4">
      <div class="home-venue bg shadow d-table w-100 lazy" style="min-height: 400px;" data-bg-class="private-room-bg">
        <div class="venue-details d-table-cell align-middle">
          <div class="venue-details-inner px-4">
            <h2>Private <br/>Room Hire</h2>
            <p class="text-one mb-4">Whether you're planning a party with friends, a milestone celebration with family or a get together with colleagues, we've got you sorted! Check out our private hire rooms, with dedicated bar and food & drinks packages for groups of all sizes.</p>
            <a href="/parties">
              <button type="button" class="btn btn-primary">Enquire Now</button>
            </a>
          </div>
        </div>
      </div>
    </div><!-- end col -->

    <div class="col-lg-6 half_col mob-mb-4">
      <div class="home-venue bg shadow d-table w-100 lazy" style="min-height: 400px;" data-bg-class="ffs-box-bg">
        <div class="venue-details d-table-cell align-middle position-relative  z-2">
          <div class="venue-details-inner px-4">
            <a href="https://haymarketbelfast.com/offers/14/family-friendly-sessions">
              <button type="button" class="btn btn-primary" style="position: absolute; bottom: calc(24% - 50px); left: calc(50% - 110px);">Book Now</button>
            </a>
          </div>
        </div>
      </div>
    </div><!-- end col -->
  </div><!-- end row -->
</div><!-- end container -->

<div class="contianer-fluid py-5 mt-5 overflow-hidden text-center">
  <div class="pre-title-lines mb-4 mx-auto"></div>
  <h2 class="mb-4">Offers & events</h2>
  <offers-slider></offers-slider>
</div><!-- end container -->

<div class="container pt-5 mob-pt-0 ipadp-pt-0 position-relative z-2">
  <div class="row px-0 d-lg-none mt-4 mob-mt-0">
    <div class="col-12 px-0">
      <picture>
        <source v-lazy:srcset="'/img/home/walk-in.webp'" type="image/webp"/> 
        <source v-lazy:srcset="'/img/home/walk-in.jpg'" type="image/jpeg"/> 
        <img v-lazy="'/img/home/walk-in.jpg'" width="507" height="507" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy w-100 h-auto mt-5"/>
      </picture>
    </div>
  </div>
  <!--<div class="row text-center text-lg-left">
    <div class="col-lg-5 py-5 mob-pb-0 ipadp-pb-0">
      <div class="haybale-bg py-5">
        <div class="pre-title-lines mb-4 mob-mx-auto"></div>
        <h2>Craft,<br class="d-none d-lg-block" /> Cocktails & Much More</h2>
      </div>
    </div>
    <div class="col-lg-7 py-5 mob-py-0 mob-px-4 ipadp-pt-0">
      <p class="text-larger mt-4 mb-0">Our indoor bars are spread over three floors, offering the perfect spot for a cosy date night or spontaneous cocktail or two! The outdoor area is the go to location in the City Centre for an alfresco drink. Banging food, banging drinks & a banging atmosphere. There's nothing more you need.</p>
      <button type="button" class="btn btn-primary booknowbtn mt-4">Book now</button>
    </div>
  </div>-->
</div><!-- end container -->

<div class="full-bg overlay w-100 pb-3 mb-5 py-sm-5 my-sm-5" style="background-image: url('/img/parties/room2.webp');">
  <div class="container">
    <div class="pre-title-lines mb-4 mt-5"></div>
    <h2>Looking for a hotel?</h2>
    
    <div class="row g-5">
      <div class="col-lg-5 pb-5 ipadp-pb-0 mob-pb-0">
        <div class="py-5">
          <p>Choose room2! Whether you're seeking a room for a night or a long-term stay away from home, there's a room perfectly suited for everyone. We love room2 because they are fully committed to creating environments that have a positive impact on the health and wellbeing of all our visitors, employees and our planet, not to mention the interior design! Celebrating a birthday, stag, hen or milestone head over to room2 and get your boujee room booked now!</p>
          <p><strong>USE CODE 'HAYMARKET' AT CHECK OUT FOR 10% OFF YOUR STAY</strong></p>
          <a class="d-inline-block mt-5" href="https://room2.com/belfast/" target="_blank">
            <button type="button" class="btn btn-primary">Find out more</button>
          </a>
        </div>
      </div><!-- end col -->

      <div class="col-lg-7">
        <image-slider class="pt-0 pb-5 py-lg-5"
          :images="[
            '/img/parties/room2-1-2',
            '/img/parties/room2-2-2',
            '/img/parties/room2-3-2',
          ]"
          ></image-slider>
      </div><!-- end col -->
    </div><!-- end row -->
  </div><!-- end container -->
</div><!-- end full-bg -->

<div class="container-fluid position-relative py-5 z-1">
  <picture>
    <source data-srcset="/img/graphics/chips-burger.webp" type="image/webp"/> 
    <source data-srcset="/img/graphics/chips-burger.png" type="image/png"/> 
    <img data-src="/img/graphics/chips-burger.png" type="image/png" width="550" height="1156" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy chips-burger-top-right h-auto"/>
  </picture>
  <div class="container py-5 text-center bg-dark position-relative z-2 shadow">
    <div class="row">
      <div class="col-lg-4">
        <img data-src="/img/logos/tripadvisor.svg" type="image/svg" width="290" height="1156" alt="Trip Advisor - Haymarket Belfast" class="lazy h-auto"/>
      </div>
      <div class="col-lg-5 pt-2">
        <p class="text-large">Check out our reviews on Tripadvisor!</p>
      </div>
      <div class="col-lg-3">
        <a href="https://www.tripadvisor.co.uk/Restaurant_Review-g186470-d23517646-Reviews-Haymarket-Belfast_Northern_Ireland.html?m=19905" target="_blank" aria-label="Tripadvisor" rel="noreferrer">
          <button type="button" class="btn btn-primary">Read Reviews</button>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="container pt-5 position-relative z-2 ">
  <div class="row text-center text-lg-left">
    <div class="col-12 mb-5 3pb-4">
      <div class="pre-title-lines mb-4 mob-mx-auto mob-mb-3"><span class="d-none d-lg-inline"><a href="https://instagram.com/haymarketbelfast" class="text-lowercase" target="_blank" rel="noreferrer" aria-label="Instagram">@haymarketbelfast <i class="fa fa-instagram ml-2"></i></a></span></div>
      <p class="d-lg-none text-large"><b><a href="https://instagram.com/haymarketbelfast" target="_blank" rel="noreferrer" aria-label="Instagram">@haymarketbelfast <i class="fa fa-instagram ml-2"></i></a></b></p>
      <p class="mimic-h3 mb-4">FOLLOW US ON INSTAGRAM</p>
      <instagram-feed></instagram-feed>
    </div>
  </div>
</div>
<div class="container position-relative z-2 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 py-5">
      <mailing-list :id="'ml-1-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')

@endsection
@section('scripts')
<script async src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&uniq=209&locationId=23517646&lang=en_US&year=2024&display_version=2" data-loadtrk onload="this.loadtrk=true"></script>
<script>
window.addEventListener('load', function() {
  document.querySelectorAll('.booknowbtn').forEach(function(button) {
    button.addEventListener('click', function() {
      var targetElement = document.getElementById("bookonline");
      var offset = 100;
      var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
      window.scrollTo({
        top: targetPosition - offset,
        behavior: 'smooth'
      });
    });
  });
  
});
</script>
<script>
document.addEventListener("DOMContentLoaded", () => {
    const elements = document.querySelectorAll(".lazy");
    const observer = new IntersectionObserver(
        (entries, observer) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    const bgClass = entry.target.getAttribute("data-bg-class");
                    if (bgClass) {
                        entry.target.classList.add(bgClass);
                        observer.unobserve(entry.target);
                    }
                }
            });
        },
        { rootMargin: "0px" } // Adjust to trigger earlier if needed
    );

    elements.forEach((el) => observer.observe(el));
});
</script>
@endsection