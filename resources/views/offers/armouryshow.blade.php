@php
$page = $offer->title;
$pagetitle = $offer->title . " - Haymarket Belfast";
$metadescription = "Check out the " . $offer->title . " offer at Haymarket Belfast!";
$pagetype = 'armoury';
$pagename = 'armoury';
$ogimage = $offer->getFirstMediaUrl('offers', 'normal');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style type="text/css">
  .offer-summary{
  	font-size: 1.2rem;
  }
</style>
@endsection
@section('header')
<picture>
    <source srcset="/img/graphics/guns.webp" type="image/webp"/> 
    <source srcset="/img/graphics/guns.png" type="image/png"/> 
    <img src="/img/graphics/guns.png" type="image/png" width="550" height="1156" alt="The Armoury - Haymarket Belfast" class="guns-top-left h-auto"/>
  </picture>
{{-- <picture>
  <source srcset="/img/graphics/burger-beer.webp" type="image/webp"/> 
  <source srcset="/img/graphics/burger-beer.png" type="image/jpeg"/> 
  <img src="/img/graphics/burger-beer.png" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy burger-beer-top-left"/>
</picture> --}}
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/the-armoury.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center py-5 mob-py-0">
        <div class="col-12 col-md-6 text-center text-lg-left mob-mb-3">
          <picture>
            <source srcset="{{$offer->getFirstMediaUrl('offers', 'normal-webp')}}" type="image/webp"/> 
            <source srcset="{{$offer->getFirstMediaUrl('offers', 'normal')}}" type="{{$offer->getFirstMedia('offers')->mime_type}}"/> 
            <img src="{{$offer->getFirstMediaUrl('offers', 'normal')}}" type="{{$offer->getFirstMedia('offers')->mime_type}}" alt="{{$offer->title}} - Haymarket Belfast" width="280" class="w-100 h-auto mt-5 mb-3 shadow"/>
          </picture>
          
        </div>
        @if($offer->booking_code)
        <div class="col-lg-5 col-md-8 mob-pl-0 mob-mb-5 mt-5 ipadp-mt-0 mob-mt-0 pr-5 mob-px-3 ipadp-px-3">
          <div id="bookonline" class="res-diary-holder d-inline-block shadow">
            <div class="res-diary-inner">
              <div class="loader loader-inner d-table">
                <div class="d-table-cell align-middle">
                  <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
              </div>
              <div class="position-relative z-2">
                <div id="rd-widget-frame" style="max-width: 600px; margin: auto;"></div>
              </div>
            </div>
          </div>
        </div>
        @else
        <div class="col-lg-5 col-md-8 pl-5 mob-pl-0 mob-mb-5 mt-5 ipadp-mt-0 mob-mt-0 pr-5 mob-px-3 ipadp-px-3 pt-4 text-center text-lg-left">
          <div id="bookonline" class="d-inline-block">
	        <h2>{{$offer->title}}</h2>
	        <div class="offer-summary mb-4">{!!$offer->summary!!}</div>
	        @if($offer->roller)
	        <a href="{{$offer->roller}}" target="_blank">
	        	<button type="button" class="btn btn-primary">Book Now</button>
	        </a>
	        @else
	        <a href="https://ecom.roller.app/haymarketbelfast/testarmoury/en/products" target="_blank">
	        	<button type="button" class="btn btn-primary">Book Now</button>
	        </a>
	        @endif
	        <p class="text-larger text-uppercase mb-1 mt-4 mb-5 din"><b class="text-pink">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-red">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($offer->title . ' -  at Haymarket Belfast')}}&amp;summary={{urlencode($offer->title . ' -  at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-red">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($offer->title . ' -  at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-red">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($offer->title . ' -  at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-red">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
	      </div>
        @endif
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
@if($offer->brunch == true)
<seating-options-armoury-brunch></seating-options-armoury-brunch>
@else
@if($offer->slug == 'armoury-date-night')
<div class="container position-relative z-1">
  <div class="row justify-content-center pt-5">
    <div class="col-lg-4 col-md-9 mb-5">
      <div class="seating-option text-center">
        <picture>
          <source srcset="/img/venues/armoury-brunch.webp" type="image/webp"/> 
          <source srcset="/img/venues/armoury-brunch.jpg" type="image/jpeg"/> 
          <img src="/img/venues/armoury-brunch.jpg" type="image/jpeg" width="350" height="350" alt="Street Food at The Armoury Haymarket Belfast Beer & Street Food" class="lazy w-100 h-auto op-45"  />
        </picture>
        <div class="card p-4 mx-auto"  :style="'background-color:'+bg+';'">
          <p class="mimic-h3 mb-2" :style="'color:'+text+';'">Date Night Menu</p>
          <div class="pre-title-lines mt-2 mb-4 mx-auto d-inline-block" :style="'border-top: 2px solid '+lines+'; border-bottom: 2px solid '+ lines +';'"></div>
        </div>
        <a href="/docs/armoury-date-night-menu.pdf" target="_blank">
          <button type="button" class="btn btn-primary mx-auto z-2" >Download</button>
        </a>
      </div>
    </div>
  </div>
</div>
@else
<seating-options-armoury></seating-options-armoury>
@endif
@endif
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :id="'ml-2-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
@if($offer->booking_code != null)
{!!$offer->booking_code!!}
@else
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/26681?includeJquery=true" type="hidden">

@if($offer->booking_code_script != null)
{!!$offer->booking_code_script!!}
@else
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
@endif

@endif

<script>
window.addEventListener('load', function() {
  document.querySelectorAll('.booknowbtn').forEach(function(button) {
    button.addEventListener('click', function() {
      var targetElement = document.getElementById("bookonline");
      var offset = 100;
      var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
      window.scrollTo({
        top: targetPosition - offset,
        behavior: 'smooth'
      });
    });
  });
});
</script>
@endsection