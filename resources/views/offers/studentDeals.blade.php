@php
$page = 'Homepage';
$pagetitle = "Student Deals | Haymarket - Belfast's best indoor/outdoor bar & street food hangout";
$metadescription = "Check out our Student Deals featuring Two For One Cocktails, Bottomless Brunch & Quiz nights at Haymarket Belfast";
$pagetype = 'offers';
$pagename = 'offers';
$ogimage = 'https://haymarketbelfast.com/img/events/bottomless-brunch/takeover/takeover.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=din:wght@500&display=swap" rel="stylesheet">
<style type="text/css">
  body{
    background-color: #46c8c0 !important;
  }
  #scroll-menu{
    background-color: #46c8ae !important;
  }
  .btn-primary{
    color: #000;
    background-color: #f9e169 !important;
  }
  .mailing-list-signup a,
  .text-primary,
  .opening-hours .today p{
    color: #f9e169 !important;
  }
  .menu .menu-links .menu-item,
  footer a{
    color: #fff !important;
  }
  .price{
    position:absolute;
    top:0.5rem;
    right:1.5rem;
    padding:18px 0 0;
    font-size:26px;
    color:#fff;
    background-image:url('/img/graphics/price.svg');
    background-size: contain;
    background-repeat: no-repeat;
    width: 80px;
    height:80px;
    font-family: "din", sans-serif;
    font-weight:900;
    text-align:center;
    transform: rotate(13deg);
  }
</style>
@endsection
@section('header')
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center pt-5 mob-py-0 mob-mb-5">
        <div class="col-lg-9 text-center">
          <div class="text-center mt-5 mob-mt-0 d-lg-none">
            <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
          </div>
          <picture>
            <source srcset="/img/students/student-deals.webp" type="image/webp"/> 
            <source srcset="/img/students/student-deals.png" type="image/png"/> 
            <img src="/img/students/student-deals.png" width="500" class="mt-5 pt-5 mob-mt-0" alt="Student Deals logo" style="max-width: 80vw;" />
          </picture>
          {{-- <p class="din text-uppercase" style="font-size:90px;color: #f9e169; line-height: 1;"><b>Bottomless Brunch Takeovers</b></p> --}}
          <p class="text-larger din mt-5 mb-0"><b>Student Brunch Friday & Sunday - £10 off with valid student ID</b></p>
          <p class="text-larger din"><b>2 for 1 Cocktails WED-FRI & Quiz night every Wednesday</b></p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container container-wide">
  <div class="row justify-content-center">
    <div class="col-lg-4 mb-5 text-center">
      <picture>
        <source srcset="/img/students/fresher-brunch.webp?v=2024-03-11" type="image/webp"/> 
        <source srcset="/img/students/fresher-brunch.jpg?v=2024-03-11" type="image/jpg"/> 
        <img src="/img/students/fresher-brunch.jpg?v=2024-03-11" width="507" height="507" type="image/jpg" alt="Student & Freshers Brunch - Haymarket Belfast" class="w-100 h-auto"/>
      </picture>
      <div class="price">£40</div>
      <a href="/bottomless-brunch/takeovers/fresher-brunch-img">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
    </div>
    <div class="col-lg-4 mb-5 text-center">
      <picture>
        <source srcset="/img/students/quiz-night.webp" type="image/webp"/> 
        <source srcset="/img/students/quiz-night.jpg" type="image/jpg"/> 
        <img src="/img/students/quiz-night.jpg" width="507" height="507" type="image/jpg" alt="Student & Freshers Quiz Night - Haymarket Belfast" class="w-100 h-auto"/>
      </picture>
      <div class="price">£40</div>
      <a href="/bottomless-brunch/takeovers/lady-gaga">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
    </div>
    <div class="col-lg-4 mb-5 text-center">
      <picture>
        <source srcset="/img/students/2for1-cocktails.webp" type="image/webp"/> 
        <source srcset="/img/students/2for1-cocktails.jpg" type="image/jpg"/> 
        <img src="/img/students/2for1-cocktails.jpg" width="507" height="507" type="image/jpg" alt="2 for 1 cocktails - Haymarket Belfast" class="w-100 h-auto"/>
      </picture>
      <div class="price">£45</div>
      <a href="/bottomless-brunch/takeovers/the-iconettes">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
    </div>
    <div class="col-12 mt-5 text-center">
      <p class="text-large mb-4">Please see T&C's of all Brunch Bookings</p>
      <a href="/faqs">
        <button class="btn btn-primary" type="button">FAQs</button>
      </a>
    </div>
  </div>
</div>
<seating-options-brunch :bg="'#46c8ae'" :food="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2023-08-04'" :cocktails="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2023-08-04'"></seating-options-brunch>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :bg="'#46c8ae'" :id="'ml-2-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
@endsection