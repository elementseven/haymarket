@php
$page = 'Homepage';
$pagetitle = "Pride | Haymarket - Belfast's best indoor/outdoor bar & street food hangout";
$metadescription = "Check out our Pride Events at Haymarket Belfast";
$pagetype = 'offers';
$pagename = 'offers';
$ogimage = 'https://haymarketbelfast.com/img/pride/general.jpg?v=2023-07-14';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style type="text/css">
  body{
    background-color: #000 !important;
    background-image: url('/img/pride/bg.jpg') !important;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: top center;
  }
  #scroll-menu{
    background-color: #ea7ad9 !important;
  }
  .btn-primary{
    background-color: #bc33ca !important;
  }
  .mailing-list-signup a,
  .text-primary,
  .opening-hours .today p{
    color: #bc33ca !important;
  }
  .menu .menu-links .menu-item,
  footer a{
    color: #fff !important;
  }
  .wrapper { 
    opacity: 0.45;
    height: 100%;
    width: 100%;
    left:0;
    right: 0;
    top: 0;
    bottom: 0;
    z-index: -1;
    position: fixed;
    background: linear-gradient(124deg, #ff2400, #e81d1d, #aaad24, #1bc438, #2115cb, #dd00f3);
    background-size: 1500% 1500%;
    -webkit-animation: rainbow 25s ease infinite;
    -z-animation: rainbow 25s ease infinite;
    -o-animation: rainbow 25s ease infinite;
    animation: rainbow 25s ease infinite;
  }
  @-webkit-keyframes rainbow {
    0%{background-position:0% 82%}
    50%{background-position:100% 19%}
    100%{background-position:0% 82%}
  }
  @-moz-keyframes rainbow {
    0%{background-position:0% 82%}
    50%{background-position:100% 19%}
    100%{background-position:0% 82%}
  }
  @-o-keyframes rainbow {
    0%{background-position:0% 82%}
    50%{background-position:100% 19%}
    100%{background-position:0% 82%}
  }
  @keyframes rainbow { 
    0%{background-position:0% 82%}
    50%{background-position:100% 19%}
    100%{background-position:0% 82%}
  }
</style>
@endsection
@section('header')
<div class="wrapper"></div>
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
@endsection
@section('content')
<div class="container mt-5 pt-5">
  <div class="row justify-content-center">
    <div class="col-12">
      <h1 class="text-center mb-5">Celebrate Pride With Us!</h1>
    </div>
    <div class="col-lg-5 mb-5 text-center">
      <picture>
        <source srcset="/img/pride/general.webp?v=2023-07-14" type="image/webp"/> 
        <source srcset="/img/pride/general.jpg?v=2023-07-14" type="image/jpg"/> 
        <img src="/img/pride/general.jpg?v=2023-07-14" width="507" height="507" type="image/jpg" alt="Pride - Haymarket Belfast" class="w-100 h-auto shadow"/>
      </picture>
      <a href="/#bookonline">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
    </div>
    <div class="col-lg-5 mb-5 text-center">
      <picture>
        <source srcset="/img/pride/brunch.webp?v=2023-06-01" type="image/webp"/> 
        <source srcset="/img/pride/brunch.jpg?v=2023-06-01" type="image/jpg"/> 
        <img src="/img/pride/brunch.jpg?v=2023-06-01" width="507" height="507" type="image/jpg" alt="Pride - Bottomless Brunch - Haymarket Belfast" class="w-100 h-auto shadow"/>
      </picture>
      <a href="/bottomless-brunch">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-lg-5 mb-5 text-center">
      <picture>
        <source srcset="/img/pride/summer-sips.webp?v=2023-07-03" type="image/webp"/> 
        <source srcset="/img/pride/summer-sips.jpg?v=2023-07-03" type="image/jpg"/> 
        <img src="/img/pride/summer-sips.jpg?v=2023-07-03" width="507" height="507" type="image/jpg" alt="Pride - Summer Sips - Haymarket Belfast" class="w-100 h-auto shadow"/>
      </picture>
      <a href="/offers/summer-sips">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
    </div>
 
    <div class="col-lg-5 mb-5 text-center">
      <picture>
        <source srcset="/img/pride/241-cocktails.webp?v=2023-06-01" type="image/webp"/> 
        <source srcset="/img/pride/241-cocktails.jpg?v=2023-06-01" type="image/jpg"/> 
        <img src="/img/pride/241-cocktails.jpg?v=2023-06-01" width="507" height="507" type="image/jpg" alt="Pride - 241 cocktails - Haymarket Belfast" class="w-100 h-auto shadow"/>
      </picture>
      <a href="/#bookonline">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
    </div>
  </div>
</div>
<seating-options-brunch :bg="'#ea7ad9'"></seating-options-brunch>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :bg="'#ea7ad9'" :id="'ml-2-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
@endsection