@php
$page = 'Homepage';
$pagetitle = "The Iconettes - Bottomless Brunch Takeovers | Haymarket - Belfast's best indoor/outdoor bar & street food hangout";
$metadescription = "Check out our Bottomless Brunch Takeovers featuring tribute acts of The Iconettes, Madonna and ABBA at Haymarket Belfast";
$pagetype = 'offers';
$pagename = 'offers';
$ogimage = 'https://haymarketbelfast.com/img/events/bottomless-brunch/takeover/the-iconettes.jpg?v=2023-08-16';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style type="text/css">
  body{
    background-color: #0d0b12 !important;
  }
  #scroll-menu{
    background-color: #0c0c0c !important;
  }
  .btn-primary{
    background-color: #f3ba38 !important;
  }
  .mailing-list-signup a,
  .text-primary,
  .opening-hours .today p{
    color: #f3ba38 !important;
  }
  .menu .menu-links .menu-item,
  footer a{
    color: #fff !important;
  }
</style>
@endsection
@section('header')
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center py-5 mob-py-0">
        <div class="col-12 col-md-8 text-center text-lg-left mob-mb-3 d-lg-none">
          <picture>
            <source srcset="/img/events/bottomless-brunch/takeover/the-iconettes.webp?v=2023-08-16" type="image/webp"/> 
            <source srcset="/img/events/bottomless-brunch/takeover/the-iconettes.jpg?v=2023-08-16" type="image/jpg"/> 
            <img src="/img/events/bottomless-brunch/takeover/the-iconettes.jpg?v=2023-08-16" width="507" height="507" type="image/jpg" alt="Bottomless Brunch Takeover - The Iconettes - Haymarket Belfast" class="w-100 h-auto"/>
          </picture>
          <p class="text-small mt-3"><a href="/terms-and-conditions" class="text-white"><b><u>Please see our terms & conditions to our brunch</u></b></a></p>
          <button type="button" class="btn btn-primary booknowbtn mt-3 mb-3">Book Now</button>
          <p class="text-larger text-uppercase mb-1 mb-5 din"><b class="text-pink">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-red">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode('The Iconettes Bottomless Brunch Takeover at Haymarket Belfast')}}&amp;summary={{urlencode('The Iconettes Bottomless Brunch Takeover at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-red">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode('The Iconettes Bottomless Brunch Takeover at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-red">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode('The Iconettes Bottomless Brunch Takeover at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-red">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
        <div class="col-lg-5 col-md-8 mob-pl-0 mob-mb-5 mt-5 ipadp-mt-0 mob-mt-0 pr-5 mob-px-3 ipadp-px-3">
          <div id="bookonline" class="res-diary-holder d-inline-block shadow">
            <div class="res-diary-inner">
              <div class="loader loader-inner d-table">
                <div class="d-table-cell align-middle">
                  <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
              </div>
              <div class="position-relative z-2">
                <div id="rd-widget-frame" style="max-width: 600px; margin: auto;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 pt-5 mob-mt-5 text-center mob-px-0 mob-mb-3 d-none d-lg-block">
          <picture>
            <source srcset="/img/events/bottomless-brunch/takeover/the-iconettes.webp?v=2023-08-16" type="image/webp"/> 
            <source srcset="/img/events/bottomless-brunch/takeover/the-iconettes.jpg?v=2023-08-16" type="image/jpg"/> 
            <img src="/img/events/bottomless-brunch/takeover/the-iconettes.jpg?v=2023-08-16" width="507" height="507" type="image/jpg" alt="Bottomless Brunch Takeover - The Iconettes - Haymarket Belfast" class="w-100 h-auto"/>
          </picture>
          <p class="text-small mt-3"><a href="/terms-and-conditions" class="text-white"><b><u>Please see our terms & conditions to our brunch</u></b></a></p>
          <p class="text-larger text-uppercase mb-1 mt-4 din"><b class="text-primary">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-red">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode('The Iconettes Bottomless Brunch Takeover at Haymarket Belfast')}}&amp;summary={{urlencode('The Iconettes Bottomless Brunch Takeover at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-red">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode('The Iconettes Bottomless Brunch Takeover at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-red">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode('The Iconettes Bottomless Brunch Takeover at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-red">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<seating-options-brunch :bg="'#0c0c0c'" :food="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2023-08-04'" :cocktails="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2023-08-04'"></seating-options-brunch>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :bg="'#0c0c0c'" :id="'ml-2-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/38264?includeJquery=true&date=2023-11-26&time=13:30" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
</script>
@endsection