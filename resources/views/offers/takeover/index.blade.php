@php
$page = 'Homepage';
$pagetitle = "Bottomless Brunch Takeovers | Haymarket - Belfast's best indoor/outdoor bar & street food hangout";
$metadescription = "Check out our Bottomless Brunch Takeovers featuring tribute acts of Garth Brooks, dolly-parton and ABBA at Haymarket Belfast";
$pagetype = 'offers';
$pagename = 'offers';
$ogimage = 'https://haymarketbelfast.com/img/events/bottomless-brunch/takeover/takeover.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=din:wght@500&display=swap" rel="stylesheet">
<style type="text/css">
  body{
    background-color: #0d0b12 !important;
  }
  #scroll-menu{
    background-color: #0c0c0c !important;
  }
  .btn-primary{
    background-color: #f3ba38 !important;
  }
  .mailing-list-signup a,
  .text-primary,
  .opening-hours .today p{
    color: #f3ba38 !important;
  }
  .menu .menu-links .menu-item,
  footer a{
    color: #fff !important;
  }
  .price{
    position:absolute;
    top:0.5rem;
    right:1.5rem;
    padding:18px 0 0;
    font-size:26px;
    color:#fff;
    background-image:url('/img/graphics/price.svg');
    background-size: contain;
    background-repeat: no-repeat;
    width: 80px;
    height:80px;
    font-family: "din", sans-serif;
    font-weight:900;
    text-align:center;
    transform: rotate(13deg);
  }
</style>
@endsection
@section('header')
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center pt-5 mob-py-0 mob-mb-5">
        <div class="col-lg-9 text-center">
          <div class="text-center mt-5 mob-mt-0 d-lg-none">
            <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
          </div>
          <picture>
            <source srcset="/img/events/bottomless-brunch/takeover/top.webp" type="image/webp"/> 
            <source srcset="/img/events/bottomless-brunch/takeover/top.png" type="image/png"/> 
            <img src="/img/events/bottomless-brunch/takeover/top.png" width="600" class="mt-5 pt-5 mob-mt-0" alt="Bottomless Brunch Takeovers logo" style="max-width: 80vw;" />
          </picture>
          {{-- <p class="din text-uppercase" style="font-size:90px;color: #f3ba38; line-height: 1;"><b>Bottomless Brunch Takeovers</b></p> --}}
          <p class="text-large mt-5"><b>Think of our boozy brunches and times it by 100 with our themed bottomless brunches!!! The perfect girl's night, birthday celebration or just excuse to sing along to your fave tributes!</b></p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container container-wide">
  <div class="row justify-content-center">
    <div class="col-lg-4 mb-5 text-center">
      <picture>
        <source srcset="/img/events/bottomless-brunch/takeover/abba-cal.webp?v=2024-03-26" type="image/webp"/> 
        <source srcset="/img/events/bottomless-brunch/takeover/abba-cal.jpg?v=2024-03-26" type="image/jpg"/> 
        <img src="/img/events/bottomless-brunch/takeover/abba-cal.jpg?v=2024-03-26" width="507" height="507" type="image/jpg" alt="Bottomless Brunch Takeover - ABBA - Haymarket Belfast" class="w-100 h-auto"/>
      </picture>
      {{-- <div class="price">£45</div> --}}
      <a href="/bottomless-brunch/takeovers/abba">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
    </div>
    <div class="col-lg-4 mb-5 text-center">
      <picture>
        <source srcset="/img/events/bottomless-brunch/takeover/shania.webp?v=2024-03-26" type="image/webp"/> 
        <source srcset="/img/events/bottomless-brunch/takeover/shania.jpg?v=2024-03-26" type="image/jpg"/> 
        <img src="/img/events/bottomless-brunch/takeover/shania.jpg?v=2024-03-26" width="507" height="507" type="image/jpg" alt="Bottomless Brunch Takeover - shinia twain general admission - Haymarket Belfast" class="w-100 h-auto"/>
      </picture>
      <a href="/bottomless-brunch/takeovers/shania-twain">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
    </div>
    <div class="col-lg-4 mb-5 text-center">
      <picture>
        <source srcset="/img/events/bottomless-brunch/takeover/taylor.webp?v=2024-03-28" type="image/webp"/> 
        <source srcset="/img/events/bottomless-brunch/takeover/taylor.jpg?v=2024-03-28" type="image/jpg"/> 
        <img src="/img/events/bottomless-brunch/takeover/taylor.jpg?v=2024-03-28" width="507" height="507" type="image/jpg" alt="Bottomless Brunch Takeover - taylor made general admission - Haymarket Belfast" class="w-100 h-auto"/>
      </picture>
      <a href="/bottomless-brunch/takeovers/shania-twain">
        <button class="btn btn-primary shadow" style="margin-top: -20px;" type="button">Book now</button>
      </a>
    </div>
    <div class="col-12 mt-5 text-center">
      <p class="text-large mb-4">Please see T&C's of all Brunch Bookings</p>
      <a href="/terms-and-conditions">
        <button class="btn btn-primary" type="button">FAQs</button>
      </a>
    </div>
  </div>
</div>
<seating-options-brunch :bg="'#0c0c0c'" :food="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2023-08-04'" :cocktails="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2023-08-04'"></seating-options-brunch>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :bg="'#0c0c0c'" :id="'ml-2-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')

@endsection