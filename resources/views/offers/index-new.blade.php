@php
$page = 'Homepage';
$pagetitle = "Offers & Discounts | Haymarket - Belfast's newest indoor/outdoor bar & street food hangout";
$metadescription = "Check out all the latest offers and discounts, drinks offers, food offers and more at Haymarket Belfast";
$pagetype = 'offers';
$pagename = 'offers';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
@endsection
@section('header')
<picture>
  <source srcset="/img/graphics/burger-beer.webp" type="image/webp"/> 
  <source srcset="/img/graphics/burger-beer.png" type="image/jpeg"/> 
  <img src="/img/graphics/burger-beer.png" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy burger-beer-top-left"/>
</picture>
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 ">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center pt-5 mob-py-0">
        <div class="col-12 text-center">
          <div class="pre-title-lines mx-auto my-4 mob-my-45"></div>
          <h1>Offers & Discounts</h1>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<offers-index></offers-index>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :id="'ml-2-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
@endsection