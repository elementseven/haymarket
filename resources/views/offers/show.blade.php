@php
$page = $offer->title;
$pagetitle = $offer->title . " - Haymarket Belfast";
$metadescription = "Check out the " . $offer->title . " offer at Haymarket Belfast!";
$pagetype = 'offers';
$pagename = 'offers';
$ogimage = $offer->getFirstMediaUrl('offers', 'normal');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style type="text/css">
  #karaoke-dates .col-lg{
    border-left: 1px solid #fff;
  }
  #karaoke-dates .col-lg:first-of-type{
    border-left: 0;
  }
  @media only screen and (max-width : 767px){
    #karaoke-dates .col-lg{
       margin-bottom: 1.5rem;
    }
    #karaoke-dates .col-lg:nth-child(odd){
      border-left: 0;
    }
  }
</style>
@endsection
@section('header')
<picture>
  <source srcset="/img/graphics/burger-beer.webp" type="image/webp"/> 
  <source srcset="/img/graphics/burger-beer.png" type="image/jpeg"/> 
  <img src="/img/graphics/burger-beer.png" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy burger-beer-top-left"/>
</picture>
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center py-5 mob-py-0">
        <div class="col-12 col-md-8 text-center text-lg-left mob-mb-3 d-lg-none">
          <picture>
            <source srcset="{{$offer->getFirstMediaUrl('offers', 'normal-webp')}}" type="image/webp"/> 
            <source srcset="{{$offer->getFirstMediaUrl('offers', 'normal')}}" type="{{$offer->getFirstMedia('offers')->mime_type}}"/> 
            <img src="{{$offer->getFirstMediaUrl('offers', 'normal')}}" type="{{$offer->getFirstMedia('offers')->mime_type}}" alt="{{$offer->title}} - Haymarket Belfast" width="280" class="w-100 h-auto mt-5 mb-3"/>
          </picture>
          <button type="button" class="btn btn-primary booknowbtn mt-3 mb-3">Book Now</button>
          <p class="text-larger text-uppercase mb-1 mb-5 din"><b class="text-pink">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-red">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($offer->title . ' -  at Haymarket Belfast')}}&amp;summary={{urlencode($offer->title . ' -  at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-red">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($offer->title . ' -  at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-red">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($offer->title . ' -  at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-red">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
        <div class="col-lg-5 col-md-8 mob-pl-0 mob-mb-5 mt-5 ipadp-mt-0 mob-mt-0 pr-5 mob-px-3 ipadp-px-3">
          <div id="bookonline" class="res-diary-holder d-inline-block shadow">
            <div class="res-diary-inner">
              <div class="loader loader-inner d-table">
                <div class="d-table-cell align-middle">
                  <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
              </div>
              <div class="position-relative z-2">
                <div id="rd-widget-frame" style="max-width: 600px; margin: auto;"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 pt-5 mob-mt-5 text-center mob-px-0 mob-mb-3 d-none d-lg-block">
          <picture>
            <source srcset="{{$offer->getFirstMediaUrl('offers', 'normal-webp')}}" type="image/webp"/> 
            <source srcset="{{$offer->getFirstMediaUrl('offers', 'normal')}}" type="{{$offer->getFirstMedia('offers')->mime_type}}"/> 
            <img src="{{$offer->getFirstMediaUrl('offers', 'normal')}}" type="{{$offer->getFirstMedia('offers')->mime_type}}" alt="{{$offer->title}} - Haymarket Belfast" width="280" class="mb-3 w-100 h-auto"/>
          </picture>
          <p class="text-larger text-uppercase mb-1 din"><b class="text-primary">Share :</b>
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-red">
              <i class="fa fa-facebook ml-2"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($offer->title . ' -  at Haymarket Belfast')}}&amp;summary={{urlencode($offer->title . ' -  at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-red">
              <i class="fa fa-linkedin ml-3"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($offer->title . ' -  at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-red">
              <i class="fa fa-twitter ml-3"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($offer->title . ' -  at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-red">
              <i class="fa fa-whatsapp ml-3"></i>
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
{{-- Karaoke dates --}}
@if($offer->id == 10)
<div id="karaoke-dates" class="container text-center">
  <div class="row">
    <div class="col-12">
      <div class="card p-5">
        <div class="row">
          <div class="col-12 mb-4">
            <div class="pre-title-lines mx-auto mb-4"></div>
            <p class="mimic-h3">Karaoke Dates</p>
            <p>We will be running our Karaoke Bottomless Brunch on the following dates:</p>
          </div>
        </div>
        <div class="row">
{{--           <div class="col-lg col-6">
            <p class="title text-large mb-2">April</p>
            <p class="mb-0">7th, 14th, 28th</p>
          </div>
          <div class="col-lg col-6">
            <p class="title text-large mb-2">May</p>
            <p class="mb-0">12th</p>
          </div> --}}
          <div class="col-lg col-6">
            <p class="title text-large mb-2">June</p>
            <p class="mb-0">2nd, 9th, 30th</p>
          </div>
          <div class="col-lg col-6">
            <p class="title text-large mb-2">July</p>
            <p class="mb-0">7th, 28th</p>
          </div>
          <div class="col-lg col-6">
            <p class="title text-large mb-2">August</p>
            <p class="mb-0">4th</p>
          </div>
          <div class="col-12 mt-5">
            <button type="button" class="btn btn-primary booknowbtn">Book Now</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@if($offer->brunch == true)
<seating-options-brunch :food="'https://haymarketbelfast.com/menus/brunch-menu-aug-2023.pdf?2023-08-04'" :cocktails="'https://haymarketbelfast.com/menus/haymarket-menu.pdf'"></seating-options-brunch>
@elseif($offer->id == 22)
<seating-options :food="'https://haymarketbelfast.com/docs/supper-club.pdf?v=2024-09-24'" :cocktails="'https://haymarketbelfast.com/docs/supper-club.pdf?v=2024-09-24'" :foodtitle="'Supper Club menu'" :cocktailtitle="'Supper Club menu'" :bg="'#1b1b1d'" :text="'#fff'" :lines="'#fff'"></seating-options>
@else
<seating-options :bg="'#1b1b1d'" :text="'#fff'" :lines="'#fff'"></seating-options>
@endif
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :id="'ml-2-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
@if($offer->booking_code != null)
{!!$offer->booking_code!!}
@else
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/26681?includeJquery=true" type="hidden">
@endif
@if($offer->booking_code_script != null)
{!!$offer->booking_code_script!!}
@else
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
@endif
<script>
window.addEventListener('load', function() {
  document.querySelectorAll('.booknowbtn').forEach(function(button) {
    button.addEventListener('click', function() {
      var targetElement = document.getElementById("bookonline");
      var offset = 100;
      var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
      window.scrollTo({
        top: targetPosition - offset,
        behavior: 'smooth'
      });
    });
  });
});
</script>
@endsection