@php
$page = 'Homepage';
$pagetitle = "Offers & Discounts | Haymarket - Belfast's newest indoor/outdoor bar & street food hangout";
$metadescription = "Check out all the latest offers and discounts, drinks offers, food offers and more at Haymarket Belfast";
$pagetype = 'offers';
$pagename = 'offers';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
@endsection
@section('header')
<picture>
  <source srcset="/img/graphics/burger-beer.webp" type="image/webp"/> 
  <source srcset="/img/graphics/burger-beer.png" type="image/jpeg"/> 
  <img src="/img/graphics/burger-beer.png" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy burger-beer-top-left"/>
</picture>
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center pt-5 mob-py-0 mob-mb-5">
        <div class="col-12 text-center">
          <div class="pre-title-lines mx-auto my-4 mob-my-45"></div>
          <h1>Offers & Discounts</h1>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container">
  <div class="row mb-5">
    <div class="col-lg-5">
      <picture>
        <source srcset="/img/offers/six-nations/six-nations.webp?v=2024-01-11" type="image/webp"/> 
        <source srcset="/img/offers/six-nations/six-nations.jpg?v=2024-01-11" type="image/jpg"/> 
        <img src="/img/offers/six-nations/six-nations.jpg?v=2024-01-11" width="507" height="507" type="image/jpg" alt="Six Nations - Haymarket Belfast" class="w-100 h-auto px-5 mob-px-0"/>
      </picture>
    </div>
    <div class="col-lg-7 mt-4 mob-mt-4 text-center text-lg-left">
      <p class="text-primary text-uppercase mb-1"><b>Offers & Discounts</b></p>
      <p class="mimic-h2 mb-1">Six Nations</p>
      <p class="mb-4">Experience the Six Nations at Haymarket!</p>
      <a href="/six-nations">
        <button class="btn btn-primary" type="button">Book now</button>
      </a>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-lg-5 mob-px-0">
      <picture>
        <source srcset="/img/offers/easter/bb.webp?v=2024-03-11" type="image/webp"/> 
        <source srcset="/img/offers/easter/bb.jpg?v=2024-03-11" type="image/jpg"/> 
        <img src="/img/offers/easter/bb.jpg?v=2024-03-11" width="507" height="507" type="image/jpg" alt="Mothers Day - Haymarket Belfast" class="w-100 h-auto px-5 mob-px-0"/>
      </picture>
    </div>
    <div class="col-lg-7 mt-4 text-center text-lg-left">
      <p class="text-primary text-uppercase d-none d-lg-block mb-1"><b>Offers & Discounts</b></p>
      <p class="mimic-h2 d-none d-lg-block mb-1">Easter Bottomless Brunch!</p>
      <p class="mb-4 d-none d-lg-block">Sunday 31st | Monday 1st | Tuesday 2nd at 3:30pm. Join us for 90 mins of bottomless cocktails, beer, prosecco & tasty street food for just £35pp!</p>
      <a href="/easter">
        <button class="btn btn-primary shadow" type="button">Book now</button>
      </a>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-lg-5">
      <picture>
        <source srcset="/img/students/student-brunch.webp?v=2024-03-11" type="image/webp"/> 
        <source srcset="/img/students/student-brunch.jpg?v=2024-03-11" type="image/jpg"/> 
        <img src="/img/students/student-brunch.jpg?v=2024-03-11" width="507" height="507" type="image/jpg" alt="Six Nations - Haymarket Belfast" class="w-100 h-auto px-5 mob-px-0"/>
      </picture>
    </div>
    <div class="col-lg-7 mt-4 mob-mt-4 text-center text-lg-left">
      <p class="text-primary text-uppercase mb-1"><b>Offers & Discounts</b></p>
      <p class="mimic-h2 mb-1">Student Brunch</p>
      <p class="mb-4">Enjoy bottomless cocktails, beer, prosecco &amp; tasty street food for just £25pp with a valid student ID!</p>
      <a href="/student-deals/student-brunch">
        <button class="btn btn-primary" type="button">Book now</button>
      </a>
    </div>
  </div>
  <div class="row mb-5">
    <div class="col-lg-5">
      <picture>
        <source src="/img/events/bottomless-brunch/bottomless-brunch-main.webp?2024-01-30" type="image/webp"/>
        <source src="/img/events/bottomless-brunch/bottomless-brunch-main.jpg?2024-01-30" type="image/jpeg"/>
        <img src="/img/events/bottomless-brunch/bottomless-brunch-main.jpg?2024-01-30" alt="Bottomless Brunch - Haymarket Belfast" width="280" class="w-100 h-auto px-5 mob-px-0" />
      </picture>
    </div>
    <div class="col-lg-7 mt-4 mob-mt-4 text-center text-lg-left">
      <p class="mimic-h2 mb-1">Bottomless Brunch</p>
      <p class="mb-2">Enjoy bottomless cocktails &amp; tasty street food from just £35<br/><span class="text-one">Every Fri | 5pm - 6:30pm & 7pm - 8:30pm*</span> <br><span class="text-one">Every Sat | 12:30pm - 2pm, 2:30pm - 4pm & 4:30pm - 6pm*</span> <br><span class="text-one">Every Sun | 3:30pm - 5:00pm</span></p>
      <p class="text-small mb-4">*Reservations must be paid in advance for booking to be valid.</p>
      <a href="/bottomless-brunch">
        <button class="btn btn-primary" type="button">Book now</button>
      </a>
    </div>
  </div>  
  <div class="row mb-5">
    <div class="col-lg-5">
      <picture>
        <source srcset="/img/events/bottomless-brunch/takeover/abba-cal.webp?v=2024-01-10" type="image/webp"/> 
        <source srcset="/img/events/bottomless-brunch/takeover/abba-cal.jpg?v=2024-01-10" type="image/jpg"/> 
        <img src="/img/events/bottomless-brunch/takeover/abba-cal.jpg?v=2024-01-10" width="507" height="507" type="image/jpg" alt="Bottomless Brunch Takeover - ABBA - Haymarket Belfast" class="w-100 h-auto px-5 mob-px-0"/>
      </picture>
      {{-- <picture>
        <source srcset="/img/events/bottomless-brunch/takeover/takeover.webp?v=2023-08-16" type="image/webp"/> 
        <source srcset="/img/events/bottomless-brunch/takeover/takeover.jpg?v=2023-08-16" type="image/jpg"/> 
        <img src="/img/events/bottomless-brunch/takeover/takeover.jpg?v=2023-08-16" width="507" height="507" type="image/jpg" alt="Bottomless Brunch Takeovers - Haymarket Belfast" class="w-100 h-auto px-5 mob-px-0"/>
      </picture> --}}
    </div>
    <div class="col-lg-7 mt-3 mob-mt-4 text-center text-lg-left">
      <p class="text-small text-uppercase text-primary letter-spacing mb-1"><b>Offers & Discounts</b></p>
      <p class="mimic-h2 mb-1">Bottomless Brunch Takeovers</p>
      <p class="mb-4">Think of our boozy brunches and times it by 100 with our themed bottomless brunches!!! The perfect girl's night, birthday celebration or just excuse to sing along to your fave tributes!</p>
      <a href="/bottomless-brunch/takeovers">
        <button class="btn btn-primary" type="button">Book now</button>
      </a>
    </div>
  </div>
</div>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :id="'ml-2-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
@endsection