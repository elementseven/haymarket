@php
$page = 'Homepage';
$pagetitle = "Six Nations - All matches shown live at Haymarket Belfast";
$metadescription = "Watch Six Nations Nations Rugby at Haymarket Belfast - Belfast's newest indoor/outdoor bar & street food hangout.";
$pagetype = 'six-nations';
$pagename = 'six-nations';
$ogimage = 'https://haymarketbelfast.com/img/offers/six-nations/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=din:wght@500&display=swap" rel="stylesheet">
@endsection
@section('header')
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 mb-5">
  <div class="row">
    <div class="container pt-5">
      <div class="row justify-content-center py-5 mob-py-0">
        <div class="col-12 text-center mob-px-0 mob-mb-3 position-relative">
          <p class="mimic-h1 din text-uppercase mb-0 text-beage"><b>SIX NATIONS</b></p>
          <p class="mimic-h2 din text-uppercase letter-spacing text-primary mb-2"><b>ALL MATCHES SHOWN LIVE</b></p>
          <p class="mimic-h2 din text-uppercase letter-spacing mb-0"><b>STARTS FRI 31ST JANUARY</b></p>
          <picture>
            <source srcset="/img/offers/six-nations/logos.webp" type="image/webp"/> 
            <source srcset="/img/offers/six-nations/logos.png" type="image/png"/> 
            <img src="/img/offers/six-nations/logos.png" type="image/png" width="708" height="112" alt="Haymarket Six Nations Logos" class="six-nations-logos lazy px-4 mt-4 h-auto"/>
          </picture>
          <br/>
          <button type="button" class="btn btn-primary booknowbtn mt-5">Book Now</button>
        </div>
      </div>
  </div>
</header>
@endsection
@section('content')
<six-nations-calendar></six-nations-calendar>
<div class="container">
   <div class="row">
    <div class="col-12 text-center mb-5">
      <p class="text-larger mt-5 text-uppercase mb-1 din"><b class="text-primary">Share :</b>
        <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
          <i class="fa fa-facebook ml-2"></i>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode('Six Nations at Haymarket Belfast')}}&amp;summary={{urlencode('Six Nations at Haymarket Belfast')}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
          <i class="fa fa-linkedin ml-3"></i>
        </a>
        <a href="https://twitter.com/intent/tweet/?text={{urlencode('Six Nations at Haymarket Belfast')}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
          <i class="fa fa-twitter ml-3"></i>
        </a>
        <a href="whatsapp://send?text={{urlencode('Six Nations at Haymarket Belfast')}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
          <i class="fa fa-whatsapp ml-3"></i>
        </a>
      </p>
    </div>
  </div>
</div>
<div class="container-fluid text-center py-5 mb-5" style="background-color:#ea3525;">
  <div class="row">
    <div class="container">
      <p class="mimic-h3 din text-white"><b>MAKE IT PERSONAL. BOOK A PRIVATE BOOTH & SCREEN FOR YOU AND YOUR MATES</b></p>
      <a href="https://ecom.roller.app/haymarketbelfast/eurofinal/en/products" target="_blank"><button type="button" class="btn btn-black m-auto mt-5">Book Now</button></a>
    </div>
  </div>
</div>
<div class="container">
  <div class="row mb-5">
    <div class="col-lg-5  mob-mt-4 col-md-8 offset-md-2 offset-lg-0 mob-pl-0 mob-mb-5 mt-5 ipadp-mt-0 mob-mt-0 pr-5 mob-px-3 ipadp-px-3">
      <div id="bookonline" class="res-diary-holder d-inline-block shadow w-100">
        <div class="res-diary-inner">
          <div class="loader loader-inner d-table">
            <div class="d-table-cell align-middle">
              <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
          </div>
          <div class="position-relative z-2">
            <div id="rd-widget-frame" style="max-width: 600px; margin: auto;"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-7 pt-5 mt-5 mob-mt-0 text-center text-lg-left">
      <div class="pre-title-lines mob-mx-auto mb-4"></div>
      <h1 class="mb-4 page-title">SIX NATIONS AT HAYMARKET</h1>
      <a href="/menu" target="_blank">
        <button type="button" class="btn btn-primary mb-5 mob-mb-0" data-aos="fade-up">View Menu </button>
      </a>
    </div>
  </div>
</div>
<seating-options :bg="'#141414'" :text="'#fff'" :lines="'#fff'"></seating-options>
<div class="container position-relative z-2 mob-mt-5">
  <div class="row">
    <div class="col-12 py-5 mb-5 mob-mb-0">
      <mailing-list :id="'ml-1-'" :bg="'#141414'" :text="'#fff'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection
@section('scripts')
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/38264?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
window.addEventListener('load', function() {
  document.querySelectorAll('.booknowbtn').forEach(function(button) {
    button.addEventListener('click', function() {
      var targetElement = document.getElementById("bookonline");
      var offset = 100;
      var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
      window.scrollTo({
        top: targetPosition - offset,
        behavior: 'smooth'
      });
    });
  });
});
</script>
@endsection