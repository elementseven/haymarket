@php
$page = 'Homepage';
$pagetitle = "Haymarket - Belfast's vibrant indoor/outdoor bar & street food hangout";
$metadescription = "Haymarket Belfast is Belfast's vibrant indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.";
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('head_section')

@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 px-0 mb-5">
  <div class="d-flex overflow-hidden bg home-top-bg home-vid" style=";max-height:640px;overflow: hidden;display: flex;align-items: center;justify-content: center;">
    <div class="container-fluid px-0 h-100" style="top: 0;left: 0;min-width: 750px;">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe src="https://player.vimeo.com/video/689775525?h=5433e291e4&autoplay=1&loop=1&autopause=0&background=1" width="640" height="361" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen loading="lazy"></iframe>
      </div>
    </div>
    <img src="/img/logos/logo.svg" class="mb-5 h-auto top-logo" alt="haymarket belfast Logo" width="438" height="163"/>
  </div>
  <div class="video-grad"></div>
  <div class="video-gradient d-none d-md-block"></div>
</header>
<home-offers-slider></home-offers-slider>
<div class="pt-5 mob-pt-0"></div>
<event-tiles></event-tiles>
<homepage-header-resdiary></homepage-header-resdiary>
@endsection
@section('content')
<seating-options></seating-options>
<div class="container py-5 mob-pt-0 position-relative z-2">
  <div class="row text-center text-lg-left">
    <div class="col-lg-5 py-5 mob-pb-0 ipadp-pb-0">
      <div class="haybale-bg py-5">
        <div class="pre-title-lines mb-4 mob-mx-auto"></div>
        <h2>Craft, Cocktails <br/>& Much More</h2>
      </div>
    </div>
    <div class="col-lg-7 py-5 mob-py-0 mob-px-4 ipadp-pt-0">
      <p class="text-larger mt-4 mb-0">Our indoor bars are spread over three floors, offering the perfect spot for a cosy date night or spontaneous cocktail or two! The outdoor area is the go to location in the City Centre for an alfresco drink. Banging food, banging drinks & a banging atmosphere. There's nothing more you need. </p>
      <button type="button" class="btn btn-primary booknowbtn mt-4">Book now</button>
    </div>
  </div>
</div>
<div class="container-fluid position-relative py-5 z-1">
  <picture>
    <source data-srcset="/img/graphics/chips-burger.webp" type="image/webp"/> 
    <source data-srcset="/img/graphics/chips-burger.png" type="image/png"/> 
    <img data-src="/img/graphics/chips-burger.png" type="image/png" width="550" height="1156" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy chips-burger-top-right h-auto"/>
  </picture>
  {{-- <div class="row py-5 ipadp-pt-0">
    <news-inline></news-inline>
  </div> --}}
  <div class="container py-5 text-center bg-dark position-relative z-2 shadow">
    <div class="row">
      <div class="col-lg-4">
        <img data-src="/img/logos/tripadvisor.svg" type="image/svg" width="290" height="1156" alt="Trip Advisor - Haymarket Belfast" class="lazy h-auto"/>
      </div>
      <div class="col-lg-5 pt-2">
        <p class="text-large">Check out our reviews on Tripadvisor!</p>
      </div>
      <div class="col-lg-3">
        <a href="https://www.tripadvisor.co.uk/Restaurant_Review-g186470-d23517646-Reviews-Haymarket-Belfast_Northern_Ireland.html?m=19905" target="_blank" aria-label="Tripadvisor" rel="noreferrer">
          <button type="button" class="btn btn-primary">Read Reviews</button>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="container pt-5 position-relative z-2">
  <div class="row text-center text-lg-left">
    <div class="col-12 mb-5 3pb-4">
      <div class="pre-title-lines mb-4 mob-mx-auto mob-mb-3"><span class="d-none d-lg-inline"><a href="https://instagram.com/haymarketbelfast" class="text-lowercase" target="_blank" rel="noreferrer" aria-label="Instagram">@haymarketbelfast <i class="fa fa-instagram ml-2"></i></a></span></div>
      <p class="d-lg-none text-large"><b><a href="https://instagram.com/haymarketbelfast" target="_blank" rel="noreferrer" aria-label="Instagram">@haymarketbelfast <i class="fa fa-instagram ml-2"></i></a></b></p>
      <p class="mimic-h3 mb-4">FOLLOW US ON INSTAGRAM</p>
      <instagram-feed></instagram-feed>
    </div>
  </div>
</div>
<div class="container position-relative z-2 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 py-5">
      <mailing-list :id="'ml-1-'"></mailing-list>
    </div>
  </div>
</div>

@endsection
@section('modals')
{{-- <offer-modal></offer-modal> --}}
@endsection
@section('scripts')
  <input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/26681?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
window.addEventListener('load', function() {
  document.querySelectorAll('.booknowbtn').forEach(function(button) {
    button.addEventListener('click', function() {
      var targetElement = document.getElementById("bookonline");
      var offset = 100;
      var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
      window.scrollTo({
        top: targetPosition - offset,
        behavior: 'smooth'
      });
    });
  });
});
</script>
@endsection