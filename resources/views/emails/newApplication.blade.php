<html>
<head></head>
<body style="background: white; color: black;">
	
<div style="background-color:#ffffff;">
  <!--[if gte mso 9]>
  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="" color="#ffffff"/>
  </v:background>
  <![endif]-->
  <style>a{color: #2c2c2c;}</style>
  <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
      	<td valign="top" align="left" background="">
	      	
	        <table width="80%" style="font-family:'Arial', arial, sans-serif, serif; text-align: left; font-weight:100; max-width: 720px;" align="center">
				
				<tr style="margin:40px 0 40px 0">
					<td>
				<p style="text-align: center; padding: 40px 40px 0;">
					<img src="{{url('/')}}/img/logos/logo.png" width="270" style="margin: auto; display: block;" alt="Haymarket Belfast Logo" />
				</p>

				<p style="font-size:22px; background-color: #262624; padding: 13px 15px; height:auto;color:#fff; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;font-weight:700;">New Job Application</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><span style="font-family:'Arial', arial, sans-serif;">Hi,</span></p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;">{{$application->name}} has uploaded their CV and applied for a role using the application form on your website. See details below:</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Name:</b> {{$application->name}}</p>
				
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Role Applied For:</b> {{$application->role}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Email Address:</b> {{$application->email}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;margin: 25px auto;"><b>Phone:</b> {{$application->phone}}</p>

				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><b>View CV:</b></p>
				<p style="font-size:18px; color:#2c2c2c; text-align: left; font-family:'Arial', arial, sans-serif;"><a href="{{url('/')}}/nova/resources/applications/{{$application->id}}">Click to login and view CV</a></p>

				</td>
				</tr>
				<tr><td>
					<hr style="margin: 30px auto 0;">
					<img src="{{url('/')}}/img/logos/logo.png" width="120" style="margin: 30px auto 0; display: block;" alt="Haymarket Belfast Logo" />
				<p style="font-size:12px; color:#2c2c2c; font-family:'Arial', arial, sans-serif; text-align: center;">This is an automatic email sent from the <a href="http://haymarketbelfast.com">Haymarket Belfast website</a>.<br>Please ignore this email if it was sent to you by mistake.</p></td></tr>
			</table>
		</td>
    </tr>
  </table>
</div>
</body>
</html>