@php
$page = '404';
$pagetitle = '404 | Haymarket Belfast';
$metadescription = 'Page not found';
$pagetype = 'white';
$pagename = '404';
$ogimage = 'https://www.haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative py-5 my-5 menu-padding">
  <div class="row py-5 mob-pt-3">
    <div class="col-lg-12 my-5 text-center">
      <h1 class="mob-mt-0 page-title">WOOPS!</h1>
      <p class="text-large mb-4">Unfortunately the page you were looking for wasn't found!</p>
  {{--     <a href="/">
      	<div class="btn btn-primary btn-icon">Home <i class="fa fa-home"></i></div>
      </a> --}}
    </div>
  </div>
</header>
@endsection
@section('content')

@endsection