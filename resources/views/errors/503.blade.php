@php
$page = '503';
$pagetitle = 'Website coming soon | Haymarket Belfast';
$metadescription = 'Page not found';
$pagetype = 'white';
$pagename = '503';
$ogimage = 'https://www.haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.maintenance', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
body{
  color: #fff;
  text-align: center;
  font-family: macho, sans-serif;
}
h1{
  font-size: 50px;
  line-height: 75px;
  font-family: aw-conqueror-inline, sans-serif;
  font-weight: 400;
  font-style: normal;
}
p{
  font-family: macho, sans-serif;
  font-weight: 400;
  font-style: normal;
  font-size: 20px;
}
.dis-table{
  display: table;
  width: 100%;
  height: 100vh;
}
.vert-mid{
  display: table-cell;
  vertical-align: middle;
  width: 100%;
}
a{
  color: #D54148;
}
a:hover{
  color: #D54148;
}
.bg-dark{
  background-color: #262624!important;
}
@media only screen and (max-width : 767px){
  h1{
    font-size: 30px;
  } 
}
</style>
@endsection
@section('header')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-12 text-center">
      <div class="container dis-table">
        <div class="vert-mid">
          <div class="row justify-content-center">
            <div class="col-12">
              <img src="/img/logos/logo.svg" width="300" alt="Haymarket Belfast logo" class="mb-4"/>
              <p class="my-2 text-uppercase"><b>Coming Soon</b></p>
              <p class="mb-2">Opening on 2nd July</p>
              <p>Follow us on <a href="https://instagram.com/haymarketbelfast" target="_blank">Instagram <i class="fa fa-instagram"></i></a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- <top-details></top-details>
 --}}@endsection
@section('content')

@endsection