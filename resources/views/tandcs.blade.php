@php
$page = 'Terms & Conditions';
$pagetitle = 'Terms & Conditions | Haymarket Belfast';
$metadescription = 'On Site & Our Services';
$pagetype = 'white';
$pagename = 'tandcs';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header class="container position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-12 pt-5 mob-pt-0 ipadp-pt-0 text-center text-lg-left">
      <div class="pre-title-lines mob-mx-auto mb-4 mob-my-45"></div>
      <h1 class="mob-mt-0">Terms &<br/>Conditions</h1>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-1">
  <div class="row">
    <div class="container pt-4">
      <p class="mb-4 text-large"><b>Bottomless Brunch T's&C's</b></p>
      <ol class="mb-5 pb-5">
        <li><p>'Bottomless' does not means an unlimited supply of alcoholic drinks. </p></li>
        <li><p>We reserve the right to refuse service to customers who, in our opinion, appear to be intoxicated. </p></li>
        <li><p>Drinks included in the offer must be served with food. Customers cannot use the promotion for drinking purposes only. </p></li>
        <li><p>All drinks included in the offer are served by table service only. </p></li>
        <li><p>Customers are only allowed to have one drink at a time under this promotion. A customer will not be served until they have finished the drink they have. </p></li>
      </ol>
      <p>Our brunch must be booked and paid for in advance and any brunch offers we have running will not be valid if booked on site</p>
        <p class="mb-4 mt-5 text-large"><b>On site and our service T's&C's</b></p>
        <ol>
          <li><p>Haymarket Belfast is a strictly adults only venue, only guests over 18 years old are permitted on site and a valid form of ID may be required. </p></li>

          <li><p>We would ask customers to report any accidents before leaving. Accidents reported after leaving our site cannot be dealt with appropriately. </p></li>
          
          <li><p>We have a strict NO REFUND policy on our services including vouchers (this, however, does not include those times where the centres are closed by the management and alternative arrangements cannot be agreed with the customer). </p><p>If you need to change a booking DO NOT CANCEL, just give us a message and we will change this for you! </p></li>
          
          <li><p>Bookings can be rescheduled and transferred however we do require a minimum of 24 hours’ notice and we will make every effort to accommodate you. The Management reserve the right to refuse admission. (Please see Section. 20 for additional information.) </p></li>
          
          <li><p>NO foul language or abuse of our staff will be tolerated at any time. </p></li>
          
          <li><p>Your booking reference is your proof of purchase. It is COMPULSORY that you present this booking confirmation when you arrive. Inability or refusal to do so will result in you NOT being admitted. Proof provided via smart phone or other smart devices will be accepted (Please ensure devices are fully charged to ensure we can see your booking if that option is to be used). </p></li>
          
          <li><p>NO football tops, sports attire or baseball caps are to be worn on site at any time and guests wearing such items will be refused entry. The Management reserve the right to ask any customer to remove themselves from the premises for any of the following reasons, aggressive behaviour towards staff, swearing (generally or at staff), inappropriate behaviour or use of the facilities.</p></li>
          
          <li><p>All Gift Vouchers are valid for 6 months from their date of purchase.</p></li>
          
          <li><p>You acknowledge that by entering the Company’s premises and that you grant Haymarket Belfast, on your behalf, the permission to photograph and/or record themselves, in connection with the Company and to use the photograph and/or recording for all purposes, including advertising and promotional purposes, in any manner chosen. The guest also waives the right to inspect or approve the use of the photograph and/or recording and acknowledges and agrees to the rights granted to this release are without compensation of any kind.</p></li>
          
          <li><p>All offers and promotions run independently and cannot be used in conjunction with each other. Promotional codes have no monetary value and cannot be transferred between activities. All offers and promotions are subject to availability and opening hours. </p></li>

          <li><p>Our 2 for 1 cocktail offer runs every week day 5-8pm, this is 2 for 1 on any house cocktail but it must be the same cocktail.</p></li>

          <li><p>Our 3 for 2 offer in The Armoury only applies to Wednesday general admission and does not apply to packages.</p></li>

        </ol>

    </div>
  </div>
</div>
<div class="container py-5 mob-px-4 mob-py-0">
  <div class="row my-5">
    <div class="col-lg-6 py-5 mob-py-0 text-center text-lg-left">
      <div class="pre-title-lines mob-mx-auto mb-4"></div>
      <p class="mimic-h2 mob-smaller"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline ">Have a question for us?</span></p>
      <a href="{{route('contact')}}"><button type="button" class="btn btn-primary">Get in touch</button></a>
    </div>
  </div>
</div>
@endsection