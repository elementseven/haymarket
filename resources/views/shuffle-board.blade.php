@php
$page = 'Shuffle Board';
$pagetitle = "Shuffle Board @ The Stock Exchange @ Haymarket - Belfast's vibrant indoor/outdoor bar & street food hangout";
$metadescription = "Discover the fun of shuffleboard at The Stock Exchange in Haymarket, Belfast! Perfect for groups and parties, our dedicated shuffleboard room offers a lively, competitive atmosphere. Challenge friends or family to a game and enjoy a unique entertainment experience in the heart of Belfast.";
$pagetype = 'stock';
$pagename = 'stock';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
  .stock-top-logo{
    width: 500px;
  }
  @media only screen and (max-width : 767px){
    .stock-top-logo{
      width: 80vw;
    }
  }
</style>
@endsection
@section('header')
<header class="container-fluid position-relative z-1 pt-5 mt-5 mob-mt-0 text-center">
  <img src="/img/logos/the-stock-exchange.svg" class="mb-4 mt-5 h-auto" alt="The Armoury - Haymarket Belfast Logo" width="500" height="114"/>
  <picture>
    <source srcset="/img/venues/stock/cocktail.webp" type="image/webp"/> 
    <source srcset="/img/venues/stock/cocktail.png" type="image/png"/> 
    <img src="/img/venues/stock/cocktail.png" type="image/png" width="550" height="1156" alt="The Stock Exchange - Virtual Darts at - Haymarket Belfast" class="cocktail-top-right h-auto"/>
  </picture>
</header>
@endsection
@section('content')
<div class="container pb-lg-5 ipadp-pt-0 position-relative z-2">
  <div class="row text-center text-lg-left pt-5 mob-pt-0">
    <div class="col-12 text-center mb-5">
      <div class="pre-title-lines mb-4 mt-5 mx-auto"></div>
      <h1 class="">Shuffle Board</h1>
      <p class=""><b>How does it work?</b></p>
    </div>
    <div class="col-lg-6">
      <picture>
        <source srcset="/img/venues/stock/shuffle-more-info.webp?v=2024-11-19" type="image/webp"/> 
        <source srcset="/img/venues/stock/shuffle-more-info.jpg?v=2024-11-19" type="image/jpeg"/> 
        <img src="/img/venues/stock/shuffle-more-info.jpg?v=2024-11-19" type="image/jpeg" width="350" height="350" alt="Shuffle Board at Haymarket Belfast" class="lazy w-100 h-auto op-45 shadow"/>
      </picture>
    </div>
    <div class="col-lg-6 pl-4 mob-px-3 text-center text-lg-left">
      <p>Shuffleboard is a game where players slide weighted pucks down a table to score points and knock their opponent's pucks off the board. It is as easy as 3 simple steps:</p>
      <p class="text-primary mb-0"><b>Gameplay</b></p>
      <p>Players take turns sliding their pucks down the table, aiming to land them in scoring zones at the other end. The player or team with the most points at the end of a round wins.</p>
       
      <p class="text-primary mb-0"><b>Scoring</b></p>
      <p>Players earn points based on where their pucks land. Pucks that land closer to the far edge of the board score more points. To count, the puck must be completely within a scoring zone.</p>
       
      <p class="text-primary mb-0"><b>Strategy</b></p>
      <p>Players can use a variety of strategies to win, such as knocking their opponent's pucks off the board, protecting their own pucks, and using the sides of the table to their advantage</p>
      <a href="https://ecom.roller.app/haymarketbelfast/stockexchange/en/products" target="_blank">
        <button type="button" class="btn btn-primary mt-3 booknowbtn">Book Now</button>
      </a>
    </div>
  </div>
  <div class="row pt-5 mt-5">
    <div class="col-12 pt-2 pl-4 mob-px-3 text-center text-lg-left">
      <h3 class="mb-4">FAQs</h3>
      <p class="text-primary"><b>We have a large group; will we have multiple playing areas?</b></p>
      <p class="mb-4">Absolutely! For large groups or corporate enquiries, please chat to our events team at <a href="mailto:info@haymarketbelfast.com">info@haymarketbelfast.com</a></p>
      <p class="text-primary"><b>Can anyone play Shuffle Board?</b></p>
      <p class="mb-4">Of course! From seasoned shufflers to first timer, shuffleboard is super accessible to everyone! Please note we are an 18+ venue!</p>
      <p class="text-primary"><b>What time should we arrive for our booking?</b></p>
      <p class="mb-4 pb-lg-5">We recommend arriving 10 minutes before your booked slot, so we can get you set up and playing as soon as possible. You are always able to arrive early and enjoy some delicious food and drinks in our bar!</p>
      <h3 class="mb-4 mt-5">On The Day:</h3>
      <p>We ask all guests to arrive at the venue (Haymarket Belfast, 84 Royal Avenue) 10 minutes prior to the start time to ensure that you are ready to puck it up, as soon as your session starts (and maybe have a cheeky cocktail to get started!)</p>
      <p>Our hosts will take you to your board and run through some quick hints and tips, then you can commence your session! Splitting into 2 teams, you will go head-to-head knock outs to gain the highest points. Our serving team will be on hand to take a food and drinks order, delivered straight to your table, and you can take advantage of our epic stock market bar.</p>
      <p>Once finished, our host will retrieve your pucks. You will then be able to move out into ‘The Haymarket Courtyard’ to enjoy some more post-activity drinks. During weekends and holiday periods, we recommend booking and securing a table <a href="/the-courtyard#bookonline">HERE</a>.</p>
      <a href="https://ecom.roller.app/haymarketbelfast/stockexchange/en/products" target="_blank">
        <button type="button" class="btn btn-primary mt-3 booknowbtn">Book Now</button>
      </a>
    </div>
  </div>
</div>
<div class="pt-5"></div>
<seating-options-stock :activity="'shuffle'"></seating-options-stock>
<div class="container position-relative z-2 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 py-5">
      <mailing-list :id="'ml-1-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')

@endsection
@section('scripts')

@endsection