@php
$page = 'Homepage';
$pagetitle = "Christmas at Haymarket - Belfast's vibrant indoor/outdoor bar & street food hangout";
$metadescription = "Whether you are planning a Christmas party for the office or some festive drinks with friends, Haymarket is the perfect venue for your festive celebrations! Full venue hire available for bookings over 100 guests!";
$pagetype = 'christmas';
$pagename = 'christmas';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header id="homepage-top" class="container-fluid position-relative bg bg-down-up z-1 my-5 mob-my-0">
  <img src="/img/christmas/reef-1.svg" class="reef-1 h-auto" alt="Haymarket Belfast Christmas reef graphic" width="200" height="200">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center py-5 mob-py-0">
        <div class="col-lg-6 pr-5 mob-px-3 position-relative ipad-py-5 text-center mob-pt-5">
           <picture>
            <source srcset="/img/christmas/brochure-2024.webp" type="image/webp"/> 
            <source srcset="/img/christmas/brochure-2024.jpg" type="image/jpeg"/> 
            <img src="/img/christmas/brochure-2024.jpg" type="image/jpeg" alt="Christmas Brochure 2024 - Haymarket Belfast" class="w-100 h-auto mb-4"/>
          </picture>
          <a href="/docs/christmas-2024.pdf?2024-08-29" target="_blank">
            <button type="button" class="btn btn-primary mb-5 px-0" style="min-width: 250px;">Download Brochure</button>
          </a>
          
        </div>
        <div class="col-lg-6 pt-3 mob-mt-5 ipad-py-5 text-center text-lg-left mob-px-0 mob-mb-3">
          <div class="pre-title-lines mob-mx-auto mb-4"></div>
          <h1 class="mb-3 ipad-smaller">Xmas At Haymarket</h1>
          <p class="text-large pr-5 mb-2">Whether you are planning a Christmas party for the office or some festive drinks with friends, Haymarket is the perfect venue for your festive celebrations! Full venue hire available for bookings over 200 guests, Monday-Thursday!</p>
          <p class="text-large mb-2">From Christmas cocktail trees, to beer buckets, prosecco receptions to wine packages we have drinks to get everyone in the SPIRIT!</p>
          <p class="text-primary mb-4"><b>For bookings of 10+, any less please book via our homepage!</b></p>
          <button type="button" class="btn btn-primary mb-5 booknowbtn" >Enquire Now</button>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pb-5 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-lg-5 offset-lg-1">
      <picture>
        <source srcset="/img/vouchers/courtyard-christmas-3.webp" type="image/webp"/> 
        <source srcset="/img/vouchers/courtyard-christmas-3.jpg" type="image/jpeg"/> 
        <img src="/img/vouchers/courtyard-christmas-3.jpg" type="image/jpeg" alt="vouchers at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
      </picture>
    </div>
    <div class="col-lg-5 pl-5 mob-px-3 pt-5 mob-pt-4 text-center text-lg-left">
      <div class="pre-title-lines mob-mx-auto mb-4"></div>
      <p class="mimic-h2">CHRISTMAS VOUCHERS</p>
      <p class="mb-4">We have vouchers availble for each of our venues, check them out on our Christmas Vouchers page!</p>
      <a href="/vouchers">
        <button class="btn btn-primary" type="button">Christmas Vouchers</button>
      </a>
    </div>
  </div>
</div>
<div class="container mob-mt-5">
  <div class="row pb-5 mob-py-0">
    <div class="col-12">
      <div class="pre-title-lines mob-mx-auto mb-4"></div>
      <p class="mimic-h2">CHRISTMAS EVENTS</p>
    </div>
  </div>
</div>

<div class="container pb-5 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 pb-5">
      <christmas-slider></christmas-slider>
    </div>
  </div>
</div>
<div class="container container-wide pb-5 mb-5 mob-mb-0">
  <div class="row justify-content-center bar-videos">
    <div class="col-lg-3 col-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe width="423" height="752" class="embed-responsive-item" src="https://www.youtube.com/embed/A4GQ_mopiq0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">HEEL BAR</p>
    </div>
    <div class="col-lg-3 col-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe width="423" height="752" class="embed-responsive-item" src="https://www.youtube.com/embed/qsgF7dAd3w4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">THE GIN BAR</p>
    </div>
    <div class="col-lg-3 col-6 mb-4">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe width="423" height="752" class="embed-responsive-item" src="https://www.youtube.com/embed/hvZEkpro5s4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">THE COCKTAIL BAR</p>
    </div>
    <div class="col-lg-3 col-6 d-lg-none">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe width="423" height="752" class="embed-responsive-item" src="https://www.youtube.com/embed/YizThumeC44" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">GRESHAM BAR</p>
    </div>
  </div>
  <div class="row justify-content-center bar-videos">
    <div class="col-lg-3 col-6 d-none d-lg-block">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe width="423" height="752" class="embed-responsive-item" src="https://www.youtube.com/embed/YizThumeC44" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">GRESHAM BAR</p>
    </div>
    <div class="col-lg-3 col-6">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe width="423" height="752" class="embed-responsive-item" src="https://www.youtube.com/embed/VkX7oVaKcZU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
      </div>
      <div class="pre-title-lines mx-auto my-3"></div>
      <p class="text-large title text-center mt-2">THE ARMOURY</p>
    </div>
    <div class="col-lg-3 col-12 mob-mt-4">
      <div class="card text-center text-lg-left shadow border-0 px-4 py-5" style="background-color:#004238;min-height:calc(693px - 5rem);">
      <p class="text-large"><b>Fancy adding a bit of action to your Christmas party?</b></p> 
      <p>Why not book The Armoury, our brand-new state of the art simulated shooting range!</p> 
      <p>With 20+ high tech virtual games to choose from along with the option to enjoy tasty sharing plates and drinks to your own private booths!</p>
      <p class="mb-4">Semi private & full private hire available!</p>
      <button class="btn btn-primary booknowbtn mob-mx-auto" type="button">Interested?</button>
    </div>
    </div>
  </div>
</div>
<div class="container pb-5 position-relative z-2 mob-pb-0">
  <div class="row text-center text-lg-left justify-content-center">
    <div class="col-lg-5 pb-5 ipadp-pb-0 mob-pb-0">
      <div class="haybale-bg py-5 ">
        <div class="pre-title-lines mb-4 mob-mx-auto"></div>
        <h2>Great for all parties & Corporate events</h2>
      </div>
    </div>
    <div class="col-lg-7 py-5 mob-py-0 mob-px-4  ipadp-pt-0">
      <p class="text-larger mt-4 mob-mt-0">Haymarket Belfast is Belfast's newest indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.</p>
      <p class="text-larger mb-5">The indoor bar is spread over three floors, offering the perfect spot for a cosy date night or spontaneous cocktail or two! The outdoor area is the go to location in the City Centre for an alfresco drink. Banging food, banging drinks & a banging atmosphere. There's nothing more you need. </p>
      <button type="button" class="btn btn-primary booknowbtn">Enquire Now </button>
    </div>
  </div>
</div>
<div class="container-fluid position-relative pb-5 mb-5 mob-pb-0">
  <div class="row">
    <div class="container pt-4">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <div class="pre-title-lines mb-4 mx-auto"></div>
          <p class="mimic-h2 mb-5 mob-mb-3">Request a booking</p>
        </div>
        <div id="bookonline" class="col-lg-12 mob-px-4">
          <christmas-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></christmas-form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('modals')

@endsection
@section('scripts')
<script>
  window.addEventListener('load', function() {
  document.querySelectorAll('.booknowbtn').forEach(function(button) {
    button.addEventListener('click', function() {
      var targetElement = document.getElementById("bookonline");
      var offset = 230;
      var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
      window.scrollTo({
        top: targetPosition - offset,
        behavior: 'smooth'
      });
    });
  });
});
</script>
<style>
  .bar-videos .embed-responsive-16by9::before{
    padding-top: 178.25%;
  }
</style>

@endsection