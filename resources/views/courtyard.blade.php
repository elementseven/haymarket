@php
$page = 'Homepage';
$pagetitle = "Haymarket - Belfast's vibrant indoor/outdoor bar & street food hangout";
$metadescription = "Haymarket Belfast is Belfast's vibrant indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.";
$pagetype = 'light';
$pagename = 'courtyard';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<div class="haymarket-bg"></div>
<header class="container position-relative z-1 pt-5 text-center logo-lines mob-px-4 mob-mb-3">
  <div class="row half_row justify-content-center pt-5 pb-3 mob-px-3">
    <div class="col-lg-8 half_col pt-5 mob-pt-0">
      <div class="venue-top-no-vid">
        <img src="/img/logos/the-courtyard.svg" class="mb-4 h-auto" alt="haymarket belfast Logo" width="550" height="246"/>
        <p class="text-one mb-4">The Courtyard is a vibrant indoor/outdoor bar & street food hangout at Haymarket Belfast. Serving up a range of delicious cocktails, draught beer & tasty street food!</p>
        {{-- <button type="button" class="btn btn-primary mx-auto position-relative z-2" onclick="RollerCheckout.show()">Book Now</button> --}}
        <button type="button" class="btn btn-primary mx-auto position-relative z-2 booknowbtn">Book Now</button>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="contianer-fluid py-5 overflow-hidden text-center position-relative z-2 venue-offers">
  <div class="pre-title-lines mb-4 mx-auto"></div>
  <h2 class="mb-4">Offers & events</h2>
  <offers-slider :venue="1"></offers-slider>
</div>
<div class="pt-5 mob-pt-0"></div>
<event-tiles></event-tiles>
<homepage-header-resdiary></homepage-header-resdiary>
<div class="container text-center pt-5">
  <div class="pre-title-lines mb-4 mx-auto"></div>
  <h2>Menus</h2>
</div>
<seating-options></seating-options>
<div class="container pt-5 ipadp-pt-0 position-relative z-2">
  <div class="row px-0 d-lg-none mt-4">
    <div class="col-12 px-0">
      <picture>
        <source srcset="/img/home/walk-in.webp" type="image/webp"/> 
        <source srcset="/img/home/walk-in.jpg" type="image/jpeg"/> 
        <img src="/img/home/walk-in.jpg" width="507" height="507" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy w-100 h-auto mt-5"/>
      </picture>
    </div>
  </div>
  <div class="row text-center text-lg-left">
    <div class="col-lg-5 py-5 mob-pb-0 ipadp-pb-0">
      <div class="haybale-bg py-5">
        <div class="pre-title-lines mb-4 mob-mx-auto"></div>
        <h2>Craft,<br/>Cocktails & Much More</h2>
      </div>
    </div>
    <div class="col-lg-7 py-5 mob-py-0 mob-px-4 ipadp-pt-0">
      <p class="text-larger mt-4 mob-mt-0">Haymarket Belfast is a vibrant indoor/outdoor bar & street food hangout. Serving up a range of delicious cocktails, draught beer & tasty street food in the historic Haymarket.</p>
      <p class="text-larger mb-0">The indoor bar is spread over three floors, offering the perfect spot for a cosy date night or spontaneous cocktail or two! The outdoor area is the go to location in the City Centre for an alfresco drink. Banging food, banging drinks & a banging atmosphere. There's nothing more you need.</p>
      {{-- <button type="button" class="btn btn-primary mt-4" onclick="RollerCheckout.show()">Book now</button> --}}
      <button type="button" class="btn btn-primary mt-4 booknowbtn">Book now</button>
    </div>
  </div>
</div>
<div class="container-fluid position-relative my-5 py-5 z-1">
  <picture>
    <source data-srcset="/img/graphics/chips-burger.webp" type="image/webp"/> 
    <source data-srcset="/img/graphics/chips-burger.png" type="image/png"/> 
    <img data-src="/img/graphics/chips-burger.png" type="image/png" width="550" height="1156" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy chips-burger-top-right h-auto"/>
  </picture>
  {{-- <div class="row py-5 ipadp-pt-0">
    <news-inline></news-inline>
  </div> --}}
  <div class="container py-5 text-center bg-dark position-relative z-2 shadow">
    <div class="row">
      <div class="col-lg-4">
        <img data-src="/img/logos/tripadvisor.svg" type="image/svg" width="290" height="1156" alt="Trip Advisor - Haymarket Belfast" class="lazy h-auto"/>
      </div>
      <div class="col-lg-5 pt-2">
        <p class="text-large">Check out our reviews on Tripadvisor!</p>
      </div>
      <div class="col-lg-3">
        <a href="https://www.tripadvisor.co.uk/Restaurant_Review-g186470-d23517646-Reviews-Haymarket-Belfast_Northern_Ireland.html?m=19905" target="_blank" aria-label="Tripadvisor" rel="noreferrer">
          <button type="button" class="btn btn-primary">Read Reviews</button>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="container pt-5 position-relative z-2 ">
  <div class="row text-center text-lg-left">
    <div class="col-12 mb-5 3pb-4">
      <div class="pre-title-lines mb-4 mob-mx-auto mob-mb-3"><span class="d-none d-lg-inline"><a href="https://instagram.com/haymarketbelfast" class="text-lowercase" target="_blank" rel="noreferrer" aria-label="Instagram">@haymarketbelfast <i class="fa fa-instagram ml-2"></i></a></span></div>
      <p class="d-lg-none text-large"><b><a href="https://instagram.com/haymarketbelfast" target="_blank" rel="noreferrer" aria-label="Instagram">@haymarketbelfast <i class="fa fa-instagram ml-2"></i></a></b></p>
      <p class="mimic-h3 mb-4">FOLLOW US ON INSTAGRAM</p>
      <instagram-feed></instagram-feed>
    </div>
  </div>
</div>
<div class="container position-relative z-2 mb-5 py-5 mob-mb-0">
  <div class="row">
    <div class="col-12 py-5">
      <mailing-list :id="'ml-1-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')
{{-- <div class="modal fade mx-auto" id="midweekModal" tabindex="-1" role="dialog" aria-labelledby="midweekModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content text-center">
      <picture>
        <source srcset="/img/christmas/newyears.webp?v=2023-12-19" type="image/webp"/> 
        <source srcset="/img/christmas/newyears.jpg?v=2023-12-19" type="image/jpg"/> 
        <img src="/img/christmas/newyears.jpg?v=2023-12-19" width="507" height="507" type="image/jpg" alt="Bottomless Brunch Takeovers - Haymarket Belfast" class="mw-100 h-auto d-inline-block" />
      </picture>
      <a href="/christmas/newyears">
        <button type="button" class="btn btn-primary mx-auto mt-3">Book Now</button>
      </a>
      <div class="btn-text cursor-pointer text-center d-block w-100 mt-2 position-relative z-200" data-dismiss="modal" aria-label="Close Modal">[X] Close</div>
    </div>
  </div>
</div> --}}
@endsection
@section('scripts')
<input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/26681?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
window.addEventListener('load', function() {
  document.querySelectorAll('.booknowbtn').forEach(function(button) {
    button.addEventListener('click', function() {
      var targetElement = document.getElementById("bookonline");
      var offset = 100;
      var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
      window.scrollTo({
        top: targetPosition - offset,
        behavior: 'smooth'
      });
    });
  });
});
</script>
 {{-- <script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/haymarketbelfast/testvenue/en/products"></script> --}}
@endsection