@php
$page = 'News';
$pagetitle = $post->title . ' | Haymarket Belfast';
$metadescription = $post->meta_description;
$keywords = $post->keywords;
$pagetype = 'light';
$pagename = 'news';
$ogimage = 'https://haymarketbelfast.com' . $post->getFirstMediaUrl('double');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<picture>
  <source srcset="/img/graphics/burger-beer.webp" type="image/webp"/> 
  <source srcset="/img/graphics/burger-beer.png" type="image/jpeg"/> 
  <img src="/img/graphics/burger-beer.png" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy burger-beer-top-left"/>
</picture>
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>
<header class="container-fluid position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row">
    <div class="container">
      <div class="row justify-content-center pt-5 pb-4 mob-pt-0">
        <div class="col-12 pt-5 ipadp-pt-0 mob-pt-0 mob-mt-0 text-center text-lg-left mob-mb-3">

          <div class="pre-title-lines mb-4 mob-my-45 mob-mx-auto mob-mb-3 text-uppercase"><span class="d-none d-lg-inline"><a href="{{route('news.index')}}"><b>Browse all news</b></a></span></div>
          <p class="d-lg-none text-large text-uppercase"><a href="{{route('news.index')}}"><b>Browse all news</b></a></p>
          <h1 class="mb-3 mob-mb-0 blog-title">{{$post->title}}</h1>
          <p class="text-large mb-4">{{$post->excerpt}}</p>
          <p class="mb-1"><b>Share this article:</b></p>
          <p class="text-larger">
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
    <div class="row pb-5 mob-py-0 text-center text-lg-left">
        <div class="col-12 mob-mt-0 mb-5 mob-mb-3">
          <div class="backdrop overflow-hidden pb-3">
            <picture>
              <source media="(min-width: 1200px)" srcset="{{$post->getFirstMediaUrl('news','double-webp')}}" type="image/webp"/> 
              <source media="(min-width: 1200px)" srcset="{{$post->getFirstMediaUrl('news','double')}}" type="{{$post->getFirstMedia('news')->mime_type}}"/> 
              <source media="(min-width: 768px)" srcset="{{$post->getFirstMediaUrl('news','normal-webp')}}" type="image/webp"/> 
              <source media="(min-width: 768px)" srcset="{{$post->getFirstMediaUrl('news','normal')}}" type="{{$post->getFirstMedia('news')->mime_type}}"/> 
              <source media="(min-width: 1px)" srcset="{{$post->getFirstMediaUrl('news','featured-webp')}}" type="image/webp"/> 
              <source media="(min-width: 1px)" srcset="{{$post->getFirstMediaUrl('news','featured')}}" type="{{$post->getFirstMedia('news')->mime_type}}"/> 
              <img src="{{$post->getFirstMediaUrl('news','featured')}} 1w,  {{$post->getFirstMediaUrl('news','normal')}} 768w, {{$post->getFirstMediaUrl('news','double')}} 1200w" type="{{$post->getFirstMedia('news')->mime_type}}" alt="{{$post->title}}" class="w-100 backdrop-content lazy" />
            </picture>
            <div class="backdrop-back" data-aos="fade-down-right"></div>
          </div>
        </div>
        <div class="col-12 mob-mt-0 blog-body">
          {!!$post->content!!}
        </div>
        <div class="col-12 mt-5 ">
          <p class="mb-1"><b>Share this article:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
<div class="container pb-5 mob-mt-5 mob-px-4">
  <div class="row mb-5">
    <div class="col-lg-6 py-5 mob-py-0 text-center text-lg-left">
      <div class="pre-title-lines mob-mx-auto mb-4"></div>
      <p class="mimic-h2 mob-smaller"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">More Haymarket Belfast news</span></p>
      <a href="{{route('news.index')}}"><button type="button" class="btn btn-primary mob-mt-2">See all news</button></a>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection