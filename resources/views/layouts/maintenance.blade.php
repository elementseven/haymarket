<!DOCTYPE html>
<html id="wav-html" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#111111">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#111111">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <title>{{$pagetitle}}</title>

  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="Haymarket Belfast">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$ogimage}}">
  <meta property="og:description" content="{{$metadescription}}">
  <meta name="description" content="{{$metadescription}}">
  <link href="{{ asset('css/app.css') }}?v0.00000008" rel="stylesheet"/>
  @yield('styles')
  <script type="application/ld+json">
   {
    "@context" : "https://schema.org",
    "@type" : "Organization",       
    "telephone": "+442895031307",
    "contactType": "Customer service"
  }
</script>
<script>window.dataLayer = window.dataLayer || [];</script>
</head>  
<body id="wav-body" class="front">
@yield('fbroot')
<div id="main-wrapper">
  <div id="app" class="front {{$pagetype}} overflow-hidden mw-100" data-scroll-container>
    @yield('header')
    <main id="content" style="z-index: 2;" class="">
      <div id="menu-trigger"></div>
      @yield('content')
    </main>
    {{-- <site-footer></site-footer> --}}
    @yield('modals')
    <loader></loader>
  </div>
  <div id="menu_body_hide"></div>
  <!-- Modal -->
</div>
@yield('prescripts')
<script src="{{ asset('js/app.js') }}"></script>
@yield('scripts')
</body>
</html>