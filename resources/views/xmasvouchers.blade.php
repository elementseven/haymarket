@php
$page = 'Homepage';
$pagetitle = "Vouchers - Haymarket Belfast";
$metadescription = "Haymarket Belfast Vouchers";
$pagetype = 'christmas';
$pagename = 'Vouchers';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<div class="christmas-snow"></div>
<header class="container-fluid vouchers-christmas-bg position-relative bg bg-down-up z-1 py-5 mob-my-0">
	<div class="christmas-snow"></div>
  <div class="row position-relative z-3 pb-5">
    <div class="container container-wide pb-lg-5 pb-0">
      <div class="row py-5 justify-content-center mob-py-0">
        <div class="col-xl-7 col-lg-9 pt-3 pt-5 text-center">
          <div class="pre-title-lines mx-auto mb-4 mt-5"></div>
  				<h1 class="page-title mob-page-title-smaller mb-2">Christmas<br/>Vouchers</h1>
  				<p class="mb-4"><b>We have vouchers availble for each of our venues, scroll down or select a venue below.</b></p>
  				<a href="#armoury-vouchers">
  					<button class="btn btn-gold mr-2 mob-mx-0 mob-mb-3">The Armoury</button>
  				</a>
  				<br class="d-lg-none d-none" />
  				<a href="#haymarket-vouchers">
  					<button class="btn btn-primary">The Courtyard</button>
  				</a>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div id="armoury-vouchers" class="pb-5 mb-5 position-relative z-2 christmas-armoury-vouchers" style="background-color: #002030; ">
	<div class="container container-wide">
	  <div class="row justify-content-center">
	  	<div class="col-12 text-center px-5">
	  		<img src="/img/logos/the-armoury.svg" class="mb-5 h-auto" alt="The Armoury - Haymarket Belfast Logo" width="280" height="80" style="max-width: 70vw;"/>
	  	</div>
	    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
	    	<div class="voucher">
		      <picture>
				    <source srcset="/img/vouchers/armoury-christmas-1.webp" type="image/webp"/> 
				    <source srcset="/img/vouchers/armoury-christmas-1.jpg" type="image/jpeg"/> 
				    <img src="/img/vouchers/armoury-christmas-1.jpg" type="image/jpeg" alt="The Armoury Christmas at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
				  </picture>
		      <div class="voucher-offer-content" style="background-color:#0D3B52">
		      	<p class="title text-large text-white mb-2">General<br/>Admission Voucher</p>
		      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
		      	<p class="text-small">The ideal gift for nan! Only joking...unless she enjoys a bit of competitive fun, testing her skills and having a laugh with her friends or family! Not to mention enjoying tasty sharing plates, pizzas & craft cocktails brought to her own private booth. Then you’re sorted!</p>
		      	<div class="mb-3"></div>
		      	{{-- <p class="subtext mb-4">Just remember...the more the merrier! They'll be able to spend this fabulous voucher any time on our banging food & drinks menu!</p> --}}
		      	<a href="https://ecom.roller.app/haymarketbelfast/armourychristmasvouchers/en/home" target="_blank">
		      		<button type="button" class="btn btn-gold shadow">Buy Now </button>
		      	</a>
		      </div>
		      <div class="backdrop-back"></div>
		    </div>
	    </div>
	    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
	    	<div class="voucher">
		      <picture>
				    <source srcset="/img/vouchers/armoury-christmas-2.webp" type="image/webp"/> 
				    <source srcset="/img/vouchers/armoury-christmas-2.jpg" type="image/jpeg"/> 
				    <img src="/img/vouchers/armoury-christmas-2.jpg" type="image/jpeg" alt="The Armoury Christmas 2 at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
				  </picture>
		      <div class="voucher-offer-content" style="background-color:#0D3B52">
		      	<p class="title text-large text-white mb-2">Date <br/>Night Voucher</p>
		      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
		      	<p class="text-small">Whether you’re looking for an interesting second date, a long overdue break from the kids, or a catch up with your bestie this voucher makes for the perfect gift for someone special! Includes a 90 min session in your own private booth, x1 drink of choice & a tasty main per person.</p>
		      	<div class="mb-3"></div>
		      	{{-- <p class="subtext mb-4">Just remember...the more the merrier! They'll be able to spend this fabulous voucher any time on our banging food & drinks menu!</p> --}}
		      	<a href="https://ecom.roller.app/haymarketbelfast/armourychristmasvouchers/en/home" target="_blank">
		      		<button type="button" class="btn btn-gold shadow">Buy Now </button>
		      	</a>
		      </div>
		      <div class="backdrop-back"></div>
		    </div>
	    </div>
	    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
	    	<div class="voucher">
		      <picture>
				    <source srcset="/img/vouchers/armoury-christmas-3.webp" type="image/webp"/> 
				    <source srcset="/img/vouchers/armoury-christmas-3.jpg" type="image/jpeg"/> 
				    <img src="/img/vouchers/armoury-christmas-3.jpg" type="image/jpeg" alt="The Armoury Christmas 3 at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
				  </picture>
		      <div class="voucher-offer-content" style="background-color:#0D3B52">
		      	<p class="title text-large text-white mb-2">Family <br/>Pass Voucher</p>
		      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
		      	<p class="text-small mb-4"> Reckon your cousins are in need of some family bonding? Then this is the ideal gift! Our family pass voucher covers 90 mins in one of our private booths for x4/5 guests ages 12+ Only available to use during our family sessions.</p>
		      	<div class="pb-3"></div>
		      	{{-- <p class="subtext mb-4">Just remember...the more the merrier! They'll be able to spend this fabulous voucher any time on our banging food & drinks menu!</p> --}}
		      	<a href="https://ecom.roller.app/haymarketbelfast/armourychristmasvouchers/en/home" target="_blank">
		      		<button type="button" class="btn btn-gold shadow">Buy Now </button>
		      	</a>
		      </div>
		      <div class="backdrop-back"></div>
		    </div>
	    </div>
	  </div>
	  <div class="row justify-content-center">
	    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
	    	<div class="voucher">
		      <picture>
				    <source srcset="/img/vouchers/armoury-christmas-4.webp" type="image/webp"/> 
				    <source srcset="/img/vouchers/armoury-christmas-4.jpg" type="image/jpeg"/> 
				    <img src="/img/vouchers/armoury-christmas-4.jpg" type="image/jpeg" alt="The Armoury Christmas 4 at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
				  </picture>
		      <div class="voucher-offer-content" style="background-color:#0D3B52">
		      	<p class="title text-large text-white mb-2">Bottomless<br/>Brunch Voucher</p>
		      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
		      	<p class="text-small">Add a little bit of spice to their Sundays with an Armoury bottomless brunch. Includes a 90 min shooting session for any Sunday along with 90 mins of bottomless drinks & a tasty main & side of their choice! EEK!</p>
		      	<div class="mb-3"></div>
		      	{{-- <p class="subtext mb-4">Just remember...the more the merrier! They'll be able to spend this fabulous voucher any time on our banging food & drinks menu!</p> --}}
		      	<a href="https://ecom.roller.app/haymarketbelfast/armourychristmasvouchers/en/home" target="_blank">
		      		<button type="button" class="btn btn-gold shadow">Buy Now </button>
		      	</a>
		      </div>
		      <div class="backdrop-back"></div>
		    </div>
	    </div>
	    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
	    	<div class="voucher">
		      <picture>
				    <source srcset="/img/vouchers/armoury-christmas-5.webp" type="image/webp"/> 
				    <source srcset="/img/vouchers/armoury-christmas-5.jpg" type="image/jpeg"/> 
				    <img src="/img/vouchers/armoury-christmas-5.jpg" type="image/jpeg" alt="The Armoury Christmas Cash Voucher at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
				  </picture>
		      <div class="voucher-offer-content" style="background-color:#0D3B52">
		      	<p class="title text-large text-white mb-2">Cash<br/>Voucher</p>
		      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
		      	<p class="text-small">Can't decide? Buying for someone fussy? Just load up a voucher and let them choose! Quick, simple & guaranteed great results. Win, win!</p>
		      	<div class="mb-3"></div>
		      	{{-- <p class="subtext mb-4">Just remember...the more the merrier! They'll be able to spend this fabulous voucher any time on our banging food & drinks menu!</p> --}}
		      	<a href="https://ecom.roller.app/haymarketbelfast/armourychristmasvouchers/en/home" target="_blank">
		      		<button type="button" class="btn btn-gold shadow">Buy Now </button>
		      	</a>
		      </div>
		      <div class="backdrop-back"></div>
		    </div>
	    </div>
	  </div>
	</div>
</div>
<div id="haymarket-vouchers" class="container container-wide pb-5 position-relative z-2">
  <div class="row justify-content-center mt-5 mob-mt-0">
  	<div class="col-12 text-center mb-5 px-5">
		  <img src="/img/logos/the-courtyard.svg" class="mt-5 pt-lg-5 h-auto" alt="Haymarket belfast Logo" width="280" height="80" style="max-width: 70vw;" />
		</div>
  	<div class="col-lg-3 mob-px-3 mb-4 pb-5">
    	<div class="voucher">
	      <picture>
			   	<source srcset="/img/vouchers/courtyard-christmas-1.webp" type="image/webp"/> 
			    <source srcset="/img/vouchers/courtyard-christmas-1.jpg" type="image/jpeg"/> 
			    <img src="/img/vouchers/courtyard-christmas-1.jpg" type="image/jpeg" alt="bottomless brunch vouchers at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
			  </picture>
	      <div class="voucher-offer-content">
	      	<p class="title text-large text-white mb-2">Bottomless<br/>Brunch Voucher</p>
	      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
	      	<div class="mb-3"></div>
	      	{{-- <p class="subtext mb-4">They don't want socks!! Trust us, everyone loves a boozy brunch you'll get major brownie points for this!</p> --}}
	      	<a href="https://giveavoucher.com/haymarket" target="_blank">
	      		<button type="button" class="btn btn-primary shadow">Buy Now </button>
	      	</a>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
    	<div class="voucher">
	      <picture>
			    <source srcset="/img/vouchers/courtyard-christmas-2.webp" type="image/webp"/> 
			    <source srcset="/img/vouchers/courtyard-christmas-2.jpg" type="image/jpeg"/> 
			    <img src="/img/vouchers/courtyard-christmas-2.jpg" type="image/jpeg" alt="vouchers at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
			  </picture>
	      <div class="voucher-offer-content">
	      	<p class="title text-large text-white mb-2">£50 <br/>voucher</p>
	      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
	      	<div class="mb-3"></div>
	      	{{-- <p class="subtext mb-4">The perfect gift for the couple that have no similar interests, the serial dater, active partier or someone who appreciates good food & even better cocktails! </p> --}}
	      	<a href="https://giveavoucher.com/haymarket" target="_blank">
	      		<button type="button" class="btn btn-primary shadow">Buy Now </button>
	      	</a>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-3 mob-px-3 mb-4 pb-5">
    	<div class="voucher">
	      <picture>
			    <source srcset="/img/vouchers/courtyard-christmas-3.webp" type="image/webp"/> 
			    <source srcset="/img/vouchers/courtyard-christmas-3.jpg" type="image/jpeg"/> 
			    <img src="/img/vouchers/courtyard-christmas-3.jpg" type="image/jpeg" alt="vouchers at Haymarket Belfast" class="lazy w-100 backdrop-content shadow" />
			  </picture>
	      <div class="voucher-offer-content">
	      	<p class="title text-large text-white mb-2">Customer<br/>Value Voucher</p>
	      	<div class="pre-title-lines mt-2 mb-2 mx-auto d-inline-block"></div>
	      	<div class="mb-3"></div>
	      	{{-- <p class="subtext mb-4">Just remember...the more the merrier! They'll be able to spend this fabulous voucher any time on our banging food & drinks menu!</p> --}}
	      	<a href="https://giveavoucher.com/haymarket" target="_blank">
	      		<button type="button" class="btn btn-primary shadow">Buy Now </button>
	      	</a>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
  </div>
</div>
<div class="py-5 mob-py-0"></div>
@endsection
@section('modals')

@endsection
@section('scripts')
<style>
	body.christmas .opening-hours .today p{
		color: #d54148;
	}
	body.christmas footer .opening-hours .today.mob-show p, 
	body.christmas footer .text-primary {
		color: #d54148 !important;
	}
	body.christmas .christmas-snow {
    position: fixed;
    width: 100vw;
    height: 100%;
    top: 0;
    left: 0;
    background-image: url(/img/graphics/snow.svg);
    background-size: 690px;
    background-repeat: initial;
    opacity: .3;
	}
	.voucher .voucher-offer-content{
		position: relative;
		z-index: 2;
		background-color: #343432;
		text-align: center;
		width: calc(100% - 2rem);
		padding: 1.5rem 1.5rem;
    margin-left: 1rem;
		margin-top: -75px;
		-webkit-box-shadow: 0 0 2rem rgba(0,0,0,.3)!important;
    -moz-box-shadow: 0 0 2rem rgba(0,0,0,.3)!important;
    box-shadow: 0 0 2rem rgba(0,0,0,.3)!important;
	}
	.voucher img{
		position: relative;
		z-index: 1;
	}
	.voucher .btn{
		position: absolute;
    width: 230px;
    left: calc(50% - 115px);
	}
	#haymarket-vouchers .voucher-offer-content{
		background-color: #004238 !important;
	}
</style>
@endsection