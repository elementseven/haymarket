@php
$page = 'Contact';
$pagetitle = 'Contact | Haymarket Belfast';
$metadescription = 'Get in touch with Haymarket belfast by phone, email or using our online contact form!';
$pagetype = 'white';
$pagename = 'contact';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<picture>
  <source srcset="/img/graphics/burger-beer.webp" type="image/webp"/> 
  <source srcset="/img/graphics/burger-beer.png" type="image/jpeg"/> 
  <img src="/img/graphics/burger-beer.png" type="image/jpeg" alt="Book a table at Belfast's newest indoor & outdoor dining - Haymarket Belfast" class="lazy burger-beer-top-left"/>
</picture>
<div class="text-center mt-5 mob-mt-0">
  <img src="/img/logos/logo.svg" class="mt-5 pt-5 h-auto header-top-logo" alt="Haymarket belfast Logo" width="438" height="163"/>
</div>

@endsection
@section('content')
<faqs :categories="{{$categories}}"></faqs>
<header class="container position-relative bg bg-down-up z-1 mb-5 mob-mb-0">
  <div class="row justify-content-center">
    <div class="col-12 mt-5 pt-5 ipadp-pt-0 mob-mt-0 mob-pt-0 text-center">
      <div class="pre-title-lines mx-auto mb-4 mob-my-45"></div>
      <h1 class="mob-mt-0">Get in touch</h1>
    </div>
  </div>
</header>
<div class="container-fluid position-relative pb-5 mob-pb-0">
  <div class="row">
    <div class="container pt-4">
      <div class="row justify-content-center">
      	<div class="col-lg-12 mob-px-4">
          <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></contact-page-form>
    		</div>
      </div>
    </div>
  </div>
</div>
<div class="mb-5"></div>

@endsection