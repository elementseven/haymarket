@php
$page = 'Stock Market Bar';
$pagetitle = "Stock Market Bar @ The Stock Exchange @ Haymarket - Belfast's vibrant indoor/outdoor bar & street food hangout";
$metadescription = "Step into The Stock Exchange in Haymarket, Belfast, for a unique bar experience where drink prices fluctuate like a real stock market! Enjoy dynamic pricing on your favorite beverages as demand dictates, all in a vibrant atmosphere perfect for an exciting night out. Dive into our interactive drink market—where your choices drive the market!";
$pagetype = 'stock';
$pagename = 'stock';
$ogimage = 'https://haymarketbelfast.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
  .stock-top-logo{
    width: 500px;
  }
  @media only screen and (max-width : 767px){
    .stock-top-logo{
      width: 80vw;
    }
  }
</style>
@endsection
@section('header')
<header class="container-fluid position-relative z-1 pt-5 mt-5 mob-mt-0 text-center">
  <img src="/img/logos/the-stock-exchange.svg" class="mb-4 mt-5 h-auto stock-top-logo" alt="The Armoury - Haymarket Belfast Logo" width="500" height="114"  style="max-width: 80%"/>
  <picture>
    <source srcset="/img/venues/stock/cocktail.webp" type="image/webp"/> 
    <source srcset="/img/venues/stock/cocktail.png" type="image/png"/> 
    <img src="/img/venues/stock/cocktail.png" type="image/png" width="550" height="1156" alt="The Stock Exchange - Interactive Darts at - Haymarket Belfast" class="cocktail-top-right h-auto"/>
  </picture>
</header>
@endsection
@section('content')
<div class="container pb-lg-5 ipadp-pt-0 position-relative z-2">
  <div class="row text-center text-lg-left pt-5 mob-pt-0">
    <div class="col-12 text-center mb-lg-5 mb-4">
      <div class="pre-title-lines mb-4 mt-5 mx-auto"></div>
      <h1 class="">Stock Market Bar</h1>
    </div>
    <div class="col-lg-6 mb-4 mb-lg-0">
      <picture>
        <source srcset="/img/venues/stock/stock-market-bar.webp?v=2024-11-20" type="image/webp"/> 
        <source srcset="/img/venues/stock/stock-market-bar.jpg?v=2024-11-20" type="image/jpeg"/> 
        <img src="/img/venues/stock/stock-market-bar.jpg?v=2024-11-20" type="image/jpeg" width="350" height="350" alt="Stockmarket Bar at Haymarket Belfast" class="lazy w-100 h-auto op-45 shadow"/>
      </picture>
    </div>
    <div class="col-lg-6 pl-4 mob-px-3 text-center text-lg-left">
      <p>Welcome to The Stock Exchange Bar – Where Drinks Are Traded Like Stocks</p>
      <p>At The Stock Exchange, we’ve redefined the bar experience with a stock market twist. Here, drink prices change on real-time demand, with frequent MARKET CRASHES, where your favourite drinks can drop in price! An exciting and dynamic environment where every order impacts the “market.”</p>
      <p class="text-larger text-primary"><b>Walk-in Only</b></p>
      {{-- <button type="button" class="btn btn-primary mt-3 booknowbtn">Book Now</button> --}}
    </div>
  </div>
  <div class="row pt-5 mt-5">
    <div class="col-12 pt-2 pl-4 mob-px-3 text-center text-lg-left">
      <h3 class="mb-4">How it works</h3>
      <p class="text-primary"><b>When the Market Crashes - Prices Plummet - Get Yours While Its Hot!</b></p>
      <p>Each drink on our menu acts as its own “stock.” - full of market crashes, trading peaks and falling prices. Keep an eye on our live ticker displays around the bar to catch all the action in real-time! The fun lies in keeping track of the board to get the best price on your fave drinks!</p>
      <p>Experience random “Market Crashes” when prices drop across the board, letting you snag your favourites at a great value.</p>
      <p class="text-primary"><b>Make Your Mark on the Market</b></p>
      <p>Groups of friends can influence drink prices by collectively ordering the same “stock,” adding a layer of strategy to your night out. Compete with others to see who drives the price of their drink down the most, causing a 'market meltdown'!</p>
      <p>Come see how The Stock Exchange Bar turns a night out into an exciting, interactive adventure. With this immersive experience, drink orders become part of the game. Are you ready to play the market?</p>
      {{-- <button type="button" class="btn btn-primary mt-3 booknowbtn">Book Now</button> --}}
    </div>
  </div>
</div>
{{-- <div class="container">
  <div class="row justify-content-center py-5 mob-py-0">
    <div class="col-12 col-md-8 text-center text-lg-left mob-px-0 mob-mb-3 d-lg-none">
      <div class="pre-title-lines mx-auto my-4 mob-my-45"></div>
      <p class="mb-3 home-title-small mob-px-4">SERVING YOU UP CRACKIN' COCKTAILS, BANGIN' STREET FOOD & LIVE MUSIC FROM LOCAL ARTISTS EVERYDAY!</p>
      <a href="/menu" target="_blank">
        <button type="button" class="btn btn-primary mb-4">View Menu </button>
      </a>
    </div>
    <div class="col-lg-5 col-md-8 mob-pl-0 mob-mb-5 mt-5 ipadp-mt-0 mob-mt-0 pr-5 mob-px-3 ipadp-px-3">
      <div id="bookonline" class="res-diary-holder d-inline-block shadow">
        <div class="res-diary-inner">
          <div class="loader loader-inner d-table">
            <div class="d-table-cell align-middle">
              <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
          </div>
          <div class="position-relative z-2">
            <div id="rd-widget-frame" style="max-width: 600px; margin: auto;"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6 pt-5 mob-mt-5 text-center text-lg-left mob-px-0 mob-mb-3 d-none d-lg-block">
      <div class="pre-title-lines mb-4"></div>
      <p class="mb-4 home-title-small">Book your table at Belfast's most unique bar experience where drink prices fluctuate like a real stock market!</p>
      <a href="/menu" target="_blank">
        <button type="button" class="btn btn-primary mb-5" data-aos="fade-up">View Menu </button>
      </a>
    </div>
  </div>
</div> --}}
<div class="pt-5"></div>
<seating-options-stock :activity="'bar'"></seating-options-stock>
<div class="container position-relative z-2 mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 py-5">
      <mailing-list :id="'ml-1-'"></mailing-list>
    </div>
  </div>
</div>
@endsection
@section('modals')

@endsection
@section('scripts')
  <input id="rdwidgeturl" name="rdwidgeturl" value="https://booking.resdiary.com/widget/Standard/HaymarketBelfast/38619?includeJquery=true" type="hidden">
<script type="text/javascript" src="https://booking.resdiary.com/bundles/WidgetV2Loader.js"></script>
<script>
window.addEventListener('load', function() {
  document.querySelectorAll('.booknowbtn').forEach(function(button) {
    button.addEventListener('click', function() {
      var targetElement = document.getElementById("bookonline");
      var offset = 100;
      var targetPosition = targetElement.getBoundingClientRect().top + window.pageYOffset;
      window.scrollTo({
        top: targetPosition - offset,
        behavior: 'smooth'
      });
    });
  });
});
</script>

@endsection