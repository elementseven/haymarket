require('./plugins/modernizr-custom.js');
$(document).ready(function(){
	$('#menu-trigger').each(function(index, element){
        var inview = new Waypoint({
          element: element,
          handler: function (direction) {
            if(direction === "down"){
              $('#scroll-menu').addClass('on');
            }else{
              $('#scroll-menu').removeClass('on');
            }
          },offset: '0%'
        });
      });
	var lazyLoadInstance = new LazyLoad({
	    elements_selector: ".lazy"
	});
	$(".scroll-btn").click(function(){
		$('html,body').animate({
		   scrollTop: $(".scrollTo").offset().top
		});
	});
});