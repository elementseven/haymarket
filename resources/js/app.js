import './bootstrap';
import * as bootstrap from 'bootstrap';

import { createApp } from 'vue';
import 'waypoints/lib/noframework.waypoints.js';
import './plugins/cookieConsent.js';
import './plugins/modernizr-custom.js';

import VueLazyLoad from 'vue3-lazyload';
import { Carousel, Slide, Pagination, Navigation } from 'vue3-carousel';
import 'vue3-carousel/dist/carousel.css';

import AOS from 'aos';
import 'aos/dist/aos.css';
AOS.init({disable: 'mobile'});
window.addEventListener('load', AOS.refresh);
window.addEventListener('resize', AOS.refresh);

import LazyLoad from 'vanilla-lazyload';

// General
import MailingList from './components/MailingList.vue';
import Loader from './components/Loader.vue';
import SiteFooter from './components/Footer.vue';
import MainMenu from './components/Menus/MainMenu.vue';
import MobileMenu from './components/Menus/MobileMenu.vue';
import InstagramFeed from './components/InstagramFeed.vue';

// Contact
import ContactPageForm from './components/Contact/ContactPageForm.vue';
import PartiesForm from './components/PartiesForm.vue';
import Apply from './components/Forms/Apply.vue';
import ChristmasForm from './components/Contact/ChristmasForm.vue';

// Homepage
import HomepageHeader from './components/Home/Header.vue';
import HomeHeaderResdiary from './components/Home/HomeHeaderResdiary.vue';
import EventTiles from './components/Home/EventTiles.vue';
import HomeOffersSlider from './components/Offers/HomeOffersSlider.vue';

// Sliders
import ImageSlider from './components/ImageSlider.vue';
import PartiesSlider from './components/PartiesSlider.vue';
import BBSlider from './components/BBSlider.vue';
import ChristmasSlider from './components/ChristmasSlider.vue';
import StudentSlider from './components/StudentSlider.vue';

// Boxes
import SeatingOptions from './components/SeatingOptions.vue';
import SeatingOptionsBrunch from './components/SeatingOptionsBrunch.vue';

// Faqs
import Faqs from './components/FAQs/Index.vue';
import ArmouryFaqs from './components/FAQs/ArmouryFaqs.vue';

// Offers
import OffersIndex from './components/Offers/Index.vue';
import OffersSlider from './components/Offers/Slider.vue';
import SixNationsCalendar from './components/Offers/SixNationsCalendar.vue';
import OfferModal from './components/Offers/OfferModal.vue';

// Armoury
import ArmouryOptions from './components/ArmouryOptions.vue';
import ArmourySlider from './components/ArmourySlider.vue';
import ArmouryForm from './components/Contact/ArmouryForm.vue';
import SeatingOptionsArmoury from './components/SeatingOptionsArmoury.vue';
import SeatingOptionsArmouryBrunch from './components/SeatingOptionsArmouryBrunch.vue';

// Stock Exchange
import SeatingOptionsStock from './components/SeatingOptionsStock.vue';
import StockSlider from './components/StockSlider.vue';

const app = createApp({
  components: {
    'mailing-list': MailingList,
    'loader': Loader,
    'site-footer': SiteFooter,
    'main-menu': MainMenu,
    'mobile-menu': MobileMenu,
    'instagram-feed': InstagramFeed,
    'contact-page-form': ContactPageForm,
    'parties-form': PartiesForm,
    'apply': Apply,
    'homepage-header': HomepageHeader,
    'homepage-header-resdiary': HomeHeaderResdiary,
    'event-tiles': EventTiles,
    'home-offers-slider': HomeOffersSlider,
    'parties-slider': PartiesSlider,
    'bb-slider': BBSlider,
    'student-slider': StudentSlider,
    'seating-options': SeatingOptions,
    'seating-options-brunch': SeatingOptionsBrunch,
    'faqs': Faqs,
    'armoury-faqs': ArmouryFaqs,
    'offers-slider': OffersSlider,
    'offers-index': OffersIndex,
    'six-nations-calendar': SixNationsCalendar,
    'armoury-options': ArmouryOptions,
    'armoury-slider': ArmourySlider,
    'armoury-form': ArmouryForm,
    'seating-options-armoury': SeatingOptionsArmoury,
    'seating-options-armoury-brunch': SeatingOptionsArmouryBrunch,
    'offer-modal': OfferModal,
    'christmas-slider': ChristmasSlider,
    'christmas-form': ChristmasForm,
    'seating-options-stock': SeatingOptionsStock,
    'stock-slider': StockSlider,
    'image-slider': ImageSlider,
  },
}); 

app.use(VueLazyLoad);
app.mount('#app');

document.addEventListener('DOMContentLoaded', function() {
    document.querySelectorAll('#menu-trigger').forEach(function(element) {
        var inview = new Waypoint({
            element: element,
            handler: function(direction) {
                if (direction === "down") {
                    document.getElementById('scroll-menu').classList.add('on');
                } else {
                    document.getElementById('scroll-menu').classList.remove('on');
                }
            },
            offset: '0%'
        });
    });

    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
    });

    document.querySelectorAll('.scroll-btn').forEach(function(button) {
        button.addEventListener('click', function() {
            window.scrollTo({
                top: document.querySelector(".scrollTo").offsetTop,
                behavior: 'smooth'
            });
        });
    });
});
